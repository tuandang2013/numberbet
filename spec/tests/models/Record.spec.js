const Record = require('../../../src/models/Record');
const User = require('../../../src/models/User');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('Record', () => {
    describe('createForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.createForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => Record.createForUser(user)).toThrowError();
        });

        it('should throw error if user already has record', () => {
            const user = new User._Model({ recordId: 'record-id' });
            expect(() => Record.createForUser(user)).toThrowError();
        });

        it('should rejected if create record failed', (done) => {
            const user = new User._Model();
            const error = 'create goal point error';
            spyOn(Record._Model, 'create').and.rejectWith(error);

            Record.createForUser(user).catch((err) => {
                expect(err).toEqual(error);
                expect(Record._Model.create).toHaveBeenCalled();
                done();
            });
        });

        it('should create record success', (done) => {
            const user = new User._Model();
            const record = { _id: 'created record id' };
            spyOn(Record._Model, 'create').and.resolveTo(record);

            Record.createForUser(user).then(result => {
                expect(result).toBe(record);
                expect(user.recordId).toBe(record._id);
                done();
            });
        })
    });

    describe('getForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.getForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => Record.getForUser(user)).toThrowError();
        });

        it('should null if user dont have record', (done) => {
            const user = new User._Model();

            Record.getForUser(user).then(result => {
                expect(result).toBe(null);
                done();
            });
        });

        it('should return record of user', (done) => {
            const recordId = 'record id';
            const user = new User._Model({
                recordId
            });
            const record = {};
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));

            Record.getForUser(user).then(result => {
                expect(result).toBe(record);
                expect(Record._Model.findById).toHaveBeenCalledWith(recordId);
                done();
            });
        });
    });

    describe('addEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.addEntryForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => Record.addEntryForUser(invalidUser)).toThrowError();
        });

        it('should throw error if entry is null', () => {
            const user = new User._Model();

            expect(() => Record.addEntryForUser(user)).toThrowError();
        });

        it('should create record if user dont have', (done) => {
            const user = new User._Model();
            const entry = {};
            const record = new Record._Model();
            spyOn(Record._Model, 'create').and.resolveTo(record);
            spyOn(user, 'save').and.resolveTo(user);
            //
            spyOn(Record._Model, 'findById')
                .and.returnValue(FakeDocumentQuery(record));
            spyOn(record, 'save').and.resolveTo();

            Record.addEntryForUser(user, entry).then(() => {
                expect(Record._Model.create).toHaveBeenCalled();
                expect(user.recordId).toEqual(record._id.toString());
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should create record if record is not found', (done) => {
            const user = new User._Model();
            const entry = {};
            const record = new Record._Model();
            spyOn(Record._Model, 'create').and.resolveTo(record);
            spyOn(user, 'save').and.resolveTo(user);
            //
            spyOn(Record._Model, 'findById')
                .and.returnValue(FakeDocumentQuery(null));
            spyOn(record, 'save').and.resolveTo();

            Record.addEntryForUser(user, entry).then(() => {
                expect(Record._Model.create).toHaveBeenCalled();
                expect(user.recordId).toEqual(record._id.toString());
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should add entry to user record', (done) => {
            const recordId = 'record id';
            const user = new User._Model({ recordId });
            const record = new Record._Model();
            const entry = {};
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));
            spyOn(record, 'save').and.resolveTo(record);

            Record.addEntryForUser(user, entry).then(result => {
                expect(result).toBe(entry);
                expect(record.entries.length).toBe(1);
                expect(record.save).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getEntriesForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.getEntriesForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => Record.getEntriesForUser(invalidUser)).toThrowError();
        });

        it('should return empty array if user dont have record', (done) => {
            const user = new User._Model();

            Record.getEntriesForUser(user).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should empty if record is not found', (done) => {
            const notFoundRecordId = 'not found record id';
            const user = new User._Model({
                recordId: notFoundRecordId
            });
            spyOn(Record._Model, 'findById').withArgs(notFoundRecordId)
                .and.returnValue(FakeDocumentQuery(null));

            Record.getEntriesForUser(user).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should return record entries of user', (done) => {
            const recordId = 'record id';
            const record = { _id: recordId, entries: [{}] };
            const user = new User._Model({
                recordId
            });
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));

            Record.getEntriesForUser(user).then(result => {
                expect(result).toBe(record.entries);
                done();
            });
        });
    });

    describe('deleteEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.deleteEntryForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => Record.deleteEntryForUser(invalidUser)).toThrowError();
        });

        it('should throw error if id is null', () => {
            const user = new User._Model({
                recordId: 'record id'
            });

            expect(() => Record.deleteEntryForUser(user)).toThrowError();
        });

        it('should throw error if user dont have record', () => {
            const user = new User._Model();
            const id = 'record entry id';

            expect(() => Record.deleteEntryForUser(user, id)).toThrowError();
        });

        it('should reject error if record is not found', (done) => {
            const notFoundRecordId = 'not found record id';
            const user = new User._Model({
                recordId: notFoundRecordId
            });
            const id = 'record entry id';
            spyOn(Record._Model, 'findById').withArgs(notFoundRecordId)
                .and.returnValue(FakeDocumentQuery(null));

            Record.deleteEntryForUser(user, id).catch(error => {
                expect(error).toBeTruthy();
                expect(Record._Model.findById).toHaveBeenCalledWith(notFoundRecordId);
                done();
            });
        });

        it('should reject error if entry is not found', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                _id: recordId,
                entries: [{}]
            });
            const notFoundEntryId = 'not found entry id';
            const user = new User._Model({
                recordId
            });
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));

            Record.deleteEntryForUser(user, notFoundEntryId).catch((error) => {
                expect(record.entries.length).toBe(1);
                expect(error).toBeTruthy();
                done();
            });
        });

        it('should delete entry from user record', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                _id: recordId,
                entries: [{}]
            });
            const deleteEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));
            spyOn(record, 'save').and.resolveTo(record);

            Record.deleteEntryForUser(user, deleteEntryId).then(() => {
                expect(record.entries.length).toBe(0);
                expect(record.save).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('alterEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.alterEntryForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => Record.alterEntryForUser(invalidUser)).toThrowError();
        });

        it('should throw error if id is null', () => {
            const user = new User._Model({
                recordId: 'record id'
            });

            expect(() => Record.alterEntryForUser(user)).toThrowError();
        });

        it('should throw error if user dont have record', () => {
            const user = new User._Model();
            const id = 'record entry id';

            expect(() => Record.alterEntryForUser(user, id)).toThrowError();
        });

        it('should throw error if entry is null', () => {
            const recordId = 'record id';
            const user = new User._Model({
                recordId
            });
            const alterEntryId = 'alter entry id';

            expect(() => Record.alterEntryForUser(user, alterEntryId));
        });

        it('should reject error if record is not found', (done) => {
            const notFoundRecordId = 'not found record id';
            const user = new User._Model({
                recordId: notFoundRecordId
            });
            const id = 'record entry id';
            const entry = {};
            spyOn(Record._Model, 'findById').withArgs(notFoundRecordId)
                .and.returnValue(FakeDocumentQuery(null));

            Record.alterEntryForUser(user, id, entry).catch(error => {
                expect(error).toBeTruthy();
                expect(Record._Model.findById).toHaveBeenCalledWith(notFoundRecordId);
                done();
            });
        });

        it('should rejected with error if entry is not found', (done) => {
            const recordId = 'record id';
            const user = new User._Model({ recordId });
            const notFoundEntryId = 'not found entry id';
            const record = { entries: [] };
            const updateData = {};
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));

            Record.alterEntryForUser(user, notFoundEntryId, updateData).catch(error => {
                expect(error).toBeTruthy();
                done();
            });
        });

        it('should update record entry of user', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                entries: [{}]
            });
            const alterEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            const entry = {};
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));
            spyOn(record, 'save').and.resolveTo(record);

            Record.alterEntryForUser(user, alterEntryId, entry).then(result => {
                expect(result).toEqual(Object.assign(record.entries[0], entry));
                expect(record.entries[0]).toBe(result);
                expect(record.save).toHaveBeenCalled();
                done();
            });
        })
    });

    describe('getRecordHistoryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.getRecordHistoryForUser).toThrowError();
        });

        it('should empty if user dont have record', (done) => {
            const user = {};

            Record.getRecordHistoryForUser(user).then(result => {
                expect(result).toEqual([]);
                done();
            });
        });

        it('should empty if record is not found', (done) => {
            const user = { recordId: 'not found record' };
            spyOn(Record._Model, 'findById').withArgs(user.recordId)
                .and.returnValue(FakeDocumentQuery(null));

            Record.getRecordHistoryForUser(user).then(result => {
                expect(result).toEqual([]);
                expect(Record._Model.findById).toHaveBeenCalled();
                done();
            });
        });

        it('should return record histories for user', (done) => {
            const record = { histories: [{}, {}] };
            const user = { recordId: 'record id' };
            spyOn(Record._Model, 'findById').withArgs(user.recordId)
                .and.returnValue(FakeDocumentQuery(record));

            Record.getRecordHistoryForUser(user).then(result => {
                expect(result.length).toEqual(record.histories.length);
                done();
            });

        });
    });

    describe('removeForUser()', () => {
        it('should throw error if user is null', () => {
            expect(Record.removeForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => Record.removeForUser(invalidUser)).toThrowError();
        });

        it('should remove record for user', (done) => {
            const recordId = 'record id';
            const record = {remove(){}};
            const user = new User._Model({ recordId });
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(record));
            spyOn(record, 'remove').and.resolveTo(null);
            spyOn(user, 'save').and.resolveTo(user);

            Record.removeForUser(user).then(() => {
                expect(user.recordId).toBe(null);
                expect(record.remove).toHaveBeenCalled();
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if record is not found', (done) => {
            const recordId = 'not found record';
            const user = new User._Model({ recordId });
            spyOn(Record._Model, 'findById').withArgs(recordId)
                .and.returnValue(FakeDocumentQuery(null));
            spyOn(user, 'save');

            Record.removeForUser(user).then(() => {
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('remove()', () => {
        it('should throw error if record is null', () => {
            expect(Record.remove).toThrowError();
        });

        it('should throw error if record is invalid', () => {
            const invalidRecord = 0;

            expect(() => Record.remove(invalidRecord)).toThrowError();
        });

        it('should remove record', (done) => {
            const record = new Record._Model();
            spyOn(record, 'remove').and.resolveTo(null);

            Record.remove(record).then(() => {
                expect(record.remove).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('findById()', () => {
        it('should throw error if id is null', () => {
            expect(Record.findById).toThrowError();
        });

        it('should return record by id', (done) => {
            const id = 'record id';
            const record = {};
            spyOn(Record._Model, 'findById').withArgs(id)
                .and.returnValue(FakeDocumentQuery(record));

            Record.findById(id).then(result => {
                expect(result).toBe(record);
                done();
            });
        });
    });
});