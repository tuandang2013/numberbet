const SouthNumber = require('../../../src/models/SouthNumber');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('SouthNumber model', () => {
    describe('insert()', () => {
        it('should throw error if item is null', () => {
            expect(() => SouthNumber.insert()).toThrowError();
        });

        it('should call create() with item', (done) => {
            const item = {};
            const doc = {};
            spyOn(SouthNumber._Model, 'create').withArgs(item).and.resolveTo([doc]);

            SouthNumber.insert(item).then(result => expect(result).toEqual(doc)).then(done);
        });
    });

    describe('getByDate()', () => {
        it('should throw error when date undefined', () => {
            expect(SouthNumber.getByDate).toThrow();
        });

        it('should throw error when date is not a string', () => {
            expect(() => SouthNumber.getByDate(1)).toThrow();
        });

        it('should try find first item with requestable date', (done) => {
            const date = '2020-12-13';
            const firstItem = new SouthNumber._Model();
            spyOn(SouthNumber._Model, "findOne").withArgs({ date })
                .and.returnValue(FakeDocumentQuery(firstItem));

            SouthNumber.getByDate(date).then(result => {
                expect(result).toBe(firstItem);
                done();
            });

            expect(SouthNumber._Model.findOne).toHaveBeenCalledWith({ date: date });
        });
    });

    describe('has()', () => {
        it('should throw if date is null', () => {
            const date = null;
            expect(() => SouthNumber.has(date)).toThrowError();
        });

        it('should throw if date is not a string', () => {
            const date = 0;
            expect(() => SouthNumber.has(date)).toThrowError();
        });

        it('should true if has data of date', (done) => {
            const date = '2020-10-10';
            const value = true;
            spyOn(SouthNumber._Model, 'exists')
                .and.resolveTo(value);

                SouthNumber.has(date).then(result => {
                expect(result).toBeTruthy();
                done();
            })
        });
    });

    describe('getById()', () => {
        it('should model query by id', (done) => {
            const id = 'id';
            const southNumber = {};
            spyOn(SouthNumber._Model, 'findById')
                .withArgs(id)
                .and.returnValue(FakeDocumentQuery(southNumber));
            
            SouthNumber.getById(id).then(result => {
                expect(result).toEqual(southNumber);
                done();
            })
        });
    });
});