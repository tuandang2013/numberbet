const User = require('../../../src/models/User');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('User', () => {
    describe('findById()', () => {
        it('should throw error if id is null', () => {
            expect(User.findById).toThrowError();
        });

        it('should query user model', (done) => {
            const id = 'id';
            const user = {};
            spyOn(User._Model, 'findById').withArgs(id)
                .and.returnValue(FakeDocumentQuery(user));

            User.findById(id).then(result => {
                expect(result).toBe(user);
                done();
            })
        });
    });

    describe('findByEmail()', () => {
        it('should throw error if email is null', () => {
            expect(User.findByEmail).toThrowError();
        });

        it('should query user model', (done) => {
            const email = 'email';
            const user = {};
            spyOn(User._Model, 'findOne').withArgs({ email })
                .and.returnValue(FakeDocumentQuery(user));

            User.findByEmail(email).then(result => {
                expect(result).toBe(user);
                done();
            })
        });
    });

    describe('insert()', () => {
        it('should throw error if user is null', () => {
            expect(User.insert).toThrowError();
        });

        it('should return inserted user', (done) => {
            const user = {};
            const insertedUser = {};
            spyOn(User._Model, 'create').withArgs(user)
                .and.resolveTo(insertedUser);

            User.insert(user).then(result => {
                expect(result).toBe(insertedUser);
                done();
            });
        });
    });

    describe('update()', () => {
        it('should throw error if user is null', () => {
            expect(User.update).toThrowError();
        });

        it('should throw error is user is not User model', () => {
            const invalidUser = {};

            expect(() => User.update(invalidUser)).toThrowError();
        });

        it('should update user', (done) => {
            const user = new User._Model();
            spyOn(user, 'save')
                .and.resolveTo(user);

            User.update(user).then(result => {
                expect(result).toBe(user);
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getAll()', () => {
        it('should return all users', (done) => {
            const users = [{}, {}];
            spyOn(User._Model, 'find').withArgs({})
                .and.returnValue(FakeDocumentQuery(users));

            User.getAll().then(result => {
                expect(result).toBe(users);
                expect(User._Model.find).toHaveBeenCalledWith({});
                done();
            });
        });
    });

    describe('count()', () => {
        it('should return document count', (done) => {
            const count = 10;
            spyOn(User._Model, 'estimatedDocumentCount').and.returnValue(FakeDocumentQuery(count));

            User.count().then(result => {
                expect(result).toBe(count);
                done();
            });
        });
    });

    describe('remove()', () => {
        it('should throw error if user is null', () => {
            expect(User.remove).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => User.remove(invalidUser)).toThrowError();
        });

        it('should remove user', (done) => {
            const user = new User._Model();
            spyOn(user, 'remove').and.resolveTo(null);
            
            User.remove(user).then(() => {
                expect(user.remove).toHaveBeenCalled();
                done();
            });
        });
    });
});