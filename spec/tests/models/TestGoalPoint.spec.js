const TestGoalPoint = require('../../../src/models/TestGoalPoint');
const User = require('../../../src/models/User');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('TestGoalPoint', () => {
    describe('createForUser()', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.createForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => TestGoalPoint.createForUser(user)).toThrowError();
        });

        it('should throw error if user already has test goal point', () => {
            const user = new User._Model({ testGoalPointId: 'goal-point-id' });
            expect(() => TestGoalPoint.createForUser(user)).toThrowError();
        });

        it('should rejected if create test goal point failed', (done) => {
            const user = new User._Model();
            const error = 'create goal point error';
            spyOn(TestGoalPoint._Model, 'create').and.rejectWith(error);

            TestGoalPoint.createForUser(user).catch((err) => {
                expect(err).toEqual(error);
                expect(TestGoalPoint._Model.create).toHaveBeenCalled();
                done();
            });
        });

        it('should return test goal point id if create goal point success', (done) => {
            const user = new User._Model();
            const testGoalPoint = { _id: 'created test goal point id' };
            spyOn(TestGoalPoint._Model, 'create').and.resolveTo(testGoalPoint);

            TestGoalPoint.createForUser(user).then(result => {
                expect(result).toBe(testGoalPoint);
                expect(user.testGoalPointId).toBe(testGoalPoint._id);
                done();
            });
        })
    });

    describe('getForUser()', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.getForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => TestGoalPoint.getForUser(user)).toThrowError();
        });

        it('should null if user dont have test goal point', (done) => {
            const user = new User._Model();

            TestGoalPoint.getForUser(user).then(result => {
                expect(result).toBe(null);
                done();
            });
        });

        it('should return test goal point of user', (done) => {
            const testGoalPointId = 'test goal point id';
            const user = new User._Model({
                testGoalPointId: testGoalPointId
            });
            const testGoalPoint = {};
            spyOn(TestGoalPoint._Model, 'findById').withArgs(testGoalPointId)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));

            TestGoalPoint.getForUser(user).then(result => {
                expect(result).toBe(testGoalPoint);
                expect(TestGoalPoint._Model.findById).toHaveBeenCalledWith(testGoalPointId);
                done();
            });
        });
    });

    describe('addEntryForUser', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.addEntryForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => TestGoalPoint.addEntryForUser(user)).toThrowError();
        });

        it('should throw error if entry is null', () => {
            const user = new User._Model();

            expect(() => TestGoalPoint.addEntryForUser(user)).toThrowError();
        });

        it('should create goal point if user don\'t have', (done) => {
            const user = new User._Model();
            const entry = {};
            const testGoalPoint = new TestGoalPoint._Model();
            spyOn(TestGoalPoint._Model, 'create').and.resolveTo(testGoalPoint);
            spyOn(user, 'save').and.resolveTo(user);
            spyOn(testGoalPoint, 'save').and.resolveTo(testGoalPoint);

            TestGoalPoint.addEntryForUser(user, entry).then(() => {
                expect(user.testGoalPointId).toEqual(testGoalPoint._id.toString());
                expect(TestGoalPoint._Model.create).toHaveBeenCalled();
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should add entry to goal point', (done) => {
            const testGoalPoint = { _id: 'test goal point id', entries: [], save() { } };
            const user = new User._Model({ testGoalPointId: testGoalPoint._id });
            const entry = {};
            spyOn(TestGoalPoint._Model, 'findById').withArgs(testGoalPoint._id)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));
            spyOn(testGoalPoint, 'save').and.resolveTo(testGoalPoint);

            TestGoalPoint.addEntryForUser(user, entry).then(result => {
                expect(testGoalPoint.entries.length).toBe(1);
                expect(result).toBe(entry);
                expect(testGoalPoint.save).toHaveBeenCalled();
                done();
            });
        })

        // it('should update total point for user', (done) => {
        //     const testGoalPoint = { _id: 'test goal point id', entries: [], save() { } };
        //     const user = new User._Model({ testGoalPointId: testGoalPoint._id });
        //     const entry = { value: 100 };
        //     spyOn(TestGoalPoint._Model, 'findById').withArgs(testGoalPoint._id)
        //         .and.returnValue(FakeDocumentQuery(testGoalPoint));
        //     spyOn(testGoalPoint, 'save').and.resolveTo(testGoalPoint);

        //     TestGoalPoint.addEntryForUser(user, entry).then(() => {
        //         expect(testGoalPoint.total).toEqual(entry.value);
        //         expect(testGoalPoint.save).toHaveBeenCalled();
        //         done();
        //     });
        // });
    });

    describe('deleteEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.deleteEntryForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => TestGoalPoint.deleteEntryForUser(user)).toThrowError();
        });

        it('should throw error if id is null', () => {
            const user = new User._Model();

            expect(() => TestGoalPoint.deleteEntryForUser(user)).toThrowError();
        });

        it('should throw error if user dont have test goal point', () => {
            const user = new User._Model();
            const id = 'delete id';

            expect(() => TestGoalPoint.deleteEntryForUser(user, id)).toThrowError();
        });

        it('should reject error if entry is not found', (done) => {
            const testGoalPoint = new TestGoalPoint._Model();
            const user = new User._Model({ testGoalPointId: testGoalPoint._id });
            testGoalPoint.entries.push({});
            const id = 'not found enty id'
            spyOn(TestGoalPoint._Model, 'findById').withArgs(user.testGoalPointId)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));
            spyOn(testGoalPoint, 'save').and.resolveTo(testGoalPoint);

            TestGoalPoint.deleteEntryForUser(user, id).catch((error) => {
                expect(error).toBeTruthy();
                done();
            });
        });

        it('should delete entry from test goal point', (done) => {
            const testGoalPoint = new TestGoalPoint._Model();
            const user = new User._Model({ testGoalPointId: testGoalPoint._id });
            testGoalPoint.entries.push({});
            const id = testGoalPoint.entries[0]._id;
            spyOn(TestGoalPoint._Model, 'findById').withArgs(user.testGoalPointId)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));
            spyOn(testGoalPoint, 'save').and.resolveTo(testGoalPoint);

            TestGoalPoint.deleteEntryForUser(user, id).then(() => {
                expect(testGoalPoint.entries.length).toBe(0);
                expect(testGoalPoint.save).toHaveBeenCalled();
                done();
            });
        });

        // it('should update goal point total value', (done) => {
        //     const testGoalPoint = new TestGoalPoint._Model();
        //     const user = new User._Model({ testGoalPointId: testGoalPoint._id });
        //     testGoalPoint.total = 100;
        //     testGoalPoint.entries.push({ value: 100 });
        //     const id = testGoalPoint.entries[0]._id;
        //     spyOn(TestGoalPoint._Model, 'findById').withArgs(user.testGoalPointId)
        //         .and.returnValue(FakeDocumentQuery(testGoalPoint));
        //     spyOn(testGoalPoint, 'save').and.resolveTo(testGoalPoint);

        //     TestGoalPoint.deleteEntryForUser(user, id).then(() => {
        //         expect(testGoalPoint.total).toEqual(0);
        //         expect(testGoalPoint.save).toHaveBeenCalled();
        //         done();
        //     });
        // });
    });

    describe('findEntryByDateForUser()', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.findEntryByDateForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => TestGoalPoint.findEntryByDateForUser(user)).toThrowError();
        });

        it('should throw error if date is null', () => {
            const user = new User._Model();

            expect(() => TestGoalPoint.findEntryByDateForUser(user)).toThrowError();
        });

        it('should throw error if date is not Date', () => {
            const user = new User._Model();
            const date = 0;

            expect(() => TestGoalPoint.findEntryByDateForUser(user, date)).toThrowError();
        });

        it('should empty if user don\'t have test goal point', (done) => {
            const user = new User._Model();
            const date = new Date();

            TestGoalPoint.findEntryByDateForUser(user, date).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should empty if test goal point is not found', (done) => {
            const notFoundTestGoalPointId = 'not found test goal point';
            const user = new User._Model({
                testGoalPointId: notFoundTestGoalPointId
            });
            const searchDate = new Date('2020-02-28');
            spyOn(TestGoalPoint._Model, 'findById').withArgs(user.testGoalPointId)
                .and.returnValue(FakeDocumentQuery(null));

            TestGoalPoint.findEntryByDateForUser(user, searchDate).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should find entries by date', (done) => {
            const entries = [{
                value: 0,
                reason: 'reason',
                createdDate: new Date('2020-02-27')
            }, {
                value: 1,
                reason: 'reason 2',
                createdDate: new Date('2020-02-28')
            }];
            const testGoalPoint = new TestGoalPoint._Model({
                entries
            });
            const user = new User._Model({
                testGoalPointId: testGoalPoint._id
            });
            const searchDate = new Date('2020-02-28');
            spyOn(TestGoalPoint._Model, 'findById').withArgs(user.testGoalPointId)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));

            TestGoalPoint.findEntryByDateForUser(user, searchDate).then(result => {
                expect(result.length).toBe(1);
                expect(result[0]).toBe(testGoalPoint.entries[1]);
                done();
            });
        });
    });

    describe('getEntriesForUser()', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.getEntriesForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => TestGoalPoint.getEntriesForUser(invalidUser)).toThrowError();
        });

        it('should empty if user dont have test goal point', (done) => {
            const user = new User._Model();

            TestGoalPoint.getEntriesForUser(user).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should empty if test goal point is not found', (done) => {
            const notFoundTestGoalPoint = 'not found test goal point';
            const user = new User._Model({ testGoalPointId: notFoundTestGoalPoint });
            spyOn(TestGoalPoint._Model, 'findById').withArgs(notFoundTestGoalPoint)
                .and.returnValue(FakeDocumentQuery(null));

            TestGoalPoint.getEntriesForUser(user).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should return entries of test goal point', (done) => {
            const testGoalPoint = { entries: [{}, {}] };
            const user = new User._Model({ testGoalPointId: 'test goal point id' });
            spyOn(TestGoalPoint._Model, 'findById').withArgs(user.testGoalPointId)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));

            TestGoalPoint.getEntriesForUser(user).then(result => {
                expect(result).toBe(testGoalPoint.entries);
                done();
            });
        });
    });

    describe('removeForUser()', () => {
        it('should throw error if user is null', () => {
            expect(TestGoalPoint.removeForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => TestGoalPoint.removeForUser(invalidUser)).toThrowError();
        });

        it('should remove goal point for user', (done) => {
            const testGoalPointId = 'test goal point id';
            const testGoalPoint = { remove() { } };
            const user = new User._Model({ testGoalPointId: testGoalPointId });
            spyOn(TestGoalPoint._Model, 'findById').withArgs(testGoalPointId)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));
            spyOn(testGoalPoint, 'remove').and.resolveTo(null);
            spyOn(user, 'save').and.resolveTo(user);

            TestGoalPoint.removeForUser(user).then(() => {
                expect(testGoalPoint.remove).toHaveBeenCalled();
                expect(user.testGoalPointId).toBe(null);
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user dont have goal point', (done) => {
            const testGoalPointId = 'not found test goal point';
            const user = new User._Model({ testGoalPointId });
            spyOn(TestGoalPoint._Model, 'findById').withArgs(testGoalPointId)
                .and.returnValue(FakeDocumentQuery(null));
            spyOn(user, 'save');

            TestGoalPoint.removeForUser(user).then(() => {
                expect(user.save).not.toHaveBeenCalled();
                done();
            });

        });
    });

    describe('remove()', () => {
        it('should throw error if test goal point is null', () => {
            expect(TestGoalPoint.remove).toThrowError();
        });

        it('should throw error if goal point is invalid', () => {
            const invalidTestGoalPoint = 0;

            expect(() => TestGoalPoint.remove(invalidTestGoalPoint)).toThrowError();
        });

        it('should remove test goal point', (done) => {
            const testGoalPoint = new TestGoalPoint._Model();
            spyOn(testGoalPoint, 'remove').and.resolveTo(null);

            TestGoalPoint.remove(testGoalPoint).then(() => {
                expect(testGoalPoint.remove).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('findById()', () => {
        it('should throw error if id is null', () => {
            expect(TestGoalPoint.findById).toThrowError();
        });

        it('should return test goal point by id', (done) => {
            const id = 'test goal point id';
            const testGoalPoint = {};
            spyOn(TestGoalPoint._Model, 'findById').withArgs(id)
                .and.returnValue(FakeDocumentQuery(testGoalPoint));

            TestGoalPoint.findById(id).then(result => {
                expect(result).toBe(testGoalPoint);
                done();
            });
        });
    });
});