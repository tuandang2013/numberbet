const Source = require('../../../src/models/Source');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('Source', () => {
    describe('getByDate()', () => {
        it('should throw error when date undefined', () => {
            expect(Source.getByDate).toThrow();
        });

        it('should throw error when date is not a string', () => {
            expect(() => Source.getByDate(1)).toThrow();
        });

        it('should try find first item with requestable date', (done) => {
            const date = '2020-12-13';
            const firstItem = new Source._Model();
            spyOn(Source._Model, "findOne").withArgs({ date })
                .and.returnValue(FakeDocumentQuery(firstItem));

            Source.getByDate(date).then(result => {
                expect(result).toBe(firstItem);
                done();
            });

            expect(Source._Model.findOne).toHaveBeenCalledWith({ date: date });
        });
    });

    describe('has()', () => {
        it('should throw if query is null', () => {
            const query = null;
            expect(() => Source.has(query)).toThrowError();
        });

        it('should true if has data', (done) => {
            const query = { date: '2020-10-10' };
            const value = true;
            spyOn(Source._Model, 'exists')
                .and.resolveTo(value);

            Source.has(query).then(result => {
                expect(result).toBeTruthy();
                done();
            })
        });
    });
});