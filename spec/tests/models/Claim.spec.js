const Claim = require('../../../src/models/Claim');
const { GUEST, REGISTERED, ADMINISTRATOR } = require('../../../src/models/Claim');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('Claim', () => {
    describe('create()', () => {
        it('should throw error if claim is null', () => {
            expect(Claim.create).toThrowError();
        });

        it('should create claim', (done) => {
            const claim = { name: 'name' };
            const insertedClaim = { _id: 'id' };
            spyOn(Claim._Model, 'create')
                .and.resolveTo(insertedClaim);

            Claim.create(claim).then(result => {
                expect(result).toBe(insertedClaim);
                done();
            });
        });
    });

    describe('findByName()', () => {
        it('should throw error if name is null', () => {
            expect(Claim.findByName).toThrowError();
        });

        it('should return claim for name', (done) => {
            const name = 'name';
            const claim = { name };
            spyOn(Claim._Model, 'findOne').withArgs({ name })
                .and.returnValue(FakeDocumentQuery(claim));

            Claim.findByName(name).then(result => {
                expect(result).toBe(claim);
                done();
            });
        });
    });

    describe('guestClaim()', () => {
        it('should return guest claim', (done) => {
            const guestClaim = { name: 'Guest' };
            spyOn(Claim._Model, 'findOne').withArgs({ name: GUEST })
                .and.returnValue(FakeDocumentQuery(guestClaim));

            Claim.guestClaim().then(result => {
                expect(result).toBe(guestClaim);
                done();
            });
        });
    });

    describe('registerClaim()', () => {
        it('should return registerd claim', (done) => {
            const registeredClaim = { name: 'Registered' };
            spyOn(Claim._Model, 'findOne').withArgs({ name: REGISTERED })
                .and.returnValue(FakeDocumentQuery(registeredClaim));

            Claim.registeredClaim().then(result => {
                expect(result).toBe(registeredClaim);
                done();
            });
        });
    });

    describe('administatorClaim()', () => {
        it('should return administrator claim', (done) => {
            const administratorClaim = { name: 'Administrator' };
            spyOn(Claim._Model, 'findOne').withArgs({ name: ADMINISTRATOR })
                .and.returnValue(FakeDocumentQuery(administratorClaim));

            Claim.administratorClaim().then(result => {
                expect(result).toBe(administratorClaim);
                done();
            });
        });
    });

    describe('findById()', () => {
        it('should throw error if id is null', () => {
            expect(Claim.findById).toThrowError();
        });

        it('should return claim by id', (done) => {
            const id = 'id';
            const claim = {};
            spyOn(Claim._Model, 'findById').withArgs(id)
                .and.returnValue(FakeDocumentQuery(claim));

            Claim.findById(id).then(result => {
                expect(result).toBe(claim);
                done();
            });
        });
    });

    describe('getAll()', () => {
        it('should return all claims', (done) => {
            const claims = [{}, {}, {}];
            spyOn(Claim._Model, 'find').and.returnValue(FakeDocumentQuery(claims));

            Claim.getAll().then(result => {
                expect(result).toBe(claims);
                done();
            });
        });
    });
});