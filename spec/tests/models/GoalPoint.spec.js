const GoalPoint = require('../../../src/models/GoalPoint');
const User = require('../../../src/models/User');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('GoalPoint', () => {
    describe('createForUser()', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.createForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => GoalPoint.createForUser(user)).toThrowError();
        });

        it('should throw error if user already has goal point', () => {
            const user = new User._Model({ goalPointId: 'goal-point-id' });
            expect(() => GoalPoint.createForUser(user)).toThrowError();
        });

        it('should rejected if create goal point failed', (done) => {
            const user = new User._Model();
            const error = 'create goal point error';
            spyOn(GoalPoint._Model, 'create').and.rejectWith(error);

            GoalPoint.createForUser(user).catch((err) => {
                expect(err).toEqual(error);
                expect(GoalPoint._Model.create).toHaveBeenCalled();
                done();
            });
        });

        it('should return goal point id if create goal point success', (done) => {
            const user = new User._Model();
            const goalPoint = { _id: 'created goal point id' };
            spyOn(GoalPoint._Model, 'create').and.resolveTo(goalPoint);

            GoalPoint.createForUser(user).then(result => {
                expect(result).toBe(goalPoint);
                expect(user.goalPointId).toBe(goalPoint._id);
                done();
            });
        })
    });

    describe('getForUser()', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.getForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => GoalPoint.getForUser(user)).toThrowError();
        });

        it('should null if user dont have goal point', (done) => {
            const user = new User._Model();

            GoalPoint.getForUser(user).then(result => {
                expect(result).toBe(null);
                done();
            });
        });

        it('should return goal point of user', (done) => {
            const goalPointId = 'goal point id';
            const user = new User._Model({
                goalPointId
            });
            const goalPoint = {};
            spyOn(GoalPoint._Model, 'findById').withArgs(goalPointId)
                .and.returnValue(FakeDocumentQuery(goalPoint));

            GoalPoint.getForUser(user).then(result => {
                expect(result).toBe(goalPoint);
                expect(GoalPoint._Model.findById).toHaveBeenCalledWith(goalPointId);
                done();
            });
        });
    });

    describe('addEntryForUser', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.addEntryForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => GoalPoint.addEntryForUser(user)).toThrowError();
        });

        it('should throw error if entry is null', () => {
            const user = new User._Model();

            expect(() => GoalPoint.addEntryForUser(user)).toThrowError();
        });

        it('should create goal point if user don\'t have', (done) => {
            const user = new User._Model();
            const entry = {};
            const goalPoint = new GoalPoint._Model();
            spyOn(GoalPoint._Model, 'create').and.resolveTo(goalPoint);
            spyOn(user, 'save').and.resolveTo(user);
            spyOn(goalPoint, 'save').and.resolveTo(goalPoint);

            GoalPoint.addEntryForUser(user, entry).then(() => {
                expect(user.goalPointId).toEqual(goalPoint._id.toString());
                expect(GoalPoint._Model.create).toHaveBeenCalled();
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should add entry to goal point', (done) => {
            const goalPoint = { _id: 'goal point id', entries: [], save() { } };
            const user = new User._Model({ goalPointId: goalPoint._id });
            const entry = {};
            spyOn(GoalPoint._Model, 'findById').withArgs(goalPoint._id)
                .and.returnValue(FakeDocumentQuery(goalPoint));
            spyOn(goalPoint, 'save').and.resolveTo(goalPoint);

            GoalPoint.addEntryForUser(user, entry).then(result => {
                expect(goalPoint.entries.length).toBe(1);
                expect(result).toBe(entry);
                expect(goalPoint.save).toHaveBeenCalled();
                done();
            });
        });

        // it('should update total point for user', (done) => {
        //     const goalPoint = { _id: 'goal point id', entries: [], save() { } };
        //     const user = new User._Model({ goalPointId: goalPoint._id });
        //     const entry = { value: 100 };
        //     spyOn(GoalPoint._Model, 'findById').withArgs(goalPoint._id)
        //         .and.returnValue(FakeDocumentQuery(goalPoint));
        //     spyOn(goalPoint, 'save').and.resolveTo(goalPoint);

        //     GoalPoint.addEntryForUser(user, entry).then(() => {
        //         expect(goalPoint.total).toEqual(entry.value);
        //         expect(goalPoint.save).toHaveBeenCalled();
        //         done();
        //     });
        // });
    });

    describe('deleteEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.deleteEntryForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => GoalPoint.deleteEntryForUser(user)).toThrowError();
        });

        it('should throw error if id is null', () => {
            const user = new User._Model();

            expect(() => GoalPoint.deleteEntryForUser(user)).toThrowError();
        });

        it('should throw error if user dont have goal point', () => {
            const user = new User._Model();
            const id = 'delete id';

            expect(() => GoalPoint.deleteEntryForUser(user, id)).toThrowError();
        });

        it('should reject error if entry is not found', (done) => {
            const goalPoint = new GoalPoint._Model();
            const user = new User._Model({ goalPointId: goalPoint._id });
            goalPoint.entries.push({});
            const id = 'not found enty id'
            spyOn(GoalPoint._Model, 'findById').withArgs(user.goalPointId)
                .and.returnValue(FakeDocumentQuery(goalPoint));
            spyOn(goalPoint, 'save').and.resolveTo(goalPoint);

            GoalPoint.deleteEntryForUser(user, id).catch((error) => {
                expect(error).toBeTruthy();
                done();
            });
        });

        it('should delete entry from goal point', (done) => {
            const goalPoint = new GoalPoint._Model();
            const user = new User._Model({ goalPointId: goalPoint._id });
            goalPoint.entries.push({});
            const id = goalPoint.entries[0]._id;
            spyOn(GoalPoint._Model, 'findById').withArgs(user.goalPointId)
                .and.returnValue(FakeDocumentQuery(goalPoint));
            spyOn(goalPoint, 'save').and.resolveTo(goalPoint);

            GoalPoint.deleteEntryForUser(user, id).then(() => {
                expect(goalPoint.entries.length).toBe(0);
                expect(goalPoint.save).toHaveBeenCalled();
                done();
            });
        });

        // it('should update goal point total value', (done) => {
        //     const goalPoint = new GoalPoint._Model();
        //     const user = new User._Model({ goalPointId: goalPoint._id });
        //     goalPoint.total = 100;
        //     goalPoint.entries.push({ value: 100 });
        //     const id = goalPoint.entries[0]._id;
        //     spyOn(GoalPoint._Model, 'findById').withArgs(user.goalPointId)
        //         .and.returnValue(FakeDocumentQuery(goalPoint));
        //     spyOn(goalPoint, 'save').and.resolveTo(goalPoint);

        //     GoalPoint.deleteEntryForUser(user, id).then(() => {
        //         expect(goalPoint.total).toEqual(0);
        //         expect(goalPoint.save).toHaveBeenCalled();
        //         done();
        //     });
        // });
    });

    describe('findEntryByDateForUser()', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.findEntryByDateForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => GoalPoint.findEntryByDateForUser(user)).toThrowError();
        });

        it('should throw error if date is null', () => {
            const user = new User._Model();

            expect(() => GoalPoint.findEntryByDateForUser(user)).toThrowError();
        });

        it('should throw error if date is not Date', () => {
            const user = new User._Model();
            const date = 0;

            expect(() => GoalPoint.findEntryByDateForUser(user, date)).toThrowError();
        });

        it('should empty if user don\'t have goal point', (done) => {
            const user = new User._Model();
            const date = new Date();

            GoalPoint.findEntryByDateForUser(user, date).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should empty if goal point is not found', (done) => {
            const notFoundGoalPointId = 'not found goal point';
            const user = new User._Model({
                goalPointId: notFoundGoalPointId
            });
            const searchDate = new Date('2020-02-28');
            spyOn(GoalPoint._Model, 'findById').withArgs(user.goalPointId)
                .and.returnValue(FakeDocumentQuery(null));

            GoalPoint.findEntryByDateForUser(user, searchDate).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should find entries by date', (done) => {
            const entries = [{
                value: 0,
                reason: 'reason',
                createdDate: new Date('2020-02-27')
            }, {
                value: 1,
                reason: 'reason 2',
                createdDate: new Date('2020-02-28')
            }];
            const goalPoint = new GoalPoint._Model({
                entries
            });
            const user = new User._Model({
                goalPointId: goalPoint._id
            });
            const searchDate = new Date('2020-02-28');
            spyOn(GoalPoint._Model, 'findById').withArgs(user.goalPointId)
                .and.returnValue(FakeDocumentQuery(goalPoint));

            GoalPoint.findEntryByDateForUser(user, searchDate).then(result => {
                expect(result.length).toBe(1);
                expect(result[0]).toBe(goalPoint.entries[1]);
                done();
            });
        });
    });

    describe('getEntriesForUser()', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.getEntriesForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => GoalPoint.getEntriesForUser(invalidUser)).toThrowError();
        });

        it('should empty if user dont have goal point', (done) => {
            const user = new User._Model();

            GoalPoint.getEntriesForUser(user).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should empty if goal point is not found', (done) => {
            const notFoundGoalPoint = 'not found goal point';
            const user = new User._Model({ goalPointId: notFoundGoalPoint });
            spyOn(GoalPoint._Model, 'findById').withArgs(notFoundGoalPoint)
                .and.returnValue(FakeDocumentQuery(null));

            GoalPoint.getEntriesForUser(user).then(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should return entries of goal point', (done) => {
            const goalPoint = { entries: [{ createdDate: new Date() }, { createdDate: new Date() }] };
            const user = new User._Model({ goalPointId: 'goal point id' });
            spyOn(GoalPoint._Model, 'findById').withArgs(user.goalPointId)
                .and.returnValue(FakeDocumentQuery(goalPoint));

            GoalPoint.getEntriesForUser(user).then(result => {
                expect(result).toBe(goalPoint.entries);
                done();
            });
        });
    });

    describe('removeForUser()', () => {
        it('should throw error if user is null', () => {
            expect(GoalPoint.removeForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => GoalPoint.removeForUser(invalidUser)).toThrowError();
        });

        it('should remove goal point for user', (done) => {
            const goalPointId = 'goal point id';
            const goalPoint = { remove() { } };
            const user = new User._Model({ goalPointId });
            spyOn(GoalPoint._Model, 'findById').withArgs(goalPointId)
                .and.returnValue(FakeDocumentQuery(goalPoint));
            spyOn(goalPoint, 'remove').and.resolveTo(null);
            spyOn(user, 'save').and.resolveTo(user);

            GoalPoint.removeForUser(user).then(() => {
                expect(goalPoint.remove).toHaveBeenCalled();
                expect(user.goalPointId).toBe(null);
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user dont have goal point', (done) => {
            const goalPointId = 'not found goal point';
            const user = new User._Model({ goalPointId });
            spyOn(GoalPoint._Model, 'findById').withArgs(goalPointId)
                .and.returnValue(FakeDocumentQuery(null));
            spyOn(user, 'save');

            GoalPoint.removeForUser(user).then(() => {
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
            
        });
    });

    describe('remove()', () => {
        it('should throw error if goal point is null', () => {
            expect(GoalPoint.remove).toThrowError();
        });

        it('should throw error if goal point is invalid', () => {
            const invalidGoalPoint = 0;

            expect(() => GoalPoint.remove(invalidGoalPoint)).toThrowError();
        });

        it('should remove goal point', (done) => {
            const goalPoint = new GoalPoint._Model();
            spyOn(goalPoint, 'remove').and.resolveTo(null);

            GoalPoint.remove(goalPoint).then(() =>{
                expect(goalPoint.remove).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('findById()', () => {
        it('should throw error if id is null', () => {
            expect(GoalPoint.findById).toThrowError();
        });

        it('should return goal point by id', (done) => {
            const id = 'goal point id';
            const goalPoint = {};
            spyOn(GoalPoint._Model, 'findById').withArgs(id)
                .and.returnValue(FakeDocumentQuery(goalPoint));

            GoalPoint.findById(id).then(result => {
                expect(result).toBe(goalPoint);
                done();
            });
        });
    });
});