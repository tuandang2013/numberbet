const NorthNumber = require('../../../src/models/NorthNumber');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('NorthNumber', () => {
    describe('insert()', () => {
        it('should throw error if item is null', () => {
            expect(() => NorthNumber.insert()).toThrowError();
        });

        it('should call create() with item', (done) => {
            const item = {};
            const doc = {};
            spyOn(NorthNumber._Model, 'create').withArgs(item).and.resolveTo([ doc ]);

            NorthNumber.insert(item).then(result => expect(result).toEqual(doc)).then(done);
        });
    });

    describe('getByDate()', () => {
        it('should throw error when date undefined', () => {
            expect(NorthNumber.getByDate).toThrow();
        });

        it('should throw error when date is not a string', () => {
            expect(() => NorthNumber.getByDate(1)).toThrow();
        });

        it('should try find first item', (done) => {
            const date = '2020-12-13';
            const firstItem = new NorthNumber._Model();
            spyOn(NorthNumber._Model, "findOne").withArgs({ date })
                .and.returnValue(FakeDocumentQuery(firstItem));

            NorthNumber.getByDate(date).then(result => {
                expect(result).toBe(firstItem);
                done();
            });

            expect(NorthNumber._Model.findOne).toHaveBeenCalledWith({ date: date });
        });
    });

    describe('has()', () => {
        it('should throw if date is null', () => {
            const date = null;
            expect(() => NorthNumber.has(date)).toThrowError();
        });

        it('should throw if date is not a string', () => {
            const date = 0;
            expect(() => NorthNumber.has(date)).toThrowError();
        });

        it('should true if has data of date', (done) => {
            const date = '2020-10-10';
            const value = true;
            spyOn(NorthNumber._Model, 'exists')
                .and.resolveTo(value);

                NorthNumber.has(date).then(result => {
                expect(result).toBeTruthy();
                done();
            })
        });
    });

    describe('getById()', () => {
        it('should model query by id', (done) => {
            const id = 'id';
            const northNumber = {};
            spyOn(NorthNumber._Model, 'findById')
                .withArgs(id)
                .and.returnValue(FakeDocumentQuery(northNumber));
            
            NorthNumber.getById(id).then(result => {
                expect(result).toEqual(northNumber);
                done();
            })
        });
    });
});