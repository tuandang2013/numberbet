const userController = require('../../../../src/api/user/user.controller');
const userService = require('../../../../src/services/user');
const tokenService = require('../../../../src/services/token');
const goalPointService = require('../../../../src/services/goal-point');
const { of, throwError } = require('rxjs');
const User = require('../../../../src/models/User');
const claimService = require('../../../../src/services/claim.service');
const { REGISTERED } = require('../../../../src/models/Claim');
const recordService = require('../../../../src/services/record');

describe('UserController', () => {
    describe('login()', () => {
        it('should 400 error result if modelErrors not empty', (done) => {
            const modelErrors = {};

            userController.login({ modelErrors }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(result.data).toBeTruthy();
                done();
            });
        });

        it('should 400 result if email is null', (done) => {
            userController.login().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 result if password is null', (done) => {
            const email = 'email';
            userController.login({ email }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 result if user data is invalid', (done) => {
            const email = 'invalid email';
            const password = 'invalid password';
            spyOn(userService, 'findUserByEmailAndPassword').withArgs(email, password)
                .and.returnValue(of(null));

            userController.login({ email, password }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(userService.findUserByEmailAndPassword).toHaveBeenCalled();
                done();
            });
        });

        it('should 200 result if user data is valid', (done) => {
            const email = 'email';
            const password = 'password';
            const user = {};
            const pointInfo = {};
            spyOn(userService, 'findUserByEmailAndPassword').withArgs(email, password)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getPointInfoForUser').withArgs(user)
                .and.returnValue(of(pointInfo));

            userController.login({ email, password }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(userService.findUserByEmailAndPassword).toHaveBeenCalled();
                expect(goalPointService.getPointInfoForUser).toHaveBeenCalled();
                done();
            });
        });

        it('should 30 days expire if remember is set', (done) => {
            const email = 'email';
            const password = 'password';
            const remember = true;
            const user = { _id: 'user id' };
            const pointInfo = {};
            const token = 'token';
            spyOn(userService, 'findUserByEmailAndPassword').withArgs(email, password)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getPointInfoForUser').withArgs(user)
                .and.returnValue(of(pointInfo));
            spyOn(tokenService, 'getToken').withArgs({ id: user._id }, { expire: '30d' }).and.returnValue(token);

            userController.login({ email, password, remember }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.user.token).toBe(token);
                done();
            });
        });
    });

    describe('register()', () => {
        it('should 400 result if email is null', (done) => {
            userController.register().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 result if password is null', (done) => {
            const user = { email: 'email' };
            userController.register(user).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 result if user email is existing', (done) => {
            const user = { email: 'existingEmail', password: 'password' };
            spyOn(userService, 'emailExists').withArgs(user.email)
                .and.returnValue(of(true));

            userController.register(user).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(userService.emailExists).toHaveBeenCalled();
                done();
            });
        });

        it('should 400 result if insert user failed', (done) => {
            const invalidUser = { email: 'invalid email', password: 'invalid password' };
            spyOn(userService, 'emailExists').withArgs(invalidUser.email)
                .and.returnValue(of(false));
            spyOn(userService, 'hashPassword').withArgs(invalidUser.password)
                .and.returnValue(of({ hash: 'hashed password', salt: 'salt' }));
            spyOn(userService, 'insert')
                .and.returnValue(throwError('error'));

            userController.register(invalidUser).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(userService.insert).toHaveBeenCalled();
                done();
            });
        });

        it('should hash password before insert', (done) => {
            const user = { email: 'email', password: 'password' };
            const hashPwdResult = { hash: 'hashedPassword', salt: 'salt' };
            const insertedUser = {};
            spyOn(userService, 'emailExists').withArgs(user.email)
                .and.returnValue(of(false));
            spyOn(userService, 'insert').and.returnValue(of(insertedUser));
            spyOn(userService, 'hashPassword').withArgs(user.password)
                .and.returnValue(of(hashPwdResult));

            userController.register(user).subscribe(() => {
                expect(userService.hashPassword).toHaveBeenCalledWith(user.password);
                expect(userService.insert).toHaveBeenCalled();
                done();
            });
        });

        it('should 200 result with token if register success', (done) => {
            const user = { email: 'email', password: 'password' };
            const hashPwdResult = { hash: 'hashedPassword', salt: 'salt' };
            const insertedUser = { _id: 1 };
            const token = 'token';
            spyOn(userService, 'emailExists').withArgs(user.email)
                .and.returnValue(of(false));
            spyOn(userService, 'insert').and.returnValue(of(insertedUser));
            spyOn(userService, 'hashPassword').withArgs(user.password)
                .and.returnValue(of(hashPwdResult));
            spyOn(tokenService, 'getToken').withArgs({ id: insertedUser._id }, { expire: '1d' })
                .and.returnValue(token);
            spyOn(claimService, 'addRegisteredClaimForUser').and.returnValue(of([]));
            spyOn(claimService, 'getClaimsForUser').and.returnValue(of([]));
            spyOn(goalPointService, 'addDailyTestGoalPointEntryForUser').and.returnValue(of({}));

            userController.register(user).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.user.token).toBe(token);
                expect(userService.insert).toHaveBeenCalled();
                done();
            });
        });

        it('should add registered claim if register success', (done) => {
            const user = { email: 'email', password: 'password' };
            const hashPwdResult = { hash: 'hashedPassword', salt: 'salt' };
            const insertedUser = { _id: 1 };
            const token = 'token';
            const registeredClaim = { _id: 'claim id', name: REGISTERED };
            spyOn(userService, 'emailExists').withArgs(user.email)
                .and.returnValue(of(false));
            spyOn(userService, 'insert').and.returnValue(of(insertedUser));
            spyOn(userService, 'hashPassword').withArgs(user.password)
                .and.returnValue(of(hashPwdResult));
            spyOn(tokenService, 'getToken').withArgs({ id: insertedUser._id }, { expire: '1d' })
                .and.returnValue(token);
            spyOn(claimService, 'addRegisteredClaimForUser').withArgs(insertedUser)
                .and.returnValue(of([registeredClaim]));
            spyOn(claimService, 'getClaimsForUser').and.returnValue(of([registeredClaim]));
            spyOn(goalPointService, 'addDailyTestGoalPointEntryForUser').and.returnValue(of({}));

            userController.register(user).subscribe(result => {
                expect(claimService.addRegisteredClaimForUser).toHaveBeenCalled();
                expect(result.data.user.claims).toEqual([registeredClaim.name]);
                done();
            });
        });

        it('should add daily test goal point if register success', (done) => {
            const user = { email: 'email', password: 'password' };
            const hashPwdResult = { hash: 'hashedPassword', salt: 'salt' };
            const insertedUser = { _id: 1 };
            const token = 'token';
            const now = new Date();
            spyOn(userService, 'emailExists').withArgs(user.email)
                .and.returnValue(of(false));
            spyOn(userService, 'insert').and.returnValue(of(insertedUser));
            spyOn(userService, 'hashPassword').withArgs(user.password)
                .and.returnValue(of(hashPwdResult));
            spyOn(tokenService, 'getToken').withArgs({ id: insertedUser._id }, { expire: '1d' })
                .and.returnValue(token);
            spyOn(claimService, 'addRegisteredClaimForUser').and.returnValue(of([]));
            spyOn(claimService, 'getClaimsForUser').and.returnValue(of([]));
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(goalPointService, 'addDailyTestGoalPointEntryForUser').withArgs(insertedUser, now)
                .and.returnValue(of({ value: 100 }));

            userController.register(user).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(goalPointService.addDailyTestGoalPointEntryForUser).toHaveBeenCalled();
                expect(result.data.user.testGoalPoint).toBe(100);
                done();
            });
        });
    });

    describe('userInfo()', () => {
        it('should 200 result null if request user not found', (done) => {
            const req = {};

            userController.userInfo(req).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toBe(null);
                done();
            });
        });

        it('should 200 result if request user found', (done) => {
            const user = new User._Model({ email: 'email' });
            const req = { user };
            const pointInfo = {
                goalPoint: 123,
                testGoalPoint: 234
            };
            spyOn(goalPointService, 'getPointInfoForUser').withArgs(user)
                .and.returnValue(of(pointInfo));

            userController.userInfo(req).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({
                    email: user.email,
                    goalPoint: pointInfo.goalPoint,
                    testGoalPoint: pointInfo.testGoalPoint,
                    claims: user.claims.map(claim => claim.name)
                });
                done();
            });
        });

        it('should 500 result if get goal points error', (done) => {
            const user = new User._Model({ email: 'email' });
            const req = { user };
            spyOn(goalPointService, 'getPointInfoForUser').withArgs(user)
                .and.returnValue(throwError('error1'));

            userController.userInfo(req).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('getUsers()', () => {
        it('should return paged user data', (done) => {
            const pageNumber = 2;
            const pageSize = 5;
            const pagedUsers = {
                data: [{}, {}, {}],
                pager: {
                    pageNumber,
                    pageSize,
                    totalItems: 8,
                    totalPages: 2
                }
            };
            spyOn(userService, 'getUsers').withArgs({ pageSize, pageNumber })
                .and.returnValue(of(pagedUsers));
            spyOn(goalPointService, 'getPointInfoForUser').and.returnValue(of({}));
            spyOn(recordService, 'getRecordCountForUser').and.returnValue(of(0));

            userController.getUsers({ pageNumber, pageSize }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.data.length).toBe(pagedUsers.data.length);
                expect(result.data.pager).toBe(pagedUsers.pager);
                done();
            });
        });

        it('should include goal point info for each user', (done) => {
            const pageNumber = 2;
            const pageSize = 5;
            const pagedUsers = {
                data: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
                pager: {
                    pageNumber,
                    pageSize,
                    totalItems: 8,
                    totalPages: 2
                }
            };
            const goalPointResults = [
                { goalPoint: 1, testGoalPoint: 1 },
                { goalPoint: 2, testGoalPoint: 2 },
                { goalPoint: 3, testGoalPoint: 3 }
            ];
            spyOn(userService, 'getUsers').withArgs({ pageSize, pageNumber })
                .and.returnValue(of(pagedUsers));
            spyOn(goalPointService, 'getPointInfoForUser')
                .and.returnValues(...goalPointResults.map(value => of(value)));
            spyOn(recordService, 'getRecordCountForUser').and.returnValue(of(0));

            userController.getUsers({ pageNumber, pageSize }).subscribe(result => {
                expect(result.data.data.map(user => ({ goalPoint: user.goalPoint, testGoalPoint: user.testGoalPoint })))
                    .toEqual(goalPointResults);
                done();
            });
        });

        it('should include goal point info for each user', (done) => {
            const pageNumber = 2;
            const pageSize = 5;
            const pagedUsers = {
                data: [{ _id: 1 }, { _id: 2 }, { _id: 3 }],
                pager: {
                    pageNumber,
                    pageSize,
                    totalItems: 8,
                    totalPages: 2
                }
            };
            spyOn(userService, 'getUsers').withArgs({ pageSize, pageNumber })
                .and.returnValue(of(pagedUsers));
            spyOn(goalPointService, 'getPointInfoForUser')
                .and.returnValue(of({}));
            spyOn(recordService, 'getRecordCountForUser').and.returnValues(...[2, 3, 4].map(v => of(v)));

            userController.getUsers({ pageNumber, pageSize }).subscribe(result => {
                expect(result.data.data.map(user => user.recordCount))
                    .toEqual([2, 3, 4]);
                done();
            });
        });

        it('should 500 error result if get users if error', (done) => {
            spyOn(userService, 'getUsers').and.returnValue(throwError('error'));

            userController.getUsers({ query: {} }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('deleteUser()', () => {
        it('should 400 error result if id is null', (done) => {
            userController.deleteUser().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if user is not found', (done) => {
            const id = 'not found user';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            userController.deleteUser(id).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should delete user', (done) => {
            const id = 'user id';
            const user = {};
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(userService, 'deleteUser').withArgs(user)
                .and.returnValue(of(null));

            userController.deleteUser(id).subscribe(result => {
                expect(result.statusCode).toBe(200);
                done();
            });
        });
    });

    describe('exists()', () => {
        it('should 400 error result if email is null', (done) => {
            userController.exists().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should check email is exists', (done) => {
            const email = 'email';
            const exists = false;
            spyOn(userService, 'findUserByEmail').withArgs(email)
                .and.returnValue(of(exists));

            userController.exists(email).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({ exists });
                done();
            });
        });

        it('should 500 error result if error', (done) => {
            const email = 'email';
            spyOn(userService, 'findUserByEmail').withArgs(email)
                .and.returnValue(throwError('error'));

            userController.exists(email).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('updateUser()', () => {
        it('should 400 error result if user id not found', (done) => {
            const id = 'not found user';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            userController.updateUser({ id }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result is changed email is exists', (done) => {
            const changeEmail = 'change email';
            const user = { email: 'email', _id: 'user' };
            const existsUser = { email: changeEmail, _id: 'other user' };
            spyOn(userService, 'findUserById').withArgs(user._id)
                .and.returnValue(of(user));
            spyOn(userService, 'findUserByEmail').withArgs(changeEmail)
                .and.returnValue(of(existsUser));

            userController.updateUser({ id: user._id, email: changeEmail }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should update user', (done) => {
            const id = 'user id';
            const email = 'email';
            const phone = 'phone';
            const user = { _id: id };
            const updateUser = Object.assign(user, { email, phone });
            spyOn(userService, 'findUserById').withArgs(user._id)
                .and.returnValues(of(user), of({
                    _id: id,
                    email,
                    phone
                }));
            spyOn(userService, 'findUserByEmail').withArgs(email)
                .and.returnValue(of(null));
            spyOn(userService, 'update').withArgs(updateUser)
                .and.returnValue(of(updateUser));

            userController.updateUser({ id, email, phone }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                done();
            });
        });

        it('should update user claims', (done) => {
            const id = 'user id';
            const email = 'email';
            const phone = 'phone';
            const claims = ['claim 1', 'claim 2'];
            const claimIds = ['claim id 1', 'claim id 2'];
            const user = { _id: id };
            const updateUser = Object.assign(user, { email, phone });
            spyOn(userService, 'findUserById').withArgs(user._id)
                .and.returnValues(of(user), of({
                    _id: id,
                    email,
                    phone,
                    claims: claims.map(name => ({ name }))
                }));
            spyOn(userService, 'findUserByEmail').withArgs(email)
                .and.returnValue(of(null));
            spyOn(userService, 'update').withArgs(updateUser)
                .and.returnValue(of(updateUser));
            spyOn(claimService, 'getClaimIdsByNames').withArgs(claims)
                .and.returnValue(of(claimIds));

            userController.updateUser({ id, email, phone, claims }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(claimService.getClaimIdsByNames).toHaveBeenCalled();
                expect(result.data.data.claims).toEqual(claims)
                done();
            });
        });

        it('should 500 error result is update error', (done) => {
            const id = 'user id';
            const email = 'email';
            const phone = 'phone';
            const user = { _id: id };
            const updateUser = Object.assign(user, { email, phone });
            spyOn(userService, 'findUserById').withArgs(user._id)
                .and.returnValue(of(user));
            spyOn(userService, 'findUserByEmail').withArgs(email)
                .and.returnValue(of(user));
            spyOn(userService, 'update').withArgs(updateUser)
                .and.returnValue(throwError('error'));

            userController.updateUser({ id, email, phone }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('changePassword()', () => {
        it('should 400 error result is user is not found', (done) => {
            const id = 'not found user';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            userController.changePassword(id, {}).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if old password is incorrect', (done) => {
            const id = 'user id';
            const user = {};
            const oldPassword = 'incorrect password';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(userService, 'checkPassword').withArgs(user, oldPassword)
                .and.returnValue(of(false));

            userController.changePassword(id, { oldPassword }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should change password', (done) => {
            const id = 'user id';
            const user = {};
            const oldPassword = 'old password';
            const password = 'password';
            const hashPasswordResult = { hash: 'hashed password', salt: 'salt' };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(userService, 'checkPassword').withArgs(user, oldPassword)
                .and.returnValue(of(true));
            spyOn(userService, 'hashPassword').withArgs(password)
                .and.returnValue(of(hashPasswordResult));
            spyOn(userService, 'update').withArgs(
                Object.assign(user, { hashedPassword: hashPasswordResult.hash, salt: hashPasswordResult.salt })
            ).and.returnValue(of(user));

            userController.changePassword(id, { oldPassword, password }).subscribe(result => {
                expect(userService.hashPassword).toHaveBeenCalled();
                expect(userService.update).toHaveBeenCalled();
                expect(result.statusCode).toBe(200);
                done();
            });
        });

        it('should 500 error result if error', (done) => {
            const id = 'user id';
            const user = {};
            const oldPassword = 'old password';
            const password = 'password';
            const hashPasswordResult = { hash: 'hashed password', salt: 'salt' };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(userService, 'checkPassword').withArgs(user, oldPassword)
                .and.returnValue(of(true));
            spyOn(userService, 'hashPassword').withArgs(password)
                .and.returnValue(of(hashPasswordResult));
            spyOn(userService, 'update').withArgs(
                Object.assign(user, { hashedPassword: hashPasswordResult.hash, salt: hashPasswordResult.salt })
            ).and.returnValue(throwError('error'));

            userController.changePassword(id, { oldPassword, password }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('getUserDetails()', () => {
        it('should 404 error result if user is not found', (done) => {
            const id = 'not found user';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            userController.getUserDetails(id).subscribe(result => {
                expect(result.statusCode).toBe(404);
                done();
            });
        });

        it('should return user details', (done) => {
            const id = 'user id';
            const user = { _id: id, email: 'email', phone: 'phone', claims: [] };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));

            userController.getUserDetails(id).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({ id, email: user.email, phone: user.phone, claims: [] });
                done();
            });
        });
    });

    describe('adminChangePassword()', () => {
        it('should 400 error result is user is not found', (done) => {
            const id = 'not found user';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            userController.adminChangePassword(id, {}).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should change password', (done) => {
            const id = 'user id';
            const user = {};
            const password = 'password';
            const hashPasswordResult = { hash: 'hashed password', salt: 'salt' };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(userService, 'hashPassword').withArgs(password)
                .and.returnValue(of(hashPasswordResult));
            spyOn(userService, 'update').withArgs(
                Object.assign(user, { hashedPassword: hashPasswordResult.hash, salt: hashPasswordResult.salt })
            ).and.returnValue(of(user));

            userController.adminChangePassword(id, { password }).subscribe(result => {
                expect(userService.hashPassword).toHaveBeenCalled();
                expect(userService.update).toHaveBeenCalled();
                expect(result.statusCode).toBe(200);
                done();
            });
        });

        it('should 500 error result if error', (done) => {
            const id = 'user id';
            const user = {};
            const password = 'password';
            const hashPasswordResult = { hash: 'hashed password', salt: 'salt' };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(userService, 'hashPassword').withArgs(password)
                .and.returnValue(of(hashPasswordResult));
            spyOn(userService, 'update').withArgs(
                Object.assign(user, { hashedPassword: hashPasswordResult.hash, salt: hashPasswordResult.salt })
            ).and.returnValue(throwError('error'));

            userController.adminChangePassword(id, { password }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });
});