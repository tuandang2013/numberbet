const taskController = require('../../../../src/api/tasks/task.controller');
const processRecordTask = require('../../../../src/tasks/process-record');
const { of, throwError } = require('rxjs');

describe('TaskController', () => {
    describe('processRecord()', () => {
        it('should 200 result if task run success', (done) => {
            spyOn(processRecordTask, 'task').and.returnValue(of(true));

            taskController.processRecord().subscribe(result => {
                expect(processRecordTask.task).toHaveBeenCalled();
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({ done: true });
                done();
            });
        });
        
        it('should 500 error result if task run failed', (done) => {
            spyOn(processRecordTask, 'task').and.returnValue(throwError('error'));

            taskController.processRecord().subscribe(result => {
                expect(processRecordTask.task).toHaveBeenCalled();
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });
});