const numberController = require('../../../../src/api/number/number.controller');
const sourceService = require('../../../../src/services/source');
const fakeSource = require('../../helpers/fakeSource');
const northNumberService = require('../../../../src/services/north-number');
const southNumberService = require('../../../../src/services/south-number');
const { of } = require('rxjs');

describe('NumberController', () => {
    describe('lastest()', () => {
        beforeEach(() => {
            numberController._source = fakeSource();
        });

        it('should check data exists', () => {
            spyOn(sourceService, 'has').and.returnValue(of());

            numberController.lastest();

            expect(sourceService.has).toHaveBeenCalled();
        });

        it('should request data if not exists', (done) => {
            const exists = false;
            const data = [{}, {}];
            spyOn(sourceService, 'has').and.returnValue(of(exists));
            spyOn(sourceService, 'getData').and.returnValue(of(data));

            numberController.lastest().subscribe(response => {
                expect(response.statusCode).toEqual(200);
                expect(response.data).toBeTruthy();
                expect(sourceService.getData).toHaveBeenCalled();
                done();
            });
        });

        it('should request number if exists', (done) => {
            const exists = true;
            const source = { north: { id: 'north id' }, south: { id: 'south id' } };
            const southNumber = { numbers: 1 };
            const northNumber = { numbers: [2, 3, 4] };
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(sourceService, 'has').and.returnValue(of(exists));
            spyOn(northNumberService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(northNumber));
            spyOn(southNumberService, 'getById').withArgs(source.south.id)
                .and.returnValue(of(southNumber));

            numberController.lastest().subscribe(response => {
                expect(response.statusCode).toEqual(200);
                expect(response.data).toBeTruthy();
                expect(northNumberService.getById).toHaveBeenCalledWith(source.north.id);
                expect(southNumberService.getById).toHaveBeenCalledWith(source.south.id);
                done();
            })
        });
    });
})