const commonController = require('../../../../src/api/common/common.controller');

describe('CommonController', () => {
    describe('getCsrfToken()', () => {
        it('should return csrf token', (done) => {
            const token = 'token';
            const req = { csrfToken() { return token; } };

            commonController.getCsrfToken(req).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({ token });
                done();
            });
        });
    });
});