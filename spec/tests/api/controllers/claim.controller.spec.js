const claimController = require('../../../../src/api/claim/claim.controller');
const claimService = require('../../../../src/services/claim.service');
const { of } = require('rxjs');
const userService = require('../../../../src/services/user');

describe('ClaimController', () => {
    describe('getClaims()', () => {
        it('should return available claims', (done) => {
            const allClaims = [{ _id: 'claim 1', name: 'claim 1' }, { _id: 'claim 2', name: 'claim 2' }];
            spyOn(claimService, 'getAllClaims').and.returnValue(of(allClaims));

            claimController.getClaims().subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.length).toBe(allClaims.length);
                done();
            });
        });

        it('should order claim by name', (done) => {
            const allClaims = [{ _id: 1, name: 'claim b' }, { _id: 2, name: 'claim a' }];
            spyOn(claimService, 'getAllClaims').and.returnValue(of(allClaims));

            claimController.getClaims().subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.map(claim => claim.id)).toEqual([2, 1]);
                done();
            });
        });
    });

    describe('getUserClaims()', () => {
        it('should return user claims', (done) => {
            const user = { claims: [{ name: 'claim' }] };

            claimController.getUserClaims({ user }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.length).toBe(user.claims.length);
                done();
            });
        });

        it('should order claims by name', (done) => {
            const user = { claims: [{ name: 'claim b' }, { name: 'claim a' }] };

            claimController.getUserClaims({ user }).subscribe(result => {
                expect(result.data.map(claim => claim.name)).toEqual(['claim a', 'claim b']);
                done();
            });
        });
    });

    describe('getClaimsByUserId()', () => {
        it('should 400 error result if user id is null', (done) => {
            claimController.getClaimsByUserId().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if user if not found', (done) => {
            const id = 'not found user id';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            claimController.getClaimsByUserId(id).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(userService.findUserById).toHaveBeenCalled();
                done();
            });
        });

        it('should return user claims if found', (done) => {
            const id = 'user id';
            const user = {};
            const claims = [{ name: 'claim 1' }, { name: 'claim 2' }];
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(claimService, 'getClaimsForUser').withArgs(user)
                .and.returnValue(of(claims));

            claimController.getClaimsByUserId(id).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.length).toBe(claims.length);
                expect(claimService.getClaimsForUser).toHaveBeenCalled();
                done();
            });
        });

        it('should order claims by name', (done) => {
            const id = 'user id';
            const user = {};
            const claims = [{ name: 'claim b' }, { name: 'claim a' }];
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(claimService, 'getClaimsForUser').withArgs(user)
                .and.returnValue(of(claims));

            claimController.getClaimsByUserId(id).subscribe(result => {
                expect(result.data.map(claim => claim.name)).toEqual(['claim a', 'claim b']);
                done();
            });
        });
    });

    describe('addClaimsForUser()', () => {
        it('should 400 error result if id is null', (done) => {
            claimController.addClaimsForUser().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if claims is null', (done) => {
            const id = 'id';

            claimController.addClaimsForUser(id).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if user is not found', (done) => {
            const id = 'not found id';
            const claims = [];
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            claimController.addClaimsForUser(id, claims).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 200 add claims for user', (done) => {
            const id = 'user id';
            const claims = ['Claim 1', 'Claim 2'];
            const user = {};
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(claimService, 'addClaimForUser').and.callFake((user, claim) => of({ _id: claim, name: claim }));

            claimController.addClaimsForUser(id, claims).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(claimService.addClaimForUser).toHaveBeenCalledTimes(2);
                done();
            });
        });
    });

    describe('removeClaimsForUser()', () => {
        it('should 400 error result if id is null', (done) => {
            claimController.removeClaimsForUser().subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if claims is null', (done) => {
            const userId = 'user id';

            claimController.removeClaimsForUser(userId).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 error result if user is not found', (done) => {
            const userId = 'not found user id';
            const claims = [];
            spyOn(userService, 'findUserById').withArgs(userId)
                .and.returnValue(of(null));

            claimController.removeClaimsForUser(userId, claims).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should remove claims for user', (done) => {
            const userId = 'user id';
            const claims = ['claim 1', 'claim 2'];
            const user = { claims };
            spyOn(userService, 'findUserById').withArgs(userId)
                .and.returnValue(of(user));
            spyOn(claimService, 'removeClaimForUser').and.callFake((user, claim) => of(user.claims));

            claimController.removeClaimsForUser(userId, claims).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(claimService.removeClaimForUser).toHaveBeenCalledTimes(claims.length);
                done();
            });
        });
    });
});