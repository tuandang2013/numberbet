const { of, throwError } = require('rxjs');
const goalController = require('../../../../src/api/goal/goal.controller');
const goalPointService = require('../../../../src/services/goal-point');
const userService = require('../../../../src/services/user');
const config = require('../../../../src/config');

describe('GoalController', () => {
    describe('getGoals()', () => {
        it('should 500 result if get goal point error', (done) => {
            const user = {};
            spyOn(goalPointService, 'getGoalPointEntriesForUser').and.returnValue(throwError('error'));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser').and.returnValue(of([]));

            goalController.getGoals({ user }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });

        it('should 500 result if get test goal point error', (done) => {
            const user = {};
            spyOn(goalPointService, 'getGoalPointEntriesForUser').and.returnValue(of([]));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser').and.returnValue(throwError('error'));

            goalController.getGoals({ user }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });

        it('should get goal point and test goal point entires for first page', (done) => {
            const user = {};
            const goalPointEntries = { data: [{}, {}, {}], pager: {} };
            const testGoalPointEntries = { data: [{}], pager: {} };
            spyOn(goalPointService, 'getGoalPointEntriesForUser').withArgs(user, { pageNumber: 1, pageSize: config.DEFAULT_PAGESIZE })
                .and.returnValue(of(goalPointEntries));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser').withArgs(user, { pageNumber: 1, pageSize: config.DEFAULT_PAGESIZE })
                .and.returnValue(of(testGoalPointEntries));

            goalController.getGoals({ user }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.goalPoint.length).toBe(goalPointEntries.length);
                expect(result.data.testGoalPoint.length).toBe(testGoalPointEntries.length);
                done();
            });
        });
    });

    describe('getGoalPoints()', () => {
        it('should 200 result with paged entries data', (done) => {
            const pageSize = 1;
            const pageNumber = 3;
            const user = {};
            const goalPoints = { data: [], pager: {} };
            spyOn(goalPointService, 'getGoalPointEntriesForUser').withArgs(user, { pageSize, pageNumber })
                .and.returnValue(of(goalPoints));

            goalController.getGoalPoints({ user, query: { pageSize, pageNumber } }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toBe(goalPoints);
                expect(goalPointService.getGoalPointEntriesForUser).toHaveBeenCalled();
                done();
            });
        });

        it('should 500 result if get entries is error', (done) => {
            const pageSize = 1;
            const pageNumber = 1;
            const user = {};
            spyOn(goalPointService, 'getGoalPointEntriesForUser').withArgs(user, { pageSize, pageNumber })
                .and.returnValue(throwError('error'));

            goalController.getGoalPoints({ user, query: { pageSize, pageNumber } }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                expect(goalPointService.getGoalPointEntriesForUser).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getTestGoalPoints()', () => {
        it('should 200 result with paged entries data', (done) => {
            const pageSize = 1;
            const pageNumber = 3;
            const user = {};
            const testGoalPoints = { data: [], pager: {} };
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser').withArgs(user, { pageSize, pageNumber })
                .and.returnValue(of(testGoalPoints));

            goalController.getTestGoalPoints({ user, query: { pageSize, pageNumber } }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toBe(testGoalPoints);
                expect(goalPointService.getTestGoalPointEntriesForUser).toHaveBeenCalled();
                done();
            });
        });

        it('should 500 result if get entries is error', (done) => {
            const pageSize = 1;
            const pageNumber = 1;
            const user = {};
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser').withArgs(user, { pageSize, pageNumber })
                .and.returnValue(throwError('error'));

            goalController.getTestGoalPoints({ user, query: { pageSize, pageNumber } }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                expect(goalPointService.getTestGoalPointEntriesForUser).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getUserGoals()', () => {
        it('should 404 error result if user is not found', (done) => {
            const id = 'not found user id';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            goalController.getUserGoals(id).subscribe(result => {
                expect(result.statusCode).toBe(404);
                done();
            });
        });

        it('should include goal point first page for user', (done) => {
            const id = 'user id';
            const user = {};
            const goalPoints = { data: [{}, {}, {}], pager: {} };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getGoalPointEntriesForUser')
                .withArgs(user, { pageNumber: 1, pageSize: config.DEFAULT_PAGESIZE })
                .and.returnValue(of(goalPoints));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser')
                .and.returnValue(of([]));

            goalController.getUserGoals(id).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.goalPoint).toBe(goalPoints);
                done();
            });
        });

        it('should include test goal point first page for user', (done) => {
            const id = 'user id';
            const user = {};
            const testGoalPoint = { data: [{}, {}, {}], pager: {} };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser')
                .withArgs(user, { pageNumber: 1, pageSize: config.DEFAULT_PAGESIZE })
                .and.returnValue(of(testGoalPoint));
            spyOn(goalPointService, 'getGoalPointEntriesForUser')
                .and.returnValue(of([]));

            goalController.getUserGoals(id).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.testGoalPoint).toBe(testGoalPoint);
                done();
            });
        });

        it('should 500 error result if get data is error', (done) => {
            const id = 'user id';
            const user = {};
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser')
                .withArgs(user, { pageNumber: 1, pageSize: config.DEFAULT_PAGESIZE })
                .and.returnValue(throwError('error'));
            spyOn(goalPointService, 'getGoalPointEntriesForUser')
                .and.returnValue(throwError('error'));

            goalController.getUserGoals(id).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('getUserGoalPoints()', () => {
        it('should 404 error result if user is not found', (done) => {
            const id = 'not found user id';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            goalController.getUserGoalPoints(id).subscribe(result => {
                expect(result.statusCode).toBe(404);
                done();
            });
        });

        it('should return paged goal point for user', (done) => {
            const id = 'user id';
            const user = {};
            const pageNumber = 2;
            const pageSize = 3;
            const goalPoint = { data: [{}, {}], pager: {} };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getGoalPointEntriesForUser')
                .withArgs(user, { pageNumber, pageSize })
                .and.returnValue(of(goalPoint));

            goalController.getUserGoalPoints(id, { pageNumber, pageSize }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toBe(goalPoint);
                done();
            });
        });

        it('should 500 error result if get data is error', (done) => {
            const id = 'user id';
            const user = {};
            const pageNumber = 2;
            const pageSize = 3;
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getGoalPointEntriesForUser')
                .withArgs(user, { pageNumber, pageSize })
                .and.returnValue(throwError('error'));

            goalController.getUserGoalPoints(id, { pageNumber, pageSize }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('getUserTestGoalPoints()', () => {
        it('should 404 error result if user is not found', (done) => {
            const id = 'not found user id';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            goalController.getUserTestGoalPoints(id).subscribe(result => {
                expect(result.statusCode).toBe(404);
                done();
            });
        });

        it('should return paged test goal point for user', (done) => {
            const id = 'user id';
            const user = {};
            const pageNumber = 2;
            const pageSize = 3;
            const testGoalPoint = { data: [{}, {}], pager: {} };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser')
                .withArgs(user, { pageNumber, pageSize })
                .and.returnValue(of(testGoalPoint));

            goalController.getUserTestGoalPoints(id, { pageNumber, pageSize }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toBe(testGoalPoint);
                done();
            });
        });

        it('should 500 error result if get data is error', (done) => {
            const id = 'user id';
            const user = {};
            const pageNumber = 2;
            const pageSize = 3;
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getTestGoalPointEntriesForUser')
                .withArgs(user, { pageNumber, pageSize })
                .and.returnValue(throwError('error'));

            goalController.getUserTestGoalPoints(id, { pageNumber, pageSize }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });

    describe('checkoutForUser()', () => {
        it('should 404 error result is user is not found', done => {
            const id = 'not found user id';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            goalController.checkoutForUser(id).subscribe(result => {
                expect(result.statusCode).toBe(404);
                done();
            });
        });

        it('should 400 result is goal point amount is less than amount', done => {
            const id = 'user id';
            const user = {};
            const goalPointAmount = 100;
            const requestAmount = 101;
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getGoalPointAmountForUser').withArgs(user)
                .and.returnValue(of(goalPointAmount));

            goalController.checkoutForUser(id, { amount: requestAmount }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should add goal point entry for user', done => {
            const id = 'user id';
            const user = {};
            const goalPointAmount = 100;
            const requestAmount = 99;
            const addedGoalPoint = {};
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(goalPointService, 'getGoalPointAmountForUser').withArgs(user)
                .and.returnValue(of(goalPointAmount));
            spyOn(goalPointService, 'addGoalPointCheckoutForUser').withArgs(user, requestAmount)
                .and.returnValue(of(addedGoalPoint))

            goalController.checkoutForUser(id, { amount: requestAmount }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.data).toBe(addedGoalPoint);
                done();
            });
        });
    });
});