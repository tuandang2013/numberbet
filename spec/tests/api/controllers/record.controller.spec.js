const recordController = require('../../../../src/api/record/record.controller');
const recordService = require('../../../../src/services/record');
const goalPointService = require('../../../../src/services/goal-point');
const userService = require('../../../../src/services/user');
const dateTimeHelper = require('../../../../src/services/date-helper');
const { of, throwError } = require('rxjs');
const { NORTH_AREA } = require('../../../../src/models/constants/areas');
const recordTypes = require('../../../../src/services/records/types');
const moment = require('moment');
const recordInfo = require('../../../../src/services/records/bet-record-info');

describe('RecordController', () => {
    describe('getInfo()', () => {

    });

    describe('getTodayInfo()', () => {
        it('should have only today channels if before end-time', (done) => {
            const now = new Date('2020-03-10 11:00');
            const dayOfWeek = moment(now).day();
            const recordableDate = new Date();
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'isBeforeEndTime').and.returnValue(true);
            spyOn(recordService, 'getRecordableDate').and.returnValue(recordableDate);

            recordController.getTodayInfo().subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({
                    ...recordInfo.betRecordInfo,
                    areas: [
                        { ...recordInfo.northBetRecordInfo, channels: recordInfo.northBetRecordInfo.channels[dayOfWeek] },
                        { ...recordInfo.southBetRecordInfo, channels: recordInfo.southBetRecordInfo.channels[dayOfWeek] }
                    ],
                    date: recordableDate
                });
                done();
            });
        });

        it('should have only tomorror channels if after end-time', (done) => {
            const now = new Date('2020-03-10 11:00');
            const dayOfWeek = moment(now).day();
            const recordableDate = new Date();
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'isBeforeEndTime').and.returnValue(false);
            spyOn(recordService, 'getRecordableDate').and.returnValue(recordableDate);

            recordController.getTodayInfo().subscribe(result => {
                const channelIndex = dayOfWeek + 1;
                expect(result.statusCode).toBe(200);
                expect(result.data).toEqual({
                    ...recordInfo.betRecordInfo,
                    areas: [
                        { ...recordInfo.northBetRecordInfo, channels: recordInfo.northBetRecordInfo.channels[channelIndex] },
                        { ...recordInfo.southBetRecordInfo, channels: recordInfo.southBetRecordInfo.channels[channelIndex] }
                    ],
                    date: recordableDate
                });
                done();
            });
        });
    });

    describe('getEntries()', () => {
        it('should empty array result if user dont have record', (done) => {
            const user = {};

            recordController.getEntries({ user }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.length).toBe(0);
                done();
            });
        });

        it('should 500 error result if get entries for user failed', (done) => {
            const recordId = 'record id';
            const user = { recordId };
            const error = 'error';
            spyOn(recordService, 'getRecordEntriesForUser').withArgs(user)
                .and.returnValue(throwError(error));

            recordController.getEntries({ user }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                expect(recordService.getRecordEntriesForUser).toHaveBeenCalledWith(user);
                done();
            });
        });

        it('should return record entries of user', (done) => {
            const recordId = 'record id';
            const user = { recordId };
            const entries = [{ createdDate: new Date() }, { createdDate: new Date() }];
            spyOn(recordService, 'getRecordEntriesForUser').withArgs(user)
                .and.returnValue(of(entries));

            recordController.getEntries({ user }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data).toBe(entries);
                expect(recordService.getRecordEntriesForUser).toHaveBeenCalledWith(user);
                done();
            });
        });

        it('should order entry desc by created date', (done) => {
            const recordId = 'record id';
            const user = { recordId };
            const entries = [{ id: 1, createdDate: new Date('2020-03-19 16:00') }, { id: 2, createdDate: new Date('2020-03-20 00:00') }];
            spyOn(recordService, 'getRecordEntriesForUser').withArgs(user)
                .and.returnValue(of(entries));

            recordController.getEntries({ user }).subscribe(result => {
                expect(result.data.map(entry => entry.id)).toEqual([2, 1]);
                done();
            });
        });
    });

    describe('createEntry()', () => {
        it('should 400 result if data is null', (done) => {
            const req = { user: {} };

            recordController.createEntry(req).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 result is model valiation failed', (done) => {
            const modelErrors = { error: 'error' };
            const req = { user: {}, body: {}, modelErrors };

            recordController.createEntry(req).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(result.data.error).toBe(modelErrors);
                done();
            });
        });

        it('should 500 result if create entry for user failed', (done) => {
            const entryDate = '2020-03-10';
            const req = {
                user: {},
                body: {
                    area: NORTH_AREA,
                    type: recordTypes[0].info.code,
                    point: recordTypes[0].config.pointRate[NORTH_AREA],
                    date: entryDate
                }
            };
            const error = 'error';
            spyOn(recordService, 'addRecordEntryForUser').withArgs(req.user, req.body)
                .and.returnValue(throwError(error));
            const now = new Date('2020-03-09');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'getEndDate').and.returnValue(now);

            recordController.createEntry(req).subscribe(result => {
                expect(result.statusCode).toBe(500);
                expect(recordService.addRecordEntryForUser).toHaveBeenCalledWith(req.user, req.body);
                done();
            });
        });

        it('should create record entry for user', (done) => {
            const user = {};
            const entry = {
                area: NORTH_AREA,
                type: recordTypes[0].info.code,
                point: recordTypes[0].config.pointRate.north,
                date: '2020-03-09'
            };
            spyOn(Date, 'now').and.returnValue(new Date('2020-03-09').getTime());
            spyOn(recordService, 'addRecordEntryForUser').withArgs(user, entry)
                .and.returnValue(of(entry));
            spyOn(goalPointService, 'getPointInfoForUser').withArgs(user)
                .and.returnValue(of(null));

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.success).toBeTruthy();
                expect(result.data.entry).toBe(entry);
                expect(recordService.addRecordEntryForUser).toHaveBeenCalledWith(user, entry);
                done();
            });
        });

        it('should response user point info if create record entry success', (done) => {
            const user = {};
            const entry = {
                area: NORTH_AREA,
                type: recordTypes[0].info.code,
                point: recordTypes[0].config.pointRate.north,
                date: '2020-03-09'
            };
            const pointInfo = {
                goalPoint: 1,
                testGoalPoint: 2
            };
            spyOn(Date, 'now').and.returnValue(new Date('2020-03-09').getTime());
            spyOn(recordService, 'addRecordEntryForUser').withArgs(user, entry)
                .and.returnValue(of(entry));
            spyOn(goalPointService, 'getPointInfoForUser').withArgs(user)
                .and.returnValue(of(pointInfo));

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.data.pointInfo).toBe(pointInfo);
                done();
            });
        });

        it('should 400 result if channel is empty and area is not north', (done) => {
            const user = {};
            const entry = {};
            spyOn(recordService, 'addRecordEntryForUser');

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(recordService.addRecordEntryForUser).not.toHaveBeenCalled();
                done();
            });
        });

        it('should 400 result if type is not found', (done) => {
            const type = 'not found record type';
            const user = {};
            const entry = { type, area: NORTH_AREA };
            spyOn(recordService, 'addRecordEntryForUser');

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(recordService.addRecordEntryForUser).not.toHaveBeenCalled();
                done();
            });
        });

        it('should 400 result if point is not multiple with point rate', (done) => {
            const recordType = recordTypes[0];
            const type = recordType.info.code;
            const invalidPoint = 1;
            const user = {};
            const entry = { type, area: NORTH_AREA, point: invalidPoint };
            spyOn(recordService, 'addRecordEntryForUser');

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(recordService.addRecordEntryForUser).not.toHaveBeenCalled();
                done();
            });
        });

        it('should 400 result if date is before now', (done) => {
            const invalidDate = '2020-03-08';
            const recordType = recordTypes[0];
            const type = recordType.info.code;
            const point = 2 * recordType.config.pointRate[NORTH_AREA];
            const user = {};
            const entry = { type, area: NORTH_AREA, point, date: invalidDate };
            const now = new Date('2020-03-09');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(recordService, 'addRecordEntryForUser');

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(recordService.addRecordEntryForUser).not.toHaveBeenCalled();
                done();
            });
        });

        it('should 400 result if date equal now but now is after end hour', (done) => {
            const invalidDate = '2020-03-09';
            const recordType = recordTypes[0];
            const type = recordType.info.code;
            const point = 2 * recordType.config.pointRate[NORTH_AREA];
            const user = {};
            const entry = { type, area: NORTH_AREA, point, date: invalidDate };
            const now = new Date('2020-03-09 17:00');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'getEndDate').and.returnValue(now);
            spyOn(recordService, 'addRecordEntryForUser');

            recordController.createEntry({ user, body: entry }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(dateTimeHelper.getEndDate).toHaveBeenCalled();
                expect(recordService.addRecordEntryForUser).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('updateEntry()', () => {
        it('should 400 result if data is null', (done) => {
            const user = {};

            recordController.updateEntry({ user, body: {} }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 400 result is model validation failed', (done) => {
            const modelErrors = { error: 'error' };
            const req = { user: {}, body: {}, modelErrors };

            recordController.updateEntry(req).subscribe(result => {
                expect(result.statusCode).toBe(400);
                expect(result.data.error).toBe(modelErrors);
                done();
            });
        });

        it('should 400 result if data dont have id', (done) => {
            const user = {};
            const data = {};

            recordController.updateEntry({ user, body: { data } }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 500 result if update entry failed', (done) => {
            const user = {};
            const entry = { id: 'entry id' };
            const error = 'error';
            spyOn(recordService, 'alterRecordEntryForUser').withArgs(user, entry.id, entry)
                .and.returnValue(throwError(error));

            recordController.updateEntry({ user, body: { data: entry } }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                expect(recordService.alterRecordEntryForUser).toHaveBeenCalledWith(user, entry.id, entry);
                done();
            });
        })

        it('should update record entry', (done) => {
            const user = {};
            const entry = { id: 'entry id' };
            spyOn(recordService, 'alterRecordEntryForUser').withArgs(user, entry.id, entry)
                .and.returnValue(of(entry));

            recordController.updateEntry({ user, body: { data: entry } }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(recordService.alterRecordEntryForUser).toHaveBeenCalledWith(user, entry.id, entry);
                expect(result.data).toBe(entry);
                done();
            });
        });
    });

    describe('deleteEntry()', () => {
        it('should 400 result if id is null', (done) => {
            const user = {};

            recordController.deleteEntry({ user }).subscribe(result => {
                expect(result.statusCode).toBe(400);
                done();
            });
        });

        it('should 500 result if delete entry is failed', (done) => {
            const user = {};
            const id = 'entryid';
            const error = 'error';
            spyOn(recordService, 'deleteRecordEntryForUser').withArgs(user, id)
                .and.returnValue(throwError(error));

            recordController.deleteEntry({ user, id }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                expect(recordService.deleteRecordEntryForUser).toHaveBeenCalledWith(user, id);
                done();
            });
        });
    });

    describe('getRecordableDate()', () => {
        it('should return recordable-date', (done) => {
            const recordableDate = new Date('2020-03-15 00:00');
            spyOn(recordService, 'getRecordableDate').and.returnValue(recordableDate);

            recordController.getRecordableDate().subscribe(result => {
                expect(result.data).toEqual(recordableDate.toJSON());
                done();
            });
        });
    });

    describe('getHistory()', () => {
        it('should 200 result with history item for user', (done) => {
            const user = {};
            const histories = [{ createdDate: new Date() }, { createdDate: new Date() }];
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(of(histories));

            recordController.getHistory({ user }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.data.length).toBe(histories.length);
                done();
            });
        });

        it('should 500 result if retrieve history is error', (done) => {
            const user = {};
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(throwError('error'));

            recordController.getHistory({ user }).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });

        it('should order history result', (done) => {
            const user = {};
            const histories = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-16') }, { id: 3, createdDate: new Date('2020-03-18') }];
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(of(histories));

            recordController.getHistory({ user }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.data.map(history => history.id)).toEqual([3, 1, 2]);
                done();
            });
        });

        it('should paged history result', (done) => {
            const pageSize = 2;
            const pageNumber = 1;
            const user = {};
            const histories = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-16') }, { id: 3, createdDate: new Date('2020-03-18') }];
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(of(histories));

            recordController.getHistory({ user, query: { pageSize, pageNumber } }).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.data.length).toBe(pageSize);
                expect(result.data.data.map(i => i.id)).toEqual([3, 1]);
                done();
            });
        });
    });

    describe('getUserRecords()', () => {
        it('should 404 error result if user is not found', (done) => {
            const id = 'not found user id';
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(null));

            recordController.getUserRecords(id).subscribe(result => {
                expect(result.statusCode).toBe(404);
                done();
            });
        });

        it('should include paged histories for user', (done) => {
            const id = 'user id';
            const pageNumber = 1;
            const pageSize = 2;
            const histories = [{ _id: 1, createdDate: new Date() }, { _id: 2, createdDate: new Date() }, { _id: 3, createdDate: new Date() }];
            const user = { histories };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(of(histories));
            spyOn(recordService, 'getRecordEntriesForUser').and.returnValue(of([]));

            recordController.getUserRecords(id, {pageNumber, pageSize}).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.histories.data.length).toBe(pageSize);
                done();
            });
        });

        it('should include record entries for user', (done) => {
            const id = 'user id';
            const pageNumber = 1;
            const pageSize = 2;
            const histories = [{ _id: 1, createdDate: new Date() }, { _id: 2, createdDate: new Date() }, { _id: 3, createdDate: new Date() }];
            const entries = [{ _id: 1, createdDate: new Date() }, { _id: 2, createdDate: new Date() }, { _id: 3, createdDate: new Date() }];
            const user = { histories };
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(of(histories));
            spyOn(recordService, 'getRecordEntriesForUser').withArgs(user)
                .and.returnValue(of(entries));

            recordController.getUserRecords(id, {pageNumber, pageSize}).subscribe(result => {
                expect(result.statusCode).toBe(200);
                expect(result.data.entries.length).toBe(entries.length);
                done();
            });
        });

        it('should 500 error result if get data is error', (done) => {
            const id = 'user id';
            const pageNumber = 1;
            const pageSize = 2;
            const user = {};
            spyOn(userService, 'findUserById').withArgs(id)
                .and.returnValue(of(user));
            spyOn(recordService, 'getRecordHistoryForUser').withArgs(user)
                .and.returnValue(throwError('error'));
            spyOn(recordService, 'getRecordEntriesForUser').withArgs(user)
                .and.returnValue(throwError('error 2'));

            recordController.getUserRecords(id, {pageNumber, pageSize}).subscribe(result => {
                expect(result.statusCode).toBe(500);
                done();
            });
        });
    });
});