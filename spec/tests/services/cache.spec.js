const cacheService = require('../../../src/services/cache');
const redis = require('redis');

describe('CacheService', () => {
    describe('after initialized', () => {
        let cache;

        beforeEach(() => {
            cache = { get() { }, set() { } };
            spyOn(redis, 'createClient').and.returnValue(cache);
            cacheService.init();
        });

        describe('getItem()', () => {
            it('should throw error if key is null', () => {
                expect(cacheService.getItem).toThrowError();
            });

            it('should return item', (done) => {
                const key = 'key';
                const item = {};
                spyOn(cache, 'get')
                    .and.callFake((key, fn) => fn(null, item));

                cacheService.getItem(key).subscribe(result => {
                    expect(result).toBe(item);
                    done();
                });
            });
        });

        describe('setItem()', () => {
            it('should throw error if key is null', () => {
                expect(cacheService.setItem).toThrowError();
            });

            it('should throw error if item is null', () => {
                const key = 'key';
                expect(() => cacheService.setItem(key)).toThrowError();
            });

            it('should set item', (done) => {
                const key = 'key';
                const item = {};
                spyOn(cache, 'set')
                    .and.callFake((key, value, fn) => fn(null));

                cacheService.setItem(key, item).subscribe(() => {
                    expect(cacheService.getCache().set).toHaveBeenCalled();
                    done();
                });
            });
        });

        describe('tryGetItem()', () => {
            it('should throw error if key is null', () => {
                expect(cacheService.tryGetItem).toThrowError();
            });

            it('should return item if exist', (done) => {
                const key = 'key';
                const item = {};
                spyOn(cache, 'get')
                    .and.callFake((key, fn) => fn(null, item));

                cacheService.tryGetItem(key).subscribe(result => {
                    expect(result).toBe(item);
                    done();
                });
            });

            it('should factory method if item is not exist', (done) => {
                const key = 'key';
                const context = {
                    getValue: () => 0
                };
                spyOn(cache, 'get')
                    .and.callFake((key, fn) => fn(null, null));
                spyOn(cache, 'set')
                    .and.callFake((key, value, fn) => fn(null));
                spyOn(context, 'getValue').and.callThrough();

                cacheService.tryGetItem(key, context.getValue).subscribe(result => {
                    expect(result).toBe(0);
                    expect(context.getValue).toHaveBeenCalled();
                    expect(cacheService.getCache().set).toHaveBeenCalled();
                    done();
                });
            });
        });
    });

    describe('init()', () => {
        it('should create redis client', () => {
            spyOn(redis, 'createClient');

            cacheService.init();

            expect(redis.createClient).toHaveBeenCalled();
        });
    });
});