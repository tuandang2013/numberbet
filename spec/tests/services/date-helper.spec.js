const datetimeHelper = require('../../../src/services/date-helper');
const moment = require('moment');

describe('DateHelperService', () => {
    describe('formateDate()', () => {
        it('should throw error if date is null', () => {
            expect(datetimeHelper.formatDate).toThrowError();
        });

        it('should throw error if input is not a date', () => {
            const invalidDate = 0;

            expect(() => datetimeHelper.formatDate(invalidDate)).toThrowError();
        });

        it('should throw error if format is null', () => {
            expect(() => datetimeHelper.formatDate(new Date(), null)).toThrowError();
        });

        it('should valid result', () => {
            const date = new Date('2020-12-13 14:24:50');
            const format = 'YYYY-MM-D';
            const expectedResult = '2020-12-13';

            const result = datetimeHelper.formatDate(date, format);

            expect(result).toEqual(expectedResult);
        });
    });

    describe('getRequestableDateFormat()', () => {
        it('should throw error when date is null', () => {
            expect(() => datetimeHelper.getRequestableDateFormat(null)).toThrow();
        });

        it('should throw error if input is invalid date', () => {
            const invalidDate = 0;

            expect(() => datetimeHelper.getRequestableDateFormat(invalidDate)).toThrow();
        });

        it('should return excluded-time date', () => {
            const date = new Date('2020-10-01 17:00:00');
            const endHour = 16;
            const endMinute = 0;
            const expectedDate = '2020-10-01';

            const resultDate = datetimeHelper.getRequestableDateFormat(date, endHour, endMinute);

            expect(resultDate).toEqual(expectedDate);
        });

        it('should minus one date if before end date', ()=> {
            const date = new Date('2020-10-01 15:59:00');
            const expectedResult = '2020-09-30';
            const endHour = 16;
            const endMinute = 0;

            const result = datetimeHelper.getRequestableDateFormat(date, endHour, endMinute);

            expect(result).toEqual(expectedResult);
        });

        it('should not minus date if after end date', ()=> {
            const date = new Date('2020-10-01 17:00:00');
            const expectedResult = '2020-10-01';
            const endHour = 16;
            const endMinute = 0;

            const result = datetimeHelper.getRequestableDateFormat(date, endHour, endMinute);

            expect(result).toEqual(expectedResult);
        });
    });
    describe('getRequestableDate()', () => {
        it('should throw error when date is null', () => {
            expect(() => datetimeHelper.getRequestableDate(null)).toThrow();
        });

        it('should throw error if input is invalid date', () => {
            const invalidDate = 0;

            expect(() => datetimeHelper.getRequestableDate(invalidDate)).toThrow();
        });

        it('should return excluded-time date', () => {
            const date = new Date('2020-10-01 17:00:00');
            const endHour = 16;
            const endMinute = 0;
            const expectedDate = '2020-10-01';

            const result = datetimeHelper.getRequestableDate(date, endHour, endMinute);

            const resultDateFormat = moment(result).format('YYYY-MM-DD');
            expect(resultDateFormat).toEqual(expectedDate);
        });

        it('should minus one date if before end date', ()=> {
            const date = new Date('2020-10-01 15:59:00');
            const expectedResult = '2020-09-30';
            const endHour = 16;
            const endMinute = 0;

            const result = datetimeHelper.getRequestableDate(date, endHour, endMinute);

            const resultDateFormat = moment(result).format('YYYY-MM-DD');
            expect(resultDateFormat).toEqual(expectedResult);
        });

        it('should not minus date if after end date', ()=> {
            const date = new Date('2020-10-01 17:00:00');
            const expectedResult = '2020-10-01';
            const endHour = 16;
            const endMinute = 0;

            const result = datetimeHelper.getRequestableDateFormat(date, endHour, endMinute);

            const resultDateFormat = moment(result).format('YYYY-MM-DD');
            expect(resultDateFormat).toEqual(expectedResult);
        });
    });

    describe('getDateString()', () => {
        it('should throw error if date is null', () => {
            expect(datetimeHelper.getDateString).toThrowError();
        });

        it('should return valid format result', () => {
            const dateString = '2020-10-10 10:10:10';
            const date = new Date(dateString);
            const expectResult = '2020-10-10';

            const result = datetimeHelper.getDateString(date);

            expect(result).toEqual(expectResult);
        });
    });

    describe('getEndDate()', () => {
        it('should return valid result', () => {
            const date = new Date('2020-10-03 17:08:10');
            const expectedResult = '2020-10-03 16:00:00';
            const endHour = 16, endMinute = 0;

            const result = datetimeHelper.getEndDate(date, endHour, endMinute);

            expect(result.format('YYYY-MM-DD HH:mm:ss')).toEqual(expectedResult);
        });
    });
});