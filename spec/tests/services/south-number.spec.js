const southNumberService = require('../../../src/services/south-number');
const SouthNumber = require('../../../src/models/SouthNumber');
const sourceService = require('../../../src/services/source');
const { of } = require('rxjs');

describe('SouthNumberService', () => {
    describe('lastest()', () => {
        it('should be today south number data if exist', (done) => {
            const todaySouthNumberData = {};
            spyOn(SouthNumber, 'has').and.resolveTo(true);
            spyOn(SouthNumber, 'getByDate').and.resolveTo(todaySouthNumberData);

            southNumberService.lastest().subscribe(result => {
                expect(result).toEqual(todaySouthNumberData);
                done();
            });
        });

        it('should request new data if today data is not exist', (done) => {
            const exist = false;
            const data = {};
            spyOn(SouthNumber, 'has').and.resolveTo(exist);
            spyOn(sourceService, 'getSouthNumber').and.returnValue(of(data));

            southNumberService.lastest().subscribe(() => {
                expect(sourceService.getSouthNumber).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getById()', () => {
        it('should throw error if id is null', () => {
            expect(southNumberService.getById).toThrowError();
        });

        it('should query get by id model', (done) => {
            const id = 'id';
            const southNumber = {};
            spyOn(SouthNumber, 'getById')
                .withArgs(id)
                .and.resolveTo(southNumber);

            southNumberService.getById(id).subscribe(result => {
                expect(result).toEqual(southNumber);
                done();
            });
        });
    });
});