const tokenService = require('../../../src/services/token');
const { of } = require('rxjs');
const { catchError } = require('rxjs/operators');
const jwt = require('jsonwebtoken');

describe('TokenService', () => {
    describe('verifyToken()', () => {
        it('should throw error if token is null', () => {
            expect(tokenService.verifyToken).toThrowError();
        });

        it('should error if verify token failed', (done) => {
            const token = 'token';
            const key = 'key';
            const error = { name: 'error', message: 'msg' };
            spyOn(jwt, 'verify')
                .and.callFake((t, k, fn) => fn(error));

            tokenService.verifyToken(token, { key }).pipe(
                catchError(err => {
                    expect(err).toEqual(error);
                    done();
                    return of();
                })
            ).subscribe();
        });

        it('should return decoded if verify token success', (done) => {
            const token = 'token';
            const key = 'key';
            const decoded = { data: 'data' };
            spyOn(jwt, 'verify').and.callFake((t, k, fn) => fn(null, decoded));

            tokenService.verifyToken(token, { key }).subscribe(result => {
                expect(result).toEqual(decoded);
                done();
            });
        });
    })
});