const goalPointService = require('../../../src/services/goal-point');
const GoalPoint = require('../../../src/models/GoalPoint');
const TestGoalPoint = require('../../../src/models/TestGoalPoint');
const User = require('../../../src/models/User');
const { AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON, CHECKOUT_REASON } = require('../../../src/models/constants/goal-point-entry-reasons');
const config = require('../../../src/config');
const moment = require('moment');
const messageService = require('../../../src/services/message.service');
const FakeDocumentQuery = require('../helpers/FakeDocumentQuery');

describe('GoalPointService', () => {
    describe('GoalPoint', () => {
        describe('createGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.createGoalPointForUser).toThrowError();
            });

            it('should throw error if user is not a User', () => {
                const user = 'invalid user';
                expect(() => goalPointService.createGoalPointForUser(user)).toThrowError();
            });

            it('should throw error if user already has goal point', () => {
                const user = new User._Model({ goalPointId: 'goal-point-id' });
                expect(() => goalPointService.createGoalPointForUser(user)).toThrowError();
            });

            it('should create goal point success', (done) => {
                const user = new User._Model();
                const goalPoint = {};
                spyOn(GoalPoint, 'createForUser').withArgs(user)
                    .and.resolveTo(goalPoint);

                goalPointService.createGoalPointForUser(user).subscribe(result => {
                    expect(result).toBe(goalPoint);
                    expect(GoalPoint.createForUser).toHaveBeenCalledWith(user);
                    done();
                });
            });
        });

        describe('getGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.getGoalPointForUser).toThrowError();
            });

            it('should throw error if user is not a User', () => {
                const user = 'invalid user';
                expect(() => goalPointService.getGoalPointForUser(user)).toThrowError();
            });

            it('should get goal point for user', (done) => {
                const user = new User._Model();
                const goalPoint = {};
                spyOn(GoalPoint, 'getForUser').withArgs(user)
                    .and.resolveTo(goalPoint);

                goalPointService.getGoalPointForUser(user).subscribe(result => {
                    expect(result).toBe(goalPoint);
                    expect(GoalPoint.getForUser).toHaveBeenCalledWith(user);
                    done();
                });
            });
        });

        describe('createGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.createUserGoalPoints).toThrowError();
            });

            it('should throw error if user is not a User', () => {
                const user = 'invalid user';
                expect(() => goalPointService.createUserGoalPoints(user)).toThrowError();
            });

            it('should return goal point and test goal point if user have it', (done) => {
                const user = new User._Model({
                    goalPointId: 'goal-point-id',
                    testGoalPointId: 'test-goal-point-id'
                });
                const goalPoint = {};
                const testGoalPoint = {};
                spyOn(GoalPoint, 'getForUser').withArgs(user)
                    .and.resolveTo(goalPoint);
                spyOn(TestGoalPoint, 'getForUser').withArgs(user)
                    .and.resolveTo(testGoalPoint);
                spyOn(GoalPoint, 'createForUser');
                spyOn(TestGoalPoint, 'createForUser');

                goalPointService.createUserGoalPoints(user).subscribe(([goalPointResult, testGoalPointResult]) => {
                    expect(goalPointResult).toBe(goalPoint);
                    expect(testGoalPointResult).toBe(testGoalPoint);
                    expect(GoalPoint.createForUser).not.toHaveBeenCalled();
                    expect(TestGoalPoint.createForUser).not.toHaveBeenCalled();
                    done();
                });
            });

            it('should create goal point and test goal point if user dont have it', (done) => {
                const user = new User._Model();
                const goalPoint = {};
                const testGoalPoint = {};
                spyOn(GoalPoint, 'createForUser').withArgs(user)
                    .and.resolveTo(goalPoint);
                spyOn(TestGoalPoint, 'createForUser').withArgs(user)
                    .and.resolveTo(testGoalPoint);

                goalPointService.createUserGoalPoints(user).subscribe(([goalPointResult, testGoalPointResult]) => {
                    expect(goalPointResult).toBe(goalPoint);
                    expect(testGoalPointResult).toBe(testGoalPoint);
                    done();
                });
            });
        });

        describe('addGoalPointEntryForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.addGoalPointEntryForUser).toThrowError();
            });

            it('should throw error if user is not User', () => {
                const user = {};

                expect(() => goalPointService.addGoalPointEntryForUser(user)).toThrowError();
            })

            it('should throw error if reason is null', () => {
                const user = new User._Model();

                expect(() => goalPointService.addGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if value is null', () => {
                const user = new User._Model();
                const reason = 'reason';

                expect(() => goalPointService.addGoalPointEntryForUser(user, reason)).toThrowError();
            });

            it('should add test goal point entry for user', (done) => {
                const goalPoint = new GoalPoint._Model({ _id: 'goal point id', entries: [] });
                const user = new User._Model({
                    goalPointId: goalPoint._id
                });
                const reason = 'reason';
                const value = 0;
                spyOn(GoalPoint, 'addEntryForUser').withArgs(user, { reason, value, description: undefined, entity: undefined })
                    .and.resolveTo({ reason, value });

                goalPointService.addGoalPointEntryForUser(user, reason, value).subscribe(result => {
                    expect(result).toEqual({
                        reason,
                        value
                    });
                    expect(GoalPoint.addEntryForUser).toHaveBeenCalledWith(user, { reason, value, description: undefined, entity: undefined });
                    done();
                });
            });

            it('should add goal point entry with description for user', (done) => {
                const goalPoint = new GoalPoint._Model({ _id: 'goal point id', entries: [] });
                const user = new User._Model({
                    goalPointId: goalPoint._id
                });
                const reason = 'reason';
                const value = 0;
                const description = 'description';
                spyOn(GoalPoint, 'addEntryForUser').withArgs(user, { reason, value, description, entity: undefined })
                    .and.resolveTo({ reason, value, description });

                goalPointService.addGoalPointEntryForUser(user, reason, value, description).subscribe(result => {
                    expect(result).toEqual({
                        reason,
                        value,
                        description
                    });
                    expect(GoalPoint.addEntryForUser).toHaveBeenCalledWith(user, { reason, value, description, entity: undefined });
                    done();
                });
            });

            it('should add goal point entry with entity for user', (done) => {
                const goalPoint = new GoalPoint._Model({ _id: 'goal point id', entries: [] });
                const user = new User._Model({
                    goalPointId: goalPoint._id
                });
                const reason = 'reason';
                const value = 0;
                const description = 'description';
                const entity = {};
                spyOn(GoalPoint, 'addEntryForUser').withArgs(user, { reason, value, description, entity })
                    .and.resolveTo({ reason, value, description, entity });

                goalPointService.addGoalPointEntryForUser(user, reason, value, description, entity).subscribe(result => {
                    expect(result).toEqual({
                        reason,
                        value,
                        description,
                        entity
                    });
                    expect(GoalPoint.addEntryForUser).toHaveBeenCalledWith(user, { reason, value, description, entity });
                    done();
                });
            });
        });

        describe('deleteGoalPointEntryForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.deleteGoalPointEntryForUser).toThrowError();
            });

            it('should throw error if user is not User', () => {
                const user = {};

                expect(() => goalPointService.deleteGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if id is null', () => {
                const user = new User._Model();

                expect(() => goalPointService.deleteGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if user dont have goal point', () => {
                const user = new User._Model();
                const id = 'id';

                expect(() => goalPointService.deleteGoalPointEntryForUser(user, id)).toThrowError();
            });

            it('should delete goal entry for user', (done) => {
                const goalPoint = new GoalPoint._Model();
                const user = new User._Model({
                    goalPointId: goalPoint._id
                });
                const id = 'delete entry id';
                spyOn(GoalPoint, 'deleteEntryForUser').withArgs(user, id)
                    .and.resolveTo();

                goalPointService.deleteGoalPointEntryForUser(user, id).subscribe(() => {
                    expect(GoalPoint.deleteEntryForUser).toHaveBeenCalledWith(user, id);
                    done();
                });
            });
        });

        describe('getGoalPointEntriesForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.getGoalPointEntriesForUser).toThrowError();
            });

            it('should throw error if user is invalid User', () => {
                const invalidUser = 0;

                expect(() => goalPointService.getGoalPointEntriesForUser(invalidUser)).toThrowError();
            });

            it('should return goal point entries', (done) => {
                const goalPointEntries = [{ createdDate: new Date() }];
                const user = new User._Model();
                spyOn(GoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(goalPointEntries);

                goalPointService.getGoalPointEntriesForUser(user).subscribe(result => {
                    expect(result.data.length).toEqual(goalPointEntries.length);
                    done();
                });
            });

            it('should return ordered desc by createdDate entries', (done) => {
                const goalPointEntries = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-18 00:00') }, { id: 3, createdDate: new Date('2020-03-18 00:01') }];
                const user = new User._Model();
                spyOn(GoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(goalPointEntries);

                goalPointService.getGoalPointEntriesForUser(user).subscribe(result => {
                    expect(result.data.map(gp => gp.id)).toEqual([3, 2, 1]);
                    done();
                });
            });

            it('should return paged goal point entries', (done) => {
                const pageNumber = 2;
                const pageSize = 2;
                const goalPointEntries = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-18 00:00') }, { id: 3, createdDate: new Date('2020-03-19 00:01') }, { id: 4, createdDate: new Date('2020-03-20 00:01') }, { id: 5, createdDate: new Date('2020-03-21 00:01') }];
                const user = new User._Model();
                spyOn(GoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(goalPointEntries);

                goalPointService.getGoalPointEntriesForUser(user, { pageNumber, pageSize }).subscribe(result => {
                    expect(result.data.length).toBe(2);
                    expect(result.data.map(gp => gp.id)).toEqual([3, 2]);
                    done();
                });
            });

            it('should include pager info in entries result', (done) => {
                const pageNumber = 2;
                const pageSize = 2;
                const goalPointEntries = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-18 00:00') }, { id: 3, createdDate: new Date('2020-03-18 00:01') }];
                const user = new User._Model();
                spyOn(GoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(goalPointEntries);

                goalPointService.getGoalPointEntriesForUser(user, { pageNumber, pageSize }).subscribe(result => {
                    expect(result.pager).toEqual({
                        pageNumber,
                        pageSize,
                        totalPages: 2,
                        totalItems: 3
                    });
                    expect(result.data.map(gp => gp.id)).toEqual([1]);
                    done();
                });
            });
        });

        describe('deleteGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.deleteGoalPointForUser).toThrowError();
            });

            it('should throw error is user is invalid', () => {
                const invalidUser = 0;

                expect(() => goalPointService.deleteGoalPointForUser(invalidUser)).toThrowError();
            });

            it('should delte goal point', (done) => {
                const user = new User._Model();
                spyOn(GoalPoint, 'removeForUser').withArgs(user)
                    .and.resolveTo(null);

                goalPointService.deleteGoalPointForUser(user).subscribe(() => {
                    expect(GoalPoint.removeForUser).toHaveBeenCalled();
                    done();
                });
            });
        });

        describe('getGoalPointAmountForUser', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.getGoalPointAmountForUser).toThrowError();
            });

            it('should throw error if user is invalid', () => {
                const invalidUser = 0;
                expect(() => goalPointService.getGoalPointAmountForUser(invalidUser)).toThrowError();
            });

            it('should 0 if user dont have goal point', done => {
                const user = new User._Model({});
                spyOn(GoalPoint._Model, 'findById')
                    .and.resolveTo(null);

                goalPointService.getGoalPointAmountForUser(user).subscribe(result => {
                    expect(result).toBe(0);
                    done();
                });
            });

            it('should return goal point amount for user', done => {
                const goalPointId = 'goal point id';
                const goalPoint = { total: 1000 };
                const user = new User._Model({ goalPointId });
                spyOn(GoalPoint._Model, 'findById').withArgs(goalPointId)
                    .and.returnValue(FakeDocumentQuery(goalPoint));

                goalPointService.getGoalPointAmountForUser(user).subscribe(result => {
                    expect(GoalPoint._Model.findById).toHaveBeenCalled();
                    expect(result).toBe(goalPoint.total);
                    done();
                });
            });
        });

        describe('addGoalPointCheckoutForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.addGoalPointCheckoutForUser).toThrowError();
            });

            it('should throw error if user is invalid', () => {
                const invalidUser = 0;

                expect(() => goalPointService.addGoalPointCheckoutForUser(invalidUser)).toThrowError();
            });

            it('should add checkout entry for user', done => {
                const now = new Date();
                const user = new User._Model();
                const amount = 100;
                const reason = CHECKOUT_REASON;
                const description = messageService.checkout(amount, now);
                const addedGoalPoint = {};
                spyOn(Date, 'now').and.returnValue(now.getTime());
                spyOn(GoalPoint, 'addEntryForUser').withArgs(user, {
                    value: -amount,
                    reason,
                    description,
                    entity: undefined
                }).and.resolveTo(addedGoalPoint);

                goalPointService.addGoalPointCheckoutForUser(user, amount).subscribe(result => {
                    expect(result).toBe(addedGoalPoint);
                    done();
                });
            });
        });
    });

    describe('TestGoalPoint', () => {
        describe('createTestGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.createTestGoalPointForUser).toThrowError();
            });

            it('should throw error if user is not a User', () => {
                const user = 'invalid user';
                expect(() => goalPointService.createTestGoalPointForUser(user)).toThrowError();
            });

            it('should throw error if user already has test goal point', () => {
                const user = new User._Model({ testGoalPointId: 'test-goal-point-id' });
                expect(() => goalPointService.createTestGoalPointForUser(user)).toThrowError();
            });

            it('should create test goal point success', (done) => {
                const user = new User._Model();
                const testGoalPoint = {};
                spyOn(TestGoalPoint, 'createForUser').withArgs(user)
                    .and.resolveTo(testGoalPoint);

                goalPointService.createTestGoalPointForUser(user).subscribe(result => {
                    expect(result).toBe(testGoalPoint);
                    expect(TestGoalPoint.createForUser).toHaveBeenCalledWith(user);
                    done();
                });
            });
        });

        describe('getTestGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.getTestGoalPointForUser).toThrowError();
            });

            it('should throw error if user is not a User', () => {
                const user = 'invalid user';
                expect(() => goalPointService.getTestGoalPointForUser(user)).toThrowError();
            });

            it('should get test goal point for user', (done) => {
                const user = new User._Model();
                const testGoalPoint = {};
                spyOn(TestGoalPoint, 'getForUser').withArgs(user)
                    .and.resolveTo(testGoalPoint);

                goalPointService.getTestGoalPointForUser(user).subscribe(result => {
                    expect(result).toBe(testGoalPoint);
                    expect(TestGoalPoint.getForUser).toHaveBeenCalledWith(user);
                    done();
                });
            });
        });

        describe('addTestGoalPointEntryForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.addTestGoalPointEntryForUser).toThrowError();
            });

            it('should throw error if user is not User', () => {
                const user = {};

                expect(() => goalPointService.addTestGoalPointEntryForUser(user)).toThrowError();
            })

            it('should throw error if reason is null', () => {
                const user = new User._Model();

                expect(() => goalPointService.addTestGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if value is null', () => {
                const user = new User._Model();
                const reason = 'reason';

                expect(() => goalPointService.addTestGoalPointEntryForUser(user, reason)).toThrowError();
            });

            it('should add test goal point entry for user', (done) => {
                const testGoalPoint = new TestGoalPoint._Model({ _id: 'test goal point id', entries: [] });
                const user = new User._Model({
                    testGoalPointId: testGoalPoint._id
                });
                const reason = 'reason';
                const value = 0;
                spyOn(TestGoalPoint, 'addEntryForUser').withArgs(user, { reason, value, description: undefined, entity: undefined })
                    .and.resolveTo({ reason, value });

                goalPointService.addTestGoalPointEntryForUser(user, reason, value).subscribe(result => {
                    expect(result).toEqual({
                        reason,
                        value
                    });
                    expect(TestGoalPoint.addEntryForUser).toHaveBeenCalledWith(user, { reason, value, description: undefined, entity: undefined });
                    done();
                });
            });

            it('should add test goal point entry with description for user', (done) => {
                const testGoalPoint = new TestGoalPoint._Model({ _id: 'test goal point id', entries: [] });
                const user = new User._Model({
                    testGoalPointId: testGoalPoint._id
                });
                const reason = 'reason';
                const value = 0;
                const description = 'description';
                spyOn(TestGoalPoint, 'addEntryForUser').withArgs(user, { reason, value, description, entity: undefined })
                    .and.resolveTo({ reason, value, description });

                goalPointService.addTestGoalPointEntryForUser(user, reason, value, description).subscribe(result => {
                    expect(result).toEqual({
                        reason,
                        value,
                        description
                    });
                    expect(TestGoalPoint.addEntryForUser).toHaveBeenCalledWith(user, { reason, value, description, entity: undefined });
                    done();
                });
            });

            it('should add test goal point entry with entity', (done) => {
                const testGoalPoint = new TestGoalPoint._Model({ _id: 'test goal point id', entries: [] });
                const user = new User._Model({
                    testGoalPointId: testGoalPoint._id
                });
                const reason = 'reason';
                const value = 0;
                const description = 'description';
                const entity = {};
                spyOn(TestGoalPoint, 'addEntryForUser').withArgs(user, { reason, value, description, entity })
                    .and.resolveTo({ reason, value, description, entity });

                goalPointService.addTestGoalPointEntryForUser(user, reason, value, description, entity).subscribe(result => {
                    expect(result).toEqual({
                        reason,
                        value,
                        description,
                        entity
                    });
                    expect(TestGoalPoint.addEntryForUser).toHaveBeenCalledWith(user, { reason, value, description, entity });
                    done();
                });

            });
        });

        describe('deleteTestGoalPointEntryForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.deleteTestGoalPointEntryForUser).toThrowError();
            });

            it('should throw error if user is not User', () => {
                const user = {};

                expect(() => goalPointService.deleteTestGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if id is null', () => {
                const user = new User._Model();

                expect(() => goalPointService.deleteTestGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if user dont have test goal point', () => {
                const user = new User._Model();
                const id = 'id';

                expect(() => goalPointService.deleteTestGoalPointEntryForUser(user, id)).toThrowError();
            });

            it('should delete test goal entry for user', (done) => {
                const testGoalPoint = new TestGoalPoint._Model();
                const user = new User._Model({
                    testGoalPointId: testGoalPoint._id
                });
                const id = 'delete entry id';
                spyOn(TestGoalPoint, 'deleteEntryForUser').withArgs(user, id)
                    .and.resolveTo();

                goalPointService.deleteTestGoalPointEntryForUser(user, id).subscribe(() => {
                    expect(TestGoalPoint.deleteEntryForUser).toHaveBeenCalledWith(user, id);
                    done();
                });
            });
        });

        describe('addDailyTestGoalPointEntryForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.addDailyTestGoalPointEntryForUser).toThrowError();
            });

            it('should throw error if user is not User', () => {
                const user = {};
                expect(() => goalPointService.addDailyTestGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if date is null', () => {
                const user = new User._Model();

                expect(() => goalPointService.addDailyTestGoalPointEntryForUser(user)).toThrowError();
            });

            it('should throw error if date is not Date', () => {
                const user = new User._Model();
                const date = 0;

                expect(() => goalPointService.addDailyTestGoalPointEntryForUser(user, date)).toThrowError();
            });

            it('should skip if daily test point is added', (done) => {
                const date = new Date();
                const entries = [{
                    reason: AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON,
                    createdDate: date
                }];
                const user = new User._Model();
                spyOn(TestGoalPoint, 'addEntryForUser');
                spyOn(TestGoalPoint, 'findEntryByDateForUser').withArgs(user, date)
                    .and.resolveTo(entries);

                goalPointService.addDailyTestGoalPointEntryForUser(user, date).subscribe(() => {
                    expect(TestGoalPoint.addEntryForUser).not.toHaveBeenCalled();
                    expect(TestGoalPoint.findEntryByDateForUser).toHaveBeenCalledWith(user, date);
                    done();
                });
            });

            it('should add daily test point diddn\'t added', (done) => {
                const date = new Date();
                const entries = [];
                const user = new User._Model();
                const dailyEntry = {
                    reason: AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON,
                    description: `You received ${config.INCREMENT_TESTPOINT} points for date(${moment(date).format('DD/MM/YYYY')})`,
                    createdDate: date,
                    value: config.INCREMENT_TESTPOINT
                };
                spyOn(TestGoalPoint, 'addEntryForUser').withArgs(user, dailyEntry)
                    .and.resolveTo(dailyEntry);
                spyOn(TestGoalPoint, 'findEntryByDateForUser').withArgs(user, date)
                    .and.resolveTo(entries);

                goalPointService.addDailyTestGoalPointEntryForUser(user, date).subscribe(result => {
                    expect(result).toBe(dailyEntry);
                    expect(TestGoalPoint.addEntryForUser).toHaveBeenCalledWith(user, dailyEntry);
                    done();
                });
            });

            it('should have description for added test goal point entry', (done) => {
                const description = 'expected description';
                const date = new Date();
                const entries = [];
                const user = new User._Model();
                const dailyEntry = {
                    reason: AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON,
                    description,
                    createdDate: date,
                    value: config.INCREMENT_TESTPOINT
                };
                spyOn(TestGoalPoint, 'addEntryForUser').withArgs(user, dailyEntry)
                    .and.resolveTo(dailyEntry);
                spyOn(TestGoalPoint, 'findEntryByDateForUser').withArgs(user, date)
                    .and.resolveTo(entries);
                spyOn(messageService, 'dailyTestGoalPoint').withArgs(config.INCREMENT_TESTPOINT, date)
                    .and.returnValue(description);

                goalPointService.addDailyTestGoalPointEntryForUser(user, date).subscribe(result => {
                    expect(messageService.dailyTestGoalPoint).toHaveBeenCalled();
                    done();
                });
            });
        });

        describe('getTestGoalPointEntriesForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.getTestGoalPointEntriesForUser).toThrowError();
            });

            it('should throw error if user is invalid User', () => {
                const invalidUser = 0;

                expect(() => goalPointService.getTestGoalPointEntriesForUser(invalidUser)).toThrowError();
            });

            it('should return test goal point entries', (done) => {
                const testGoalPointEntries = [{ createdDate: new Date() }];
                const user = new User._Model();
                spyOn(TestGoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(testGoalPointEntries);

                goalPointService.getTestGoalPointEntriesForUser(user).subscribe(result => {
                    expect(result.data.length).toEqual(testGoalPointEntries.length);
                    done();
                });
            });

            it('should return ordered desc by createdDate entries', (done) => {
                const goalPointEntries = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-18 00:00') }, { id: 3, createdDate: new Date('2020-03-18 00:01') }];
                const user = new User._Model();
                spyOn(TestGoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(goalPointEntries);

                goalPointService.getTestGoalPointEntriesForUser(user).subscribe(result => {
                    expect(result.data.map(gp => gp.id)).toEqual([3, 2, 1]);
                    done();
                });
            });

            it('should return paged goal point entries', (done) => {
                const pageNumber = 2;
                const pageSize = 2;
                const testGoalPointEntries = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-18 00:00') }, { id: 3, createdDate: new Date('2020-03-19 00:01') }, { id: 4, createdDate: new Date('2020-03-20 00:01') }, { id: 5, createdDate: new Date('2020-03-21 00:01') }];
                const user = new User._Model();
                spyOn(TestGoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(testGoalPointEntries);

                goalPointService.getTestGoalPointEntriesForUser(user, { pageNumber, pageSize }).subscribe(result => {
                    expect(result.data.length).toBe(2);
                    expect(result.data.map(gp => gp.id)).toEqual([3, 2]);
                    done();
                });
            });

            it('should include pager info in entries result', (done) => {
                const pageNumber = 2;
                const pageSize = 2;
                const testGoalPointEntries = [{ id: 1, createdDate: new Date('2020-03-17') }, { id: 2, createdDate: new Date('2020-03-18 00:00') }, { id: 3, createdDate: new Date('2020-03-18 00:01') }];
                const user = new User._Model();
                spyOn(TestGoalPoint, 'getEntriesForUser').withArgs(user)
                    .and.resolveTo(testGoalPointEntries);

                goalPointService.getTestGoalPointEntriesForUser(user, { pageNumber, pageSize }).subscribe(result => {
                    expect(result.pager).toEqual({
                        pageNumber,
                        pageSize,
                        totalPages: 2,
                        totalItems: 3
                    });
                    expect(result.data.map(gp => gp.id)).toEqual([1]);
                    done();
                });
            });
        });

        describe('deleteTestGoalPointForUser()', () => {
            it('should throw error if user is null', () => {
                expect(goalPointService.deleteTestGoalPointForUser).toThrowError();
            });

            it('should throw error is user is invalid', () => {
                const invalidUser = 0;

                expect(() => goalPointService.deleteTestGoalPointForUser(invalidUser)).toThrowError();
            });

            it('should delte goal point', (done) => {
                const user = new User._Model();
                spyOn(TestGoalPoint, 'removeForUser').withArgs(user)
                    .and.resolveTo(null);

                goalPointService.deleteTestGoalPointForUser(user).subscribe(() => {
                    expect(TestGoalPoint.removeForUser).toHaveBeenCalled();
                    done();
                });
            });
        });
    });

    describe('getPointInfoForUser()', () => {
        it('should throw error if user is null', () => {
            expect(goalPointService.getPointInfoForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => goalPointService.getPointInfoForUser(invalidUser)).toThrowError();
        });

        it('should return point info for user', (done) => {
            const user = new User._Model();
            const goalPoint = { total: 1 };
            const testGoalPoint = { total: 2 };
            spyOn(GoalPoint, 'getForUser').withArgs(user)
                .and.resolveTo(goalPoint);
            spyOn(TestGoalPoint, 'getForUser').withArgs(user)
                .and.resolveTo(testGoalPoint);

            goalPointService.getPointInfoForUser(user).subscribe(result => {
                expect(result).toEqual({
                    goalPoint: goalPoint.total,
                    testGoalPoint: testGoalPoint.total
                });
                expect(GoalPoint.getForUser).toHaveBeenCalled();
                expect(TestGoalPoint.getForUser).toHaveBeenCalled();
                done();
            });
        });

        it('should default zero point if point is not found', (done) => {
            const user = new User._Model();
            spyOn(GoalPoint, 'getForUser').withArgs(user)
                .and.resolveTo(null);
            spyOn(TestGoalPoint, 'getForUser').withArgs(user)
                .and.resolveTo(null);

            goalPointService.getPointInfoForUser(user).subscribe(result => {
                expect(result).toEqual({
                    goalPoint: 0,
                    testGoalPoint: 0
                });
                done();
            });
        });
    });

    describe('deleteGoalPointById()', () => {
        it('should throw error if id is null', () => {
            expect(goalPointService.deleteGoalPointById).toThrowError();
        });

        it('should delete goal point if found', (done) => {
            const goalPoint = {};
            const id = 'goal point id';
            spyOn(GoalPoint, 'findById').withArgs(id)
                .and.resolveTo(goalPoint);
            spyOn(GoalPoint, 'remove').withArgs(goalPoint)
                .and.resolveTo(null);

            goalPointService.deleteGoalPointById(id).subscribe(() => {
                expect(GoalPoint.remove).toHaveBeenCalled();
                done();
            });
        });

        it('should throw error if goalpoint is not found', (done) => {
            const id = 'not found goal point';
            spyOn(GoalPoint, 'findById').withArgs(id)
                .and.resolveTo(null);

            goalPointService.deleteGoalPointById(id).subscribe(null, (error) => {
                expect(error).toBeTruthy();
                done();
            });
        });
    });

    describe('deleteTestGoalPointById()', () => {
        it('should throw error if id is null', () => {
            expect(goalPointService.deleteTestGoalPointById).toThrowError();
        });

        it('should delete test goal point if found', (done) => {
            const testGoalPoint = {};
            const id = 'test goal point id';
            spyOn(TestGoalPoint, 'findById').withArgs(id)
                .and.resolveTo(testGoalPoint);
            spyOn(TestGoalPoint, 'remove').withArgs(testGoalPoint)
                .and.resolveTo(null);

            goalPointService.deleteTestGoalPointById(id).subscribe(() => {
                expect(TestGoalPoint.remove).toHaveBeenCalled();
                done();
            });
        });

        it('should throw error if test goal point is not found', (done) => {
            const id = 'not found test goal point';
            spyOn(TestGoalPoint, 'findById').withArgs(id)
                .and.resolveTo(null);

            goalPointService.deleteTestGoalPointById(id).subscribe(null, (error) => {
                expect(error).toBeTruthy();
                done();
            });
        });
    });
});