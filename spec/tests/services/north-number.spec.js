const northNumberService = require('../../../src/services/north-number');
const NorthNumber = require('../../../src/models/NorthNumber');
const sourceService = require('../../../src/services/source');
const { of } = require('rxjs');

describe('NorthNumberService', () => {
    describe('lastest()', () => {
        it('should be today south number data if exist', (done) => {
            const todaySouthNumberData = {};
            spyOn(NorthNumber, 'has').and.resolveTo(true);
            spyOn(NorthNumber, 'getByDate').and.resolveTo(todaySouthNumberData);

            northNumberService.lastest().subscribe(result => {
                expect(result).toEqual(todaySouthNumberData);
                done();
            });
        });

        it('should request new data if today data is not exist', (done) => {
            const exist = false;
            const data = {};
            spyOn(NorthNumber, 'has').and.resolveTo(exist);
            spyOn(sourceService, 'getNorthNumber').and.returnValue(of(data));

            northNumberService.lastest().subscribe(() => {
                expect(sourceService.getNorthNumber).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getById()', () => {
        it('should throw error if id is null', () => {
            expect(northNumberService.getById).toThrowError();
        });

        it('should query get by id model', (done) => {
            const id = 'id';
            const northNumber = {};
            spyOn(NorthNumber, 'getById')
                .withArgs(id)
                .and.resolveTo(northNumber);

            northNumberService.getById(id).subscribe(result => {
                expect(result).toEqual(northNumber);
                done();
            });
        });
    });
});