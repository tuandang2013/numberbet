const settingService = require('../../../src/services/setting.service');
const SettingModel = require('../../../src/models/Setting');

describe('SettingService', () => {
    describe('init()', () => {
        it('should call model initialization', () => {
            spyOn(SettingModel, 'init').and.resolveTo(true);

            settingService.init();

            expect(SettingModel.init).toHaveBeenCalled();
        });
    });
});