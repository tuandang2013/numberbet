const { of } = require('rxjs');
const sourceService = require('../../../src/services/source');
const Source = require('../../../src/models/Source');
const datetimeHelper = require('../../../src/services/date-helper');
const axios = require('axios');
const fakeSource = require('../helpers/fakeSource');

describe('SourceService', () => {

    describe('getSouthNumber()', () => {
    });

    describe('has()', () => {
        it('should throw error if date is null', () => {
            expect(sourceService.has).toThrowError();
        });

        it('should throw error if date is not a Date', () => {
            const invalidDate = 'a';
            expect(() => sourceService.has(invalidDate)).toThrowError();
        });

        it('should query model', (done) => {
            const date = new Date();
            const requestDate = datetimeHelper.formatDate(date);
            const exists = true;
            spyOn(Source, 'has').withArgs({ date: requestDate })
                .and.returnValue(of(exists));
            sourceService.has(date).subscribe(result => {
                expect(result).toEqual(exists);
                expect(Source.has).toHaveBeenCalledWith({ date: requestDate });
                done();
            })
        })
    });

    describe('getByDate()', () => {
        it('should throw error if date is null', () => {
            expect(sourceService.getByDate).toThrowError();
        });

        it('should throw error if date is invalid', () => {
            const invalidDate = 0;

            expect(() => sourceService.getByDate(invalidDate)).toThrowError();
        });

        it('should query model get by date', (done) => {
            const date = new Date();
            const requestDateFormat = '2020-12-13';
            const data = { north: {}, south: {} };
            spyOn(datetimeHelper, 'formatDate')
                .withArgs(date)
                .and.returnValue(requestDateFormat);
            spyOn(Source, 'getByDate').withArgs(requestDateFormat)
                .and.resolveTo(data);

            sourceService.getByDate(date).subscribe(result => {
                expect(result).toEqual(data);
                done();
            });

            expect(datetimeHelper.formatDate).toHaveBeenCalled();
        });

        it('should request data if data is not found', (done) => {
            const date = new Date();
            const requestDateFormat = '2020-12-13';
            const source = fakeSource();
            spyOn(datetimeHelper, 'formatDate')
                .withArgs(date)
                .and.returnValue(requestDateFormat);
            spyOn(Source, 'getByDate').withArgs(requestDateFormat)
                .and.resolveTo(null);
            spyOn(axios, 'get').and.resolveTo({ data: 'html' });

            sourceService.getByDate(date, source).subscribe(() => {
                expect(axios.get).toHaveBeenCalledWith(source.sections[0].dailyUrl(date));
                done();
            });

            expect(datetimeHelper.formatDate).toHaveBeenCalled();
        });

        it('should null if getted data date is not match', (done) => {
            const date = new Date();
            const requestDateFormat = '2020-12-13';
            const source = fakeSource();
            spyOn(datetimeHelper, 'formatDate')
                .withArgs(date)
                .and.returnValue(requestDateFormat);
            spyOn(Source, 'getByDate').withArgs(requestDateFormat)
                .and.resolveTo(null);
            spyOn(axios, 'get').and.resolveTo({ data: 'html' });
            spyOn(source.sections[0], 'process')
                .and.returnValue(of({ date: new Date('2020-12-14') }));

            sourceService.getByDate(date, source).subscribe(result => {
                expect(axios.get).toHaveBeenCalledWith(source.sections[0].dailyUrl(date));
                expect(result).toBeFalsy();
                done();
            });

            expect(datetimeHelper.formatDate).toHaveBeenCalled();
        });
    });

    describe('getData()', () => {
        let _source;

        beforeEach(() => {
            _source = fakeSource();
        });

        it('should throw error if date is null', () => {
            expect(sourceService.getData).toThrowError();
        });

        it('should throw error if date is invalid', () => {
            const invalidDate = 0;
            expect(() => sourceService.getData(invalidDate)).toThrowError();
        });

        it('should throw error if source is null', () => {
            const date = new Date();
            expect(() => sourceService.getData(date, null)).toThrowError();
        });

        it('should get already data if exist', (done) => {
            const source = _source;
            const date = new Date('2020-02-12 16:42:02');
            const requestDateFormat = '2020-02-12';
            const exists = true;
            const data = { north: {}, south: {} };
            spyOn(datetimeHelper, 'formatDate').withArgs(date)
                .and.returnValue(requestDateFormat);
            spyOn(Source, 'has').withArgs({ date: requestDateFormat })
                .and.resolveTo(exists);
            spyOn(Source, 'getByDate').withArgs(requestDateFormat)
                .and.resolveTo(data);

            sourceService.getData(date, source).subscribe(result => {
                expect(result).toEqual(data);
                expect(Source.getByDate).toHaveBeenCalledWith(requestDateFormat);
                done();
            });
        });

        it('should request new data if data don\'t exist', (done) => {
            const source = _source;
            const date = new Date('2020-02-12 16:42:02');
            const requestDateFormat = '2020-02-12';
            const exists = false;
            spyOn(datetimeHelper, 'formatDate').withArgs(date)
                .and.returnValue(requestDateFormat);
            spyOn(Source, 'has').withArgs({ date: requestDateFormat })
                .and.resolveTo(exists);
            spyOn(axios, 'get').and.resolveTo({ data: 'html' });

            sourceService.getData(date, source).subscribe(() => {
                expect(axios.get).toHaveBeenCalled();
                done();
            });
        });

        it('should request lastest data if date is not provided', (done) => {
            const source = _source;
            spyOn(axios, 'get').and.resolveTo({ data: 'html' });
            spyOn(source.sections[0], 'process').and.returnValue(of({ date: 'date' }));
            spyOn(Source, 'has').and.resolveTo(true);

            sourceService.getData(null, source).subscribe(() => {
                expect(axios.get).toHaveBeenCalled();
                done();
            });
        });

        it('should not process source if already has data same requested data [lastest]', (done) => {
            const source = _source;
            const alreadyHasDataDate = '2020-02-11';
            spyOn(Source, 'has').withArgs({ date: alreadyHasDataDate }).and.returnValue(of(true));
            spyOn(axios, 'get').and.resolveTo({ data: 'html' });
            spyOn(source.sections[0], 'process').and.returnValue(of({
                date: alreadyHasDataDate
            }));
            spyOn(source, 'process');

            sourceService.getData(null, source).subscribe(() => {
                expect(axios.get).toHaveBeenCalled();
                expect(Source.has).toHaveBeenCalled();
                expect(source.process).not.toHaveBeenCalled();
                done();
            });
        });

        xit('should catch error if request data error', (done) => {
            const source = _source;
            const date = new Date('2020-02-12 16:42:02');
            const requestDateFormat = '2020-02-12';
            const exists = false;
            spyOn(datetimeHelper, 'formatDate').withArgs(date)
                .and.returnValue(requestDateFormat);
            spyOn(Source, 'has').withArgs({ date: requestDateFormat })
                .and.resolveTo(exists);
            spyOn(axios, 'get').and.rejectWith('error');

            sourceService.getData(date, source).subscribe(null, error => {
                expect(error).toBeTruthy();
                expect(axios.get).toHaveBeenCalled();
                done();
            });
        });
    });
});