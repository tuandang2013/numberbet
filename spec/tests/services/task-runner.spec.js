const { of } = require('rxjs');
const taskRunner = require('../../../src/services/task-runner');

describe('TaskRunner', () => {
    it('should call task if task\'s lastRunTime is null', (done) => {
        const task = { info: {}, task() { } };
        spyOn(task, 'task').and.returnValue(of(null));

        taskRunner.run([task]).subscribe(null, null, () => {
            expect(task.task).toHaveBeenCalled();
            done();
        });
    });

    it('should skip task if before next run time', (done) => {
        const task = { info: { lastRunTime: Date.now(), nextRunTime: Date.now() + 10000 }, task() { } };
        spyOn(task, 'task');

        taskRunner.run([task]).subscribe(null, null, () => {
            expect(task.task).not.toHaveBeenCalled();
            done();
        });
    });

    it('should run task if after next run time', (done) => {
        const task = { info: { lastRunTime: Date.now(), nextRunTime: Date.now() - 10000 }, task() { } };
        const taskResult = {};
        spyOn(task, 'task').and.returnValue(of(taskResult));

        taskRunner.run([task]).subscribe(result => {
            expect(result).toBe(taskResult);
            expect(task.task).toHaveBeenCalled();
            done();
        });
    });
});