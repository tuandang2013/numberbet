const userService = require('../../../src/services/user');
const User = require('../../../src/models/User');
const bcrypt = require('bcrypt');
const goalPointService = require('../../../src/services/goal-point');
const recordService = require('../../../src/services/record');
const { of } = require('rxjs');
const pagerHelper = require('../../../src/helpers/pager.helper');
const config = require('../../../src/config');

describe('UserService', () => {
    describe('findUserByEmailAndPassword()', () => {
        it('should throw error if email is null', () => {
            expect(userService.findUserByEmailAndPassword).toThrowError();
        });

        it('should throw error if password is null', () => {
            const email = 'email';
            expect(() => userService.findUserByEmailAndPassword(email)).toThrowError();
        });

        it('should null if user by email not found', (done) => {
            const email = 'email';
            const password = 'password';
            spyOn(User, 'findByEmail').withArgs(email)
                .and.resolveTo(null);
            spyOn(bcrypt, 'hash').and.callFake((p, s, fn) => fn(null, ''));

            userService.findUserByEmailAndPassword(email, password).subscribe(result => {
                expect(result).toBeFalsy();
                expect(User.findByEmail).toHaveBeenCalled();
                done();
            });
        });

        it('should null if user password is invalid', (done) => {
            const email = 'email';
            const password = 'password';
            const user = { hashedPassword: 'hashed password', salt: '' };
            spyOn(User, 'findByEmail').withArgs(email)
                .and.resolveTo(user);
            spyOn(bcrypt, 'hash').and.callFake((p, s, fn) => fn(null, 'invalid hashed password'));

            userService.findUserByEmailAndPassword(email, password).subscribe(result => {
                expect(result).toBeFalsy();
                expect(User.findByEmail).toHaveBeenCalled();
                done();
            });
        })
    });

    describe('hashPassword()', () => {
        it('should throw error if password is null', () => {
            expect(userService.hashPassword).toThrowError();
        });

        it('should call genSalt and hash', (done) => {
            const password = 'password';
            const hashedPassword = 'hashed password';
            const salt = 'salt';
            spyOn(bcrypt, 'genSalt').and.callFake((n, fn) => fn(null, salt));
            spyOn(bcrypt, 'hash').and.callFake((pwd, s, fn) => fn(null, hashedPassword));

            userService.hashPassword(password).subscribe(result => {
                expect(bcrypt.genSalt).toHaveBeenCalled();
                expect(bcrypt.hash).toHaveBeenCalled();
                expect(result.hash).toBe(hashedPassword);
                expect(result.salt).toBe(salt);
                done();
            });
        });
    });

    describe('checkPassword()', () => {
        it('should throw error if user is null', () => {
            expect(userService.checkPassword).toThrowError();
        });

        it('should throw error if user don\'t have password & salt properties', () => {
            const user = {};
            expect(() => userService.checkPassword(user)).toThrowError();
        });

        it('should return false if password is null', (done) => {
            const user = { hashedPassword: '', salt: '' };

            userService.checkPassword(user).subscribe(result => {
                expect(result).toBeFalsy();
                done();
            });
        });

        it('should check hashed password', (done) => {
            const user = { hashedPassword: '', salt: '' };
            const password = 'hashed password';
            spyOn(bcrypt, 'hash').and.callFake((pwd, salt, fn) => fn(null, user.hashedPassword));

            userService.checkPassword(user, password).subscribe(result => {
                expect(result).toBeTruthy();
                expect(bcrypt.hash).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('insert()', () => {
        it('should throw error if user is null', () => {
            expect(userService.insert).toThrowError();
        });

        it('should throw error if user email is null', () => {
            const user = {};
            expect(() => userService.insert(user)).toThrowError();
        });

        it('should throw error if hashed password is null', () => {
            const user = { email: 'email' };
            expect(() => userService.insert(user)).toThrowError();
        });

        it('should throw error if password salt is null', () => {
            const user = { email: 'email', hashedPassword: 'hashed password' };
            expect(() => userService.insert(user)).toThrowError();
        });

        it('should create goal points and record for user if insert success', (done) => {
            const user = { email: 'email', hashedPassword: 'hashed password', salt: 'salt' };
            const insertedUser = { id: 1 };
            const goalPoints = [{}, {}];
            const record = {};
            spyOn(User, 'insert').withArgs(user)
                .and.resolveTo(insertedUser);
            spyOn(goalPointService, 'createUserGoalPoints').withArgs(insertedUser)
                .and.returnValue(of(goalPoints));
            spyOn(User, 'update').withArgs(insertedUser)
                .and.resolveTo(insertedUser);
            spyOn(recordService, 'createRecordForUser').withArgs(insertedUser)
                .and.returnValue(of(record));

            userService.insert(user).subscribe(result => {
                expect(result).toBe(insertedUser);
                expect(recordService.createRecordForUser).toHaveBeenCalledWith(insertedUser);
                expect(goalPointService.createUserGoalPoints).toHaveBeenCalledWith(insertedUser);
                expect(User.update).toHaveBeenCalledWith(insertedUser);
                done();
            });
        });
    });

    describe('emailExists()', () => {
        it('should throw error if email is null', () => {
            expect(userService.emailExists).toThrowError();
        });

        it('should true if find user by email found', (done) => {
            const email = 'email';
            const user = {};
            spyOn(User, 'findByEmail').withArgs(email)
                .and.resolveTo(user);

            userService.emailExists(email).subscribe(result => {
                expect(result).toBeTruthy();
                done();
            });
        });
    });

    describe('findById', () => {
        it('should throw error if id is null', () => {
            expect(userService.findUserById).toThrowError();
        });

        it('should return user if found', (done) => {
            const id = 'id';
            const user = { user: 'user' };
            spyOn(User, 'findById').withArgs(id)
                .and.resolveTo(user);

            userService.findUserById(id).subscribe(result => {
                expect(result).toEqual(user);
                done();
            });
        });
    });
    describe('update()', () => {
        it('should throw error if user is null', () => {
            expect(userService.update).toThrowError();
        });

        it('should throw error is user is not User model', () => {
            const invalidUser = {};

            expect(() => userService.update(invalidUser)).toThrowError();
        });

        it('should update user', (done) => {
            const user = new User._Model();
            spyOn(User, 'update').withArgs(user)
                .and.resolveTo(user);

            userService.update(user).subscribe(result => {
                expect(result).toBe(user);
                expect(User.update).toHaveBeenCalledWith(user);
                done();
            });
        });
    });

    describe('getAllUsers()', () => {
        it('should get all users', (done) => {
            const users = [{}, {}, {}];
            spyOn(User, 'getAll')
                .and.resolveTo(users);

            userService.getAllUsers().subscribe(result => {
                expect(result.length).toBe(users.length);
                expect(User.getAll).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getusers()', () => {
        it('should return paged user data', (done) => {
            const pageNumber = 1;
            const pageSize = 2;
            const count = 5;
            const users = [{}, {}, {}];
            spyOn(User, 'count').and.resolveTo(count);
            spyOn(User, 'getUsers').withArgs({ pageNumber, pageSize })
                .and.resolveTo(users);

            userService.getUsers({ pageNumber, pageSize }).subscribe(result => {
                expect(result.data).toBe(users);
                expect(result.pager).toEqual(pagerHelper.getPager({ pageNumber, pageSize, count }));
                done();
            });
        });

        it('should fallback to default value if pageNumber pageSize are invalid', (done) => {
            const pageNumber = 'a';
            const pageSize = 'b';
            const count = 5;
            const users = [{}, {}, {}];
            spyOn(User, 'count').and.resolveTo(count);
            spyOn(User, 'getUsers').and.resolveTo(users);

            userService.getUsers({ pageNumber, pageSize }).subscribe(result => {
                expect(result.data).toBe(users);
                expect(User.getUsers).toHaveBeenCalledWith({ pageNumber: 1, pageSize: config.DEFAULT_PAGESIZE });
                done();
            });
        });
    });

    describe('deleteUser()', () => {
        it('should throw error if user is null', () => {
            expect(userService.deleteUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => userService.deleteUser(invalidUser)).toThrowError();
        });

        it('should delete user', (done) => {
            const recordId = 'record id';
            const goalPointId = 'goal point id';
            const testGoalPointId = 'test goal point id';
            const user = new User._Model({
                recordId,
                goalPointId,
                testGoalPointId
            });
            spyOn(User, 'remove').and.resolveTo(null);
            spyOn(goalPointService, 'deleteGoalPointById').withArgs(goalPointId)
                .and.returnValue(of(null));
            spyOn(goalPointService, 'deleteTestGoalPointById').withArgs(testGoalPointId)
                .and.returnValue(of(null));
            spyOn(recordService, 'deleteRecordById').withArgs(recordId)
                .and.returnValue(of(null));

            userService.deleteUser(user).subscribe(() => {
                expect(User.remove).toHaveBeenCalled();
                expect(goalPointService.deleteGoalPointById).toHaveBeenCalled();
                expect(goalPointService.deleteTestGoalPointById).toHaveBeenCalled();
                expect(recordService.deleteRecordById).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user dont have entities', (done) => {
            const recordId = null;
            const goalPointId = null;
            const testGoalPointId = null;
            const user = new User._Model({
                recordId,
                goalPointId,
                testGoalPointId
            });
            spyOn(User, 'remove').and.resolveTo(null);
            spyOn(goalPointService, 'deleteGoalPointById');
            spyOn(goalPointService, 'deleteTestGoalPointById');
            spyOn(recordService, 'deleteRecordById');

            userService.deleteUser(user).subscribe(() => {
                expect(User.remove).toHaveBeenCalled();
                expect(goalPointService.deleteGoalPointById).not.toHaveBeenCalled();
                expect(goalPointService.deleteTestGoalPointById).not.toHaveBeenCalled();
                expect(recordService.deleteRecordById).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('findUserByEmail()', () => {
        it('should throw error if email is null', () => {
            expect(userService.findUserByEmail).toThrowError();
        });

        it('should find user by email', (done) => {
            const email = 'email';
            const user = {};
            spyOn(User, 'findByEmail').withArgs(email)
                .and.resolveTo(user);

            userService.findUserByEmail(email).subscribe(result => {
                expect(result).toBe(user);
                done();
            });
        });
    });
});