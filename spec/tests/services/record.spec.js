const Record = require('../../../src/models/Record');
const recordService = require('../../../src/services/record');
const sourceService = require('../../../src/services/source');
const northService = require('../../../src/services/north-number');
const southService = require('../../../src/services/south-number');
const goalPointService = require('../../../src/services/goal-point');
const messageService = require('../../../src/services/message.service');
const dateTimeHelper = require('../../../src/services/date-helper');
const User = require('../../../src/models/User');
const { of, from } = require('rxjs');
const { catchError } = require('rxjs/operators');
const recordTypes = require('../../../src/services/records/types');
const { NORTH_AREA, SOUTH_AREA } = require('../../../src/models/constants/areas');
const { GOAL_POINT } = require('../../../src/models/constants/goal-points');
const { SUCCESS_GOAL_POINT_REASON, ADD_RECORD_ENTRY_GOAL_POINT_REASON, DELETE_RECORD_ENTRY_GOAL_POINT_REASON, UPDATE_RECORD_ENTRY_GOAL_POINT_REASON } = require('../../../src/models/constants/goal-point-entry-reasons');
const { RECORD_ENTRY } = require('../../../src/models/constants/entities.const');
const cacheService = require('../../../src/services/cache');
const cacheNumberKey = require('../../../src/services/caches/number.key');
const { INVALID_DATA_REASON, NUMBER_CANNOT_PROCESS_REASON, PROCESSOR_NOT_FOUND_REASON, PROCESS_FAILED } = require('../../../src/models/constants/record-entry-reasons.const');
const moment = require('moment');

describe('Record', () => {
    describe('createRecordForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.createRecordForUser).toThrowError();
        });

        it('should throw error if user is not a User', () => {
            const user = 'invalid user';
            expect(() => recordService.createRecordForUser(user)).toThrowError();
        });

        it('should throw error if user already has goal point', () => {
            const user = new User._Model({ recordId: 'record-id' });
            expect(() => recordService.createRecordForUser(user)).toThrowError();
        });

        it('should create goal point success', (done) => {
            const user = new User._Model();
            const record = {};
            spyOn(Record, 'createForUser').withArgs(user)
                .and.resolveTo(record);

            recordService.createRecordForUser(user).subscribe(result => {
                expect(result).toBe(record);
                expect(Record.createForUser).toHaveBeenCalledWith(user);
                done();
            });
        });
    });

    describe('addRecordEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.addRecordEntryForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => recordService.addRecordEntryForUser(invalidUser)).toThrowError();
        });

        it('should throw error if entry is null', () => {
            const user = new User._Model();

            expect(() => recordService.addRecordEntryForUser(user)).toThrowError();
        });

        it('should throw error if point type is invalid', () => {
            const user = new User._Model();
            const pointType = 'invalid point type';
            const entry = { pointType };

            expect(() => recordService.addRecordEntryForUser(user, entry)).toThrowError();
        });

        it('should add record entry for user', (done) => {
            const user = new User._Model();
            const entry = { pointType: GOAL_POINT, point: 1 };
            const goalPoint = { total: 1 };
            spyOn(goalPointService, 'getGoalPointForUser').withArgs(user)
                .and.returnValue(of(goalPoint))
            spyOn(Record, 'addEntryForUser').withArgs(user, entry)
                .and.resolveTo(entry);
            spyOn(goalPointService, 'addGoalPointEntryForUser')
                .and.returnValue(of(null));

            recordService.addRecordEntryForUser(user, entry).subscribe((result) => {
                expect(result).toBe(entry);
                expect(Record.addEntryForUser).toHaveBeenCalledWith(user, entry);
                done();
            });
        });

        it('should error if user goal point is less than entry point', (done) => {
            const user = new User._Model();
            const entry = { pointType: GOAL_POINT, point: 100 };
            const goalPoint = { total: 99 };
            spyOn(goalPointService, 'getGoalPointForUser').withArgs(user)
                .and.returnValue(of(goalPoint));
            spyOn(Record, 'addEntryForUser');

            recordService.addRecordEntryForUser(user, entry).subscribe(null, () => {
                expect(goalPointService.getGoalPointForUser).toHaveBeenCalledWith(user);
                expect(Record.addEntryForUser).not.toHaveBeenCalled();
                done();
            });
        });

        it('should decrement goal point for user', (done) => {
            const user = new User._Model();
            const entry = { pointType: GOAL_POINT, point: 100 };
            const goalPoint = { total: 100 };
            const description = 'description';
            const entity = { name: RECORD_ENTRY, target: JSON.stringify(entry) };
            spyOn(goalPointService, 'getGoalPointForUser').withArgs(user)
                .and.returnValue(of(goalPoint));
            spyOn(Record, 'addEntryForUser').withArgs(user, entry)
                .and.resolveTo(entry);
            spyOn(messageService, 'addRecordEntry').withArgs(entry)
                .and.returnValue(description);
            spyOn(goalPointService, 'addGoalPointEntryForUser').withArgs(user, ADD_RECORD_ENTRY_GOAL_POINT_REASON, -entry.point, description, entity)
                .and.returnValue(of({}));

            recordService.addRecordEntryForUser(user, entry).subscribe(() => {
                expect(messageService.addRecordEntry).toHaveBeenCalled();
                expect(goalPointService.addGoalPointEntryForUser).toHaveBeenCalledWith(user, ADD_RECORD_ENTRY_GOAL_POINT_REASON, -entry.point, description, entity);
                done();
            });
        });
    });

    describe('getRecordEntriesForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.getRecordEntriesForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => recordService.getRecordEntriesForUser(invalidUser)).toThrowError();
        });

        it('should return empty array if user dont have record', (done) => {
            const user = new User._Model();

            recordService.getRecordEntriesForUser(user).subscribe(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should return record entries of user', (done) => {
            const recordId = 'record id';
            const record = { _id: recordId, entries: [{}] };
            const user = new User._Model({
                recordId
            });
            spyOn(Record, 'getEntriesForUser').withArgs(user)
                .and.resolveTo(record.entries);

            recordService.getRecordEntriesForUser(user).subscribe(result => {
                expect(result).toBe(record.entries);
                done();
            });
        });
    });

    describe('deleteRecordEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.deleteRecordEntryForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => recordService.deleteRecordEntryForUser(invalidUser)).toThrowError();
        });

        it('should throw error if id is null', () => {
            const user = new User._Model({
                recordId: 'record id'
            });

            expect(() => recordService.deleteRecordEntryForUser(user)).toThrowError();
        });

        it('should throw error if user dont have record', () => {
            const user = new User._Model();
            const id = 'record entry id';

            expect(() => recordService.deleteRecordEntryForUser(user, id)).toThrowError();
        });

        it('should delete entry from user record', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                _id: recordId,
                entries: [{}]
            });
            const deleteEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            spyOn(Record, 'deleteEntryForUser').withArgs(user, deleteEntryId)
                .and.resolveTo(record.entries[0]);

            recordService.deleteRecordEntryForUser(user, deleteEntryId).subscribe(() => {
                expect(Record.deleteEntryForUser).toHaveBeenCalledWith(user, deleteEntryId);
                done();
            });
        });

        it('should increment goal point for user', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                _id: recordId,
                entries: [{ pointType: GOAL_POINT, point: 100 }]
            });
            const deleteEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            const description = 'description';
            const entity = { name: RECORD_ENTRY, target: JSON.stringify(record.entries[0]) };
            spyOn(messageService, 'deleteRecordEntry').withArgs(record.entries[0])
                .and.returnValue(description);
            spyOn(Record, 'deleteEntryForUser').withArgs(user, deleteEntryId)
                .and.resolveTo(record.entries[0]);
            spyOn(goalPointService, 'addGoalPointEntryForUser').withArgs(user, DELETE_RECORD_ENTRY_GOAL_POINT_REASON, record.entries[0].point, description, entity)
                .and.returnValue(of({}));

            recordService.deleteRecordEntryForUser(user, deleteEntryId).subscribe(() => {
                expect(goalPointService.addGoalPointEntryForUser).toHaveBeenCalledWith(user, DELETE_RECORD_ENTRY_GOAL_POINT_REASON, record.entries[0].point, description, entity);
                done();
            });
        });
    });

    describe('alterRecordEntryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.alterRecordEntryForUser).toThrowError();
        });

        it('should throw error if user is not User model', () => {
            const invalidUser = {};

            expect(() => recordService.alterRecordEntryForUser(invalidUser)).toThrowError();
        });

        it('should throw error if id is null', () => {
            const user = new User._Model({
                recordId: 'record id'
            });

            expect(() => recordService.alterRecordEntryForUser(user)).toThrowError();
        });

        it('should throw error if entry is null', () => {
            const recordId = 'record id';
            const user = new User._Model({
                recordId
            });
            const alterEntryId = 'alter entry id';

            expect(() => recordService.alterRecordEntryForUser(user, alterEntryId));
        });

        it('should throw error if entry has pointType', () => {
            const recordId = 'record id';
            const user = new User._Model({
                recordId
            });
            const alterEntryId = 'alter entry id';
            const entry = { pointType: null };

            expect(() => recordService.alterRecordEntryForUser(user, alterEntryId, entry));
        });

        it('should throw error if entry has _id', () => {
            const recordId = 'record id';
            const user = new User._Model({
                recordId
            });
            const alterEntryId = 'alter entry id';
            const entry = { _id: 'id' };

            expect(() => recordService.alterRecordEntryForUser(user, alterEntryId, entry));
        });

        it('should throw error if user dont have record', (done) => {
            const recordId = 'record id';
            const user = new User._Model({
                recordId
            });
            const id = 'record entry id';
            const entry = {};
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(null);

            recordService.alterRecordEntryForUser(user, id, entry).pipe(
                catchError((error) => {
                    expect(error).toBeTruthy();
                    expect(Record.getForUser).toHaveBeenCalledWith(user);
                    return of(error);
                })
            ).subscribe(done);
        });

        it('should update record entry of user', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                entries: [{ pointType: GOAL_POINT, point: 0 }]
            });
            const alterEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            const entry = {};
            const description = 'description';
            spyOn(messageService, 'alterRecordEntry').and.returnValue(description);
            spyOn(goalPointService, 'getGoalPointForUser').withArgs(user)
                .and.returnValue(of({ total: 0 }));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(Record, 'alterEntryForUser').withArgs(user, alterEntryId, entry)
                .and.resolveTo(Object.assign(record.entries[0], entry));
            spyOn(goalPointService, 'addGoalPointEntryForUser').and.returnValue(of({}));

            recordService.alterRecordEntryForUser(user, alterEntryId, entry).subscribe(result => {
                expect(result).toEqual(Object.assign(record.entries[0], entry));
                expect(Record.alterEntryForUser).toHaveBeenCalledWith(user, alterEntryId, entry);
                done();
            });
        });

        it('should error if user don\'t have enough goal point', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                entries: [{ point: 100, pointType: GOAL_POINT }]
            });
            const alterEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            const entry = { point: 150 };
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(Record, 'alterEntryForUser').withArgs(user, alterEntryId, entry)
                .and.resolveTo({
                    point: 50,
                    pointType: GOAL_POINT
                });
            spyOn(goalPointService, 'addGoalPointEntryForUser').withArgs(user, UPDATE_RECORD_ENTRY_GOAL_POINT_REASON, 50)
                .and.returnValue(of({}));

            recordService.alterRecordEntryForUser(user, alterEntryId, entry).pipe(
                catchError(error => {
                    expect(goalPointService.addGoalPointEntryForUser).not.toHaveBeenCalled();
                    return of(error);
                })
            ).subscribe(done);
        });

        it('should update goal point for user', (done) => {
            const recordId = 'record id';
            const record = new Record._Model({
                entries: [{ point: 100, pointType: GOAL_POINT }]
            });
            const alterEntryId = record.entries[0]._id;
            const user = new User._Model({
                recordId
            });
            const entry = { point: 50 };
            const goalPoint = { total: 0 };
            const description = 'description';
            const alteredRecordEntry = {
                point: 50,
                pointType: GOAL_POINT
            };
            const entity = {
                name: RECORD_ENTRY, target: JSON.stringify(alteredRecordEntry)
            };
            spyOn(messageService, 'alterRecordEntry').withArgs(Object.assign({}, record.entries[0]), alteredRecordEntry)
                .and.returnValue(description);
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(goalPointService, 'getGoalPointForUser').withArgs(user)
                .and.returnValue(of(goalPoint));
            spyOn(Record, 'alterEntryForUser').withArgs(user, alterEntryId, entry)
                .and.resolveTo(alteredRecordEntry);
            spyOn(goalPointService, 'addGoalPointEntryForUser').withArgs(user, UPDATE_RECORD_ENTRY_GOAL_POINT_REASON, 50, description, entity)
                .and.returnValue(of({}));

            recordService.alterRecordEntryForUser(user, alterEntryId, entry).subscribe(() => {
                expect(goalPointService.addGoalPointEntryForUser).toHaveBeenCalledWith(user, UPDATE_RECORD_ENTRY_GOAL_POINT_REASON, 50, description, entity);
                done();
            });
        });
    });

    describe('getRecordTypeForEntry()', () => {
        it('should throw error if entry is null', () => {
            expect(recordService.getRecordTypeForEntry).toThrowError();
        });

        it('should return record type find by entry type', () => {
            const entry = { type: recordTypes[0].info.code };

            const recordType = recordService.getRecordTypeForEntry(entry);

            expect(recordType).toBe(recordTypes[0]);
        });
    });

    describe('getDataForEntry()', () => {
        it('should throw error if entry is null', () => {
            expect(recordService.getDataForEntry).toThrowError();
        });

        it('should null if source not have data by date', (done) => {
            const entry = { date: 'date' };
            spyOn(sourceService, 'getByDate')
                .and.returnValue(of(null));

            recordService.getDataForEntry(entry, new Record._Model(), new User._Model()).subscribe(result => {
                expect(result).toBeFalsy();
                expect(sourceService.getByDate).toHaveBeenCalled();
                done();
            });
        });

        it('should null if get source is null', (done) => {
            const entry = { date: 'date' };
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(sourceService, 'getByDate').and.returnValue(of(null));

            recordService.getDataForEntry(entry, new Record._Model(), new User._Model()).subscribe(result => {
                expect(result).toBeFalsy();
                expect(sourceService.getByDate).toHaveBeenCalled();
                done();
            });
        });

        it('should get data for entry success', (done) => {
            const entry = { date: 'date', area: 'north', channel: 'channel' };
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const north = {
                _id: 'north id',
                channels: [{ name: 'channel', allNumbers: [] }]
            }
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(north));

            recordService.getDataForEntry(entry, new Record._Model(), new User._Model()).subscribe(result => {
                expect(result).toEqual({
                    success: true,
                    result: {
                        sourceId: source._id,
                        areaId: north._id,
                        numbers: north.channels[0].allNumbers
                    }
                });
                done();
            });
        });

        it('should error if area data is not found', (done) => {
            const entry = { date: 'date', area: 'north', channel: 'channel' };
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(null));

            recordService.getDataForEntry(entry, new Record._Model(), new User._Model()).subscribe(null, error => {
                expect(error).toBeTruthy();
                expect(northService.getById).toHaveBeenCalled();
                done();
            });
        });

        xit('should get data from cache if exists', (done) => {
            const entry = { date: 'date', area: 'north', channel: 'channel' };
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const north = {
                _id: 'north id',
                channels: [{ name: 'channel', allNumbers: [] }]
            }
            spyOn(cacheService, 'tryGetItem').and.callFake((key) => {
                switch (key) {
                    case cacheNumberKey.sourceByDate(entry.date):
                        return of(source);
                    case cacheNumberKey.northNumberByDate(entry.date):
                        return of(north);
                }
                return of(null);
            });
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate');
            spyOn(northService, 'getById');

            recordService.getDataForEntry(entry).subscribe(result => {
                expect(result).toEqual({
                    sourceId: source._id,
                    areaId: north._id,
                    data: north.channels[0].allNumbers,
                    entry
                });
                expect(sourceService.getByDate).not.toHaveBeenCalled();
                expect(northService.getById).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('processForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.processForUser).toThrowError();
        });

        it('should throw error if user is not User', () => {
            const user = {};

            expect(() => recordService.processForUser(user)).toThrowError();
        });

        it('should skip if record is not found', (done) => {
            const user = new User._Model();
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(null);

            recordService.processForUser(user).subscribe(null, null, () => {
                expect(Record.getForUser).toHaveBeenCalledWith(user);
                done();
            });
        });

        it('should skip if record entries is empty', (done) => {
            const user = new User._Model();
            const record = new Record._Model();
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);

            recordService.processForUser(user).subscribe(null, null, () => {
                expect(Record.getForUser).toHaveBeenCalledWith(user);
                done();
            });
        });

        it('should result failed with reason processor not found if get record type is not found', (done) => {
            const user = new User._Model();
            const record = new Record._Model({ entries: [{ type: 'not found record type' }] });
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(() => {
                expect(record.save).toHaveBeenCalled();
                expect(record.entries.length).toBe(0);
                expect(record.histories[0].failedReason).toBe(PROCESSOR_NOT_FOUND_REASON);
                expect(Record.getForUser).toHaveBeenCalledWith(user);
                done();
            });
        });

        it('should skip if source don\'t have', (done) => {
            const user = new User._Model();
            const record = new Record._Model({ entries: [{ number: '000', type: recordTypes[0].info.code }] });
            const entry = record.entries[0];
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'getByDate').and.returnValue(of(null));

            recordService.processForUser(user).subscribe(() => {
                expect(sourceService.getByDate).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if source by date is not found', (done) => {
            const user = new User._Model();
            const record = new Record._Model({ entries: [{ number: '000', type: recordTypes[0].info.code, area: NORTH_AREA }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(null));

            recordService.processForUser(user).subscribe(() => {
                expect(sourceService.getByDate).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if area source is not found', (done) => {
            const user = new User._Model();
            const source = {
                north: {
                    id: 'north id'
                }
            };
            const record = new Record._Model({ entries: [{ type: recordTypes[0].info.code, number: '000', area: NORTH_AREA }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(null));

            recordService.processForUser(user).subscribe(null, () => {
                expect(northService.getById).toHaveBeenCalledWith(source.north.id);
                done();
            });
        });

        it('should result failed with reason invalid data if area is not match', (done) => {
            const invalidArea = 'invalid area';
            const user = new User._Model();
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const record = new Record._Model({ entries: [{ type: recordTypes[0].info.code, number: '000', area: invalidArea }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(() => {
                expect(record.save).toHaveBeenCalled();
                expect(record.entries.length).toBe(0);
                expect(record.histories[0].failedReason).toBe(INVALID_DATA_REASON);
                done();
            });
        });

        it('should result failed with reason number cannot process', (done) => {
            const user = new User._Model();
            const record = new Record._Model({ entries: [{ number: '000', type: recordTypes[0].info.code }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(false);
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(() => {
                expect(record.save).toHaveBeenCalled();
                expect(record.entries.length).toBe(0);
                expect(record.histories[0].failedReason).toBe(NUMBER_CANNOT_PROCESS_REASON);
                expect(recordTypes[0].processor.canProcess).toHaveBeenCalled();
                done();
            });
        });

        it('should result failed with reason invalid data if channel is not found [south area]', (done) => {
            const channel = 'not found channel';
            const user = new User._Model();
            const source = {
                _id: 'source id',
                south: {
                    id: 'south id',
                    channels: []
                }
            };
            const record = new Record._Model({ entries: [{ type: recordTypes[0].info.code, number: '000', area: SOUTH_AREA, channel }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(southService, 'getById').withArgs(source.south.id)
                .and.returnValue(of(source.south));
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(() => {
                expect(record.save).toHaveBeenCalled();
                expect(record.entries.length).toBe(0);
                expect(record.histories[0].failedReason).toBe(INVALID_DATA_REASON);
                done();
            });
        });

        it('should get first channel if entry area is north', (done) => {
            const channelName = 'not important channel';
            const user = new User._Model();
            const source = {
                north: {
                    id: 'north id'
                }
            };
            const north = {
                id: 'north id',
                channels: [{
                    name: 'channel name',
                    allNumbers: [123, 456]
                }]
            };
            const record = new Record._Model({ entries: [{ type: recordTypes[0].info.code, number: '000', area: NORTH_AREA, channel: channelName }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(north));
            spyOn(recordTypes[0].processor, 'process').withArgs(entry.number, north.channels[0].allNumbers)
                .and.returnValue(false);
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(() => {
                expect(northService.getById).toHaveBeenCalled();
                expect(record.save).toHaveBeenCalled();
                done();
            });
        });

        it('should add history if process number is failed', (done) => {
            const channelName = 'channel';
            const user = new User._Model();
            const source = {
                north: {
                    id: 'north id'
                }
            };
            const north = {
                id: 'north id',
                channels: [{
                    name: channelName,
                    allNumbers: [123, 456]
                }]
            };
            const record = new Record._Model({ entries: [{ type: recordTypes[0].info.code, number: '000', area: NORTH_AREA, channel: channelName }] });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(north));
            spyOn(recordTypes[0].processor, 'process').withArgs(entry.number, north.channels[0].allNumbers)
                .and.returnValue(false);
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(() => {
                expect(record.entries.length).toBe(0);
                expect(record.histories.length).toBe(1);
                expect(record.histories[0].failedReason).toBe(PROCESS_FAILED);
                expect(record.save).toHaveBeenCalled();
                done();
            });
        });

        it('should add goal point if process number is success', (done) => {
            const channelName = 'channel';
            const user = new User._Model();
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const north = {
                channels: [{
                    name: channelName,
                    allNumbers: [123, 456]
                }]
            };
            const record = new Record._Model({
                entries: [{
                    type: recordTypes[0].info.code,
                    number: '000',
                    area: NORTH_AREA,
                    channel: channelName,
                    pointType: GOAL_POINT,
                    point: 1
                }]
            });
            const entry = record.entries[0];
            const description = 'success description';
            const entity = { name: RECORD_ENTRY, target: JSON.stringify(entry) };
            spyOn(messageService, 'processRecordEntrySuccess')
                .and.returnValue(description);
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(north));
            spyOn(recordTypes[0].processor, 'process').withArgs(entry.number, north.channels[0].allNumbers)
                .and.returnValue(true);
            spyOn(goalPointService, 'addGoalPointEntryForUser').withArgs(user, SUCCESS_GOAL_POINT_REASON, entry.point * recordTypes[0].config.goalRate.north, description, entity)
                .and.returnValue(of({}));
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(null, null, () => {
                expect(goalPointService.addGoalPointEntryForUser).toHaveBeenCalled();
                done();
            });
        });

        it('should return process result if process success', (done) => {
            const channelName = 'channel';
            const user = new User._Model();
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const north = {
                channels: [{
                    name: channelName,
                    allNumbers: [123, 456]
                }]
            };
            const record = new Record._Model({
                entries: [{
                    type: recordTypes[0].info.code,
                    number: '000',
                    area: NORTH_AREA,
                    channel: channelName,
                    pointType: GOAL_POINT,
                    point: 1
                }]
            });
            const entry = record.entries[0];
            const description = 'success description';
            const entity = { name: RECORD_ENTRY, target: JSON.stringify(entry) };
            const goalPoint = entry.point * recordTypes[0].config.goalRate.north;
            spyOn(messageService, 'processRecordEntrySuccess')
                .and.returnValue(description);
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(north));
            spyOn(recordTypes[0].processor, 'process').withArgs(entry.number, north.channels[0].allNumbers)
                .and.returnValue(true);
            spyOn(goalPointService, 'addGoalPointEntryForUser').withArgs(user, SUCCESS_GOAL_POINT_REASON, goalPoint, description, entity)
                .and.returnValue(of({}));
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(result => {
                expect(result).toEqual({
                    entry,
                    success: true,
                    goalPoint
                });
                done();
            });
        });

        it('should return process result if process failed', (done) => {
            const channelName = 'channel';
            const user = new User._Model();
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const north = {
                channels: [{
                    name: channelName,
                    allNumbers: [123, 456]
                }]
            };
            const record = new Record._Model({
                entries: [{
                    type: recordTypes[0].info.code,
                    number: '000',
                    area: NORTH_AREA,
                    channel: channelName,
                    pointType: GOAL_POINT,
                    point: 1
                }]
            });
            const entry = record.entries[0];
            const description = 'failed description';
            spyOn(messageService, 'processRecordEntrySuccess')
                .and.returnValue(description);
            spyOn(cacheService, 'tryGetItem').and.callFake((key, fn) => from(fn()));
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(true);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate').and.returnValue(of(source));
            spyOn(northService, 'getById').withArgs(source.north.id)
                .and.returnValue(of(north));
            spyOn(recordTypes[0].processor, 'process').withArgs(entry.number, north.channels[0].allNumbers)
                .and.returnValue(false);
            spyOn(record, 'save').and.resolveTo(record);

            recordService.processForUser(user).subscribe(result => {
                expect(result).toEqual({
                    entry,
                    success: false,
                    reason: PROCESS_FAILED
                });
                done();
            });
        });

        xit('should get data from cache if exist', (done) => {
            const channelName = 'channel';
            const user = new User._Model();
            const source = {
                _id: 'source id',
                north: {
                    id: 'north id'
                }
            };
            const north = {
                channels: [{
                    name: channelName,
                    allNumbers: [123, 456]
                }]
            };
            const record = new Record._Model({
                entries: [{
                    type: recordTypes[0].info.code,
                    number: '000',
                    area: NORTH_AREA,
                    channel: channelName,
                    pointType: GOAL_POINT,
                    point: 1,
                    date: 'date'
                }]
            });
            const entry = record.entries[0];
            spyOn(cacheService, 'tryGetItem').and.callFake((key) => {
                switch (key) {
                    case cacheNumberKey.sourceByDate(entry.date):
                        return of(source);
                    case cacheNumberKey.northNumberByDate(entry.date):
                        return of(north);
                }
                return of(null);
            });
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);
            spyOn(recordTypes[0].processor, 'canProcess').withArgs(entry.number)
                .and.returnValue(false);
            spyOn(sourceService, 'has').and.returnValue(of(true));
            spyOn(sourceService, 'getByDate');
            spyOn(northService, 'getById');

            recordService.processForUser(user).subscribe(null, null, () => {
                expect(recordTypes[0].processor.canProcess).toHaveBeenCalled();
                expect(sourceService.getByDate).not.toHaveBeenCalled();
                expect(northService.getById).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('isRecordableDate()', () => {
        it('should throw error is date is null', () => {
            expect(recordService.isRecordableDate).toThrowError();
        });

        it('should throw error is date is not Date', () => {
            const invalidDate = 0;

            expect(() => recordService.isRecordableDate(invalidDate)).toThrowError();
        });

        it('should false if date is before now', () => {
            const date = new Date('2020-03-15');
            const now = new Date('2020-03-16');
            spyOn(Date, 'now').and.returnValue(now.getTime());

            const result = recordService.isRecordableDate(date);

            expect(result).toBeFalsy();
        });

        it('should false if date is alter end and before today', () => {
            const date = new Date('2020-03-15');
            const now = new Date('2020-03-15 16:01');
            const end = new Date('2020-03-15 16:00');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'getEndDate').withArgs(now)
                .and.returnValue(end);

            const result = recordService.isRecordableDate(date);

            expect(result).toBeFalsy();
        });

        it('should true if date is before end and is today', () => {
            const date = new Date('2020-03-15');
            const now = new Date('2020-03-15 15:59');
            const end = new Date('2020-03-15 16:00');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'getEndDate').withArgs(now)
                .and.returnValue(end);

            const result = recordService.isRecordableDate(date);

            expect(result).toBeTruthy();
        });
    });

    describe('getRecordableDate()', () => {
        it('should today if before end date', () => {
            const end = new Date('2020-03-15 16:00');
            const now = new Date('2020-03-15 15:59');
            const recordableDate = new Date('2020-03-15 00:00');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'getEndDate').withArgs(now)
                .and.returnValue(moment(end));

            const result = recordService.getRecordableDate();
            expect(result.toJSON()).toEqual(recordableDate.toJSON());
        });

        it('should today plus one if after end date', () => {
            const end = new Date('2020-03-15 16:00');
            const now = new Date('2020-03-15 16:01');
            const recordableDate = new Date('2020-03-16 00:00');
            spyOn(Date, 'now').and.returnValue(now.getTime());
            spyOn(dateTimeHelper, 'getEndDate').withArgs(now)
                .and.returnValue(moment(end));

            const result = recordService.getRecordableDate();
            expect(result.toJSON()).toEqual(recordableDate.toJSON());
        });
    });

    describe('getRecordHistoryForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.getRecordHistoryForUser).toThrowError();
        });

        it('should empty if user dont have record', (done) => {
            const user = {};

            recordService.getRecordHistoryForUser(user).subscribe(result => {
                expect(result).toEqual([]);
                done();
            });
        });

        it('should return record histories for user', (done) => {
            const histories = [{}, {}, {}];
            const user = { recordId: 'record id' };
            spyOn(Record, 'getRecordHistoryForUser').withArgs(user)
                .and.resolveTo(histories);

            recordService.getRecordHistoryForUser(user).subscribe(result => {
                expect(result).toBe(histories);
                done();
            });
        });
    });

    describe('getRecordCountForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.getRecordCountForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => recordService.getRecordCountForUser(invalidUser)).toThrowError();
        });

        it('should return 0 if user dont have record', (done) => {
            const user = new User._Model();

            recordService.getRecordCountForUser(user).subscribe(result => {
                expect(result).toBe(0);
                done();
            });
        });

        it('should return 0 if record is not found', (done) => {
            const recordId = 'not found record'
            const user = new User._Model({ recordId });
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(null);

            recordService.getRecordCountForUser(user).subscribe(result => {
                expect(Record.getForUser).toHaveBeenCalled();
                expect(result).toBe(0);
                done();
            });
        });

        it('should result entires count plus histories count', (done) => {
            const recordId = 'record';
            const record = {
                entries: [{}, {}, {}],
                histories: [{}]
            };
            const user = new User._Model({ recordId });
            spyOn(Record, 'getForUser').withArgs(user)
                .and.resolveTo(record);

            recordService.getRecordCountForUser(user).subscribe(result => {
                expect(result).toBe(4);
                done();
            });
        });
    });

    describe('deleteRecordForUser()', () => {
        it('should throw error if user is null', () => {
            expect(recordService.deleteRecordForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => recordService.deleteRecordForUser(invalidUser)).toThrowError();
        });

        it('should delete record', (done) => {
            const user = new User._Model();
            spyOn(Record, 'removeForUser').withArgs(user)
                .and.resolveTo(null);

            recordService.deleteRecordForUser(user).subscribe(() => {
                expect(Record.removeForUser).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('deleteRecordById()', () => {
        it('should throw error if id is null', () => {
            expect(recordService.deleteRecordById).toThrowError();
        });

        it('should delete record if found', (done) => {
            const record = {};
            const id = 'record id';
            spyOn(Record, 'findById').withArgs(id)
                .and.resolveTo(record);
            spyOn(Record, 'remove').withArgs(record)
                .and.resolveTo(null);

            recordService.deleteRecordById(id).subscribe(() => {
                expect(Record.remove).toHaveBeenCalled();
                done();
            });
        });

        it('should throw error if record is not found', (done) => {
            const id = 'not found record';
            spyOn(Record, 'findById').withArgs(id)
                .and.resolveTo(null);

            recordService.deleteRecordById(id).subscribe(null, (error) => {
                expect(error).toBeTruthy();
                done();
            });
        });
    });
});