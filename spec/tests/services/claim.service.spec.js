const Claim = require('../../../src/models/Claim');
const User = require('../../../src/models/User');
const claimService = require('../../../src/services/claim.service');

describe('ClaimService', () => {
    describe('init()', () => {
        it('should create default claim if not exist', (done) => {
            const defaultClaims = ['default claim1', 'default claim2'];
            spyOnProperty(Claim, 'defaultClaims', 'get').and.returnValue(defaultClaims);
            spyOn(Claim, 'findByName').and.resolveTo(false);
            spyOn(Claim, 'create').and.callFake(claim => Promise.resolve(claim));

            claimService.init().subscribe(null, null, () => {
                expect(Claim.findByName).toHaveBeenCalledWith(defaultClaims[0]);
                expect(Claim.findByName).toHaveBeenCalledWith(defaultClaims[1]);
                expect(Claim.create).toHaveBeenCalledTimes(2);
                done();
            });
        });

        it('should skip if claim is created', (done) => {
            const defaultClaims = ['exist claim 1', 'exist claim 2'];
            spyOnProperty(Claim, 'defaultClaims', 'get').and.returnValue(defaultClaims);
            spyOn(Claim, 'findByName').and.resolveTo(true);
            spyOn(Claim, 'create').and.callFake(claim => Promise.resolve(claim));

            claimService.init().subscribe(null, null, () => {
                expect(Claim.findByName).toHaveBeenCalledWith(defaultClaims[0]);
                expect(Claim.findByName).toHaveBeenCalledWith(defaultClaims[1]);
                expect(Claim.create).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('create()', () => {
        it('should throw error if claim is null', () => {
            expect(claimService.create).toThrowError();
        });

        it('should create claim', (done) => {
            const claim = { name: 'name' };
            const insertedClaim = { _id: 'id', name: 'name' };
            spyOn(Claim, 'create').and.resolveTo(insertedClaim);

            claimService.create(claim).subscribe(result => {
                expect(result).toBe(insertedClaim);
                done();
            });
        });
    });

    describe('addClaimForUser()', () => {
        it('should throw error if user if null', () => {
            expect(claimService.addClaimForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => claimService.addClaimForUser(invalidUser)).toThrowError();
        });

        it('should throw error if claim name is null', () => {
            const user = new User._Model();

            expect(() => claimService.addClaimForUser(user)).toThrowError();
        });

        it('should add claim to user', (done) => {
            const user = new User._Model();
            const claim = { _id: 'claim id', name: 'new claim' };
            spyOn(Claim, 'findByName').withArgs(claim.name).and.resolveTo(claim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.addClaimForUser(user, claim.name).subscribe(result => {
                expect(result.length).toBe(1);
                expect(Claim.findByName).toHaveBeenCalled();
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should create claim if claim is not exist', (done) => {
            const user = new User._Model();
            const claimName = 'not existed';
            const insertedClaim = { _id: 'id', name: claimName };
            spyOn(Claim, 'findByName').withArgs(claimName)
                .and.resolveTo(null);
            spyOn(Claim, 'create').withArgs({ name: claimName })
                .and.resolveTo(insertedClaim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.addClaimForUser(user, claimName).subscribe(result => {
                expect(Claim.create).toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getClaimsForUser', () => {
        it('should throw error if user is null', () => {
            expect(claimService.getClaimsForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => claimService.getClaimsForUser(invalidUser)).toThrowError();
        });

        it('should return claims for user', (done) => {
            const claims = ['id Claims1', 'id Claims2'];
            const user = new User._Model({ claims });
            spyOn(Claim, 'findById').and.callFake(id => Promise.resolve({ _id: id }));

            claimService.getClaimsForUser(user).subscribe(result => {
                expect(result).toEqual(claims.map(id => ({ _id: id })));
                done();
            });
        });
    });

    describe('addRegisteredClaimForUser()', () => {
        it('should throw error if user is null', () => {
            expect(claimService.addRegisteredClaimForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => claimService.addRegisteredClaimForUser(invalidUser)).toThrowError();
        });

        it('should add registered claim to user', (done) => {
            const user = new User._Model();
            const registeredClaim = { _id: 'registered claim' };
            spyOn(Claim, 'registeredClaim').and.resolveTo(registeredClaim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.addRegisteredClaimForUser(user).subscribe(result => {
                expect(result.length).toBe(1);
                expect(Claim.registeredClaim).toHaveBeenCalled();
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user already have registered', done => {
            const userClaims = ['registered claim'];
            const user = new User._Model({ claims: userClaims });
            const registeredClaim = { _id: 'registered claim' };
            spyOn(Claim, 'registeredClaim').and.resolveTo(registeredClaim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.addRegisteredClaimForUser(user).subscribe(result => {
                expect(result.length).toBe(1);
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('addGuestClaimForUser()', () => {
        it('should throw error if user is null', () => {
            expect(claimService.addGuestClaimForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => claimService.addGuestClaimForUser(invalidUser)).toThrowError();
        });

        it('should add registered claim to user', (done) => {
            const user = new User._Model();
            const guestClaim = { _id: 'guest claim' };
            spyOn(Claim, 'guestClaim').and.resolveTo(guestClaim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.addGuestClaimForUser(user).subscribe(result => {
                expect(result.length).toBe(1);
                expect(Claim.guestClaim).toHaveBeenCalled();
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user already have guest', done => {
            const userClaims = ['guest claim'];
            const user = new User._Model({ claims: userClaims });
            const guestClaim = { _id: 'guest claim' };
            spyOn(Claim, 'guestClaim').and.resolveTo(guestClaim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.addGuestClaimForUser(user).subscribe(result => {
                expect(result.length).toBe(1);
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getAllClaims()', () => {
        it('should return all claims', (done) => {
            const allClaims = [{}];
            spyOn(Claim, 'getAll').and.resolveTo(allClaims);

            claimService.getAllClaims().subscribe(result => {
                expect(result).toBe(allClaims);
                done();
            });
        });
    });

    describe('removeClaimForUser()', () => {
        it('should throw error if user is null', () => {
            expect(claimService.removeClaimForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => claimService.removeClaimForUser(invalidUser)).toThrowError();
        });

        it('should throw error if claim name is null', () => {
            const user = new User._Model();

            expect(() => claimService.removeClaimForUser(user)).toThrowError();
        });

        it('should remove claim for user', (done) => {
            const removeClaimName = 'claims 1';
            const claim = { _id: 1, name: removeClaimName };
            const userClaims = [claim._id];
            const user = new User._Model({ claims: userClaims });
            spyOn(Claim, 'findByName').withArgs(removeClaimName)
                .and.resolveTo(claim);
            spyOn(user, 'save').and.resolveTo(user);

            claimService.removeClaimForUser(user, removeClaimName).subscribe(result => {
                expect(user.claims.length).toBe(0);
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if claim is not found', (done) => {
            const removeClaimName = 'not found claim';
            const userClaims = [];
            const user = new User._Model({ claims: userClaims });
            spyOn(Claim, 'findByName').withArgs(removeClaimName)
                .and.resolveTo(null);
            spyOn(user, 'save');

            claimService.removeClaimForUser(user, removeClaimName).subscribe(result => {
                expect(result).toBe(user.claims);
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user dont have claim', (done) => {
            const removeClaimName = 'claims 1';
            const claim = { _id: 1, name: removeClaimName };
            const userClaims = [];
            const user = new User._Model({ claims: userClaims });
            spyOn(Claim, 'findByName').withArgs(removeClaimName)
                .and.resolveTo(claim);
            spyOn(user, 'save');

            claimService.removeClaimForUser(user, removeClaimName).subscribe(result => {
                expect(result).toBe(user.claims);
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('removeClaimByIdForUser()', () => {
        it('should throw error if user is null', () => {
            expect(claimService.removeClaimByIdForUser).toThrowError();
        });

        it('should throw error if user is invalid', () => {
            const invalidUser = 0;

            expect(() => claimService.removeClaimByIdForUser(invalidUser)).toThrowError();
        });

        it('should throw error if claim id is null', () => {
            const user = new User._Model();

            expect(() => claimService.removeClaimByIdForUser(user)).toThrowError();
        });

        it('should remove claim', (done) => {
            const removeClaimId = 'claim id';
            const claim = { _id: removeClaimId };
            const userClaims = [claim._id];
            const user = new User._Model({ claims: userClaims });
            spyOn(user, 'save').and.resolveTo(user);

            claimService.removeClaimByIdForUser(user, removeClaimId).subscribe(result => {
                expect(user.claims.length).toBe(0);
                expect(user.save).toHaveBeenCalled();
                done();
            });
        });

        it('should skip if user dont have claim', (done) => {
            const removeClaimId = 'dont have claim id';
            const userClaims = [];
            const user = new User._Model({ claims: userClaims });
            spyOn(user, 'save');

            claimService.removeClaimByIdForUser(user, removeClaimId).subscribe(result => {
                expect(result).toBe(user.claims);
                expect(user.save).not.toHaveBeenCalled();
                done();
            });
        });
    });

    describe('getClaimIdsByNames()', () => {
        it('should empty result if names is null', (done) => {
            claimService.getClaimIdsByNames().subscribe(result => {
                expect(result.length).toBe(0)
                done();
            });
        });

        it('should empty result is names is empty', (done) => {
            claimService.getClaimIdsByNames([]).subscribe(result => {
                expect(result.length).toBe(0);
                done();
            });
        });

        it('should return ids of claim names', (done) => {
            const names = ['claim 1', 'claim 2', 'claim 3'];
            spyOn(Claim, 'findByName')
                .withArgs(names[0]).and.resolveTo({ _id: 1 })
                .withArgs(names[1]).and.resolveTo({ _id: 2 })
                .withArgs(names[2]).and.resolveTo({ _id: 3 });

            claimService.getClaimIdsByNames(names).subscribe(result => {
                expect(result).toEqual(['1', '2', '3']);
                done();
            });
        });

        it('should exclude not found claim name', (done) => {
            const names = ['claim 1', 'claim 2', 'not found claim'];
            spyOn(Claim, 'findByName')
                .withArgs(names[0]).and.resolveTo({ _id: 1 })
                .withArgs(names[1]).and.resolveTo({ _id: 2 })
                .and.resolveTo(null);

            claimService.getClaimIdsByNames(names).subscribe(result => {
                expect(result).toEqual(['1', '2']);
                done();
            });
        });
    });
});