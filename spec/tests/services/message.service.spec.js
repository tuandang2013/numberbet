const messageService = require('../../../src/services/message.service');
const moment = require('moment');

describe('MessageService', () => {
    describe('dailyTestGoalPoint()', () => {
        it('should throw error if value is null', () => {
            expect(messageService.dailyTestGoalPoint).toThrowError();
        });

        it('should throw error if date is null', () => {
            const value = 100;

            expect(() => messageService.dailyTestGoalPoint(value)).toThrowError();
        })

        it('should valid message', () => {
            const value = 100;
            const date = new Date('2020-03-07');

            const message = messageService.dailyTestGoalPoint(value, date);

            const expected = `You received ${value} points for date(${moment(date).format('DD/MM/YYYY')})`;
            expect(message).toBe(expected);
        });
    });

    describe('addRecordEntry()', () => {
        it('should throw error if newEntry is null', () => {
            expect(messageService.addRecordEntry).toThrowError();
        });

        it('should valid message', () => {
            const entry = {
                area: 'north',
                channel: 'channel',
                number: '000',
                point: 14,
                pointType: 'goal-point',
                type: 'type',
                date: 'date'
            };
            const now = new Date('2020-03-07');
            spyOn(Date, 'now').and.returnValue(now.getTime());

            const message = messageService.addRecordEntry(entry);

            const expected = `Record entry added at ${moment(now).format('YYYY-MM-DD hh:mm')} [number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})]`;
            expect(message).toBe(expected);
        });
    });

    describe('deleteRecordEntry()', () => {
        it('should throw error if entry is null', () => {
            expect(messageService.deleteRecordEntry).toThrowError();
        });

        it('should valid message', () => {
            const entry = {
                _id: 'entry id',
                area: 'north',
                channel: 'channel',
                number: '000',
                point: 14,
                pointType: 'goal-point',
                type: 'type',
                date: 'date'
            };
            const now = new Date('2020-03-07');
            spyOn(Date, 'now').and.returnValue(now.getTime());

            const message = messageService.deleteRecordEntry(entry);

            const expected = `Record entry ${entry._id} deleted at ${moment(now).format('YYYY-MM-DD hh:mm')} [number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})]`;
            expect(message).toBe(expected);
        });
    });

    describe('alterRecordEntry()', () => {
        it('should throw error if old entry is null', () => {
            expect(messageService.alterRecordEntry).toThrowError();
        });

        it('should throw error if new entry is null', () => {
            const oldEntry = {};

            expect(() => messageService.alterRecordEntry(oldEntry)).toThrowError();
        });

        it('should valid message', () => {
            const oldEntry = {
                _id: 'entry id',
                area: 'north',
                channel: 'channel',
                number: '000',
                point: 14,
                pointType: 'goal-point',
                type: 'type',
                date: 'date'
            };
            const newEntry = {
                _id: 'entry id',
                area: 'south',
                channel: 'new channel',
                number: '111',
                point: 41,
                pointType: 'test-goal-point',
                type: 'new type',
                date: 'new date'
            };
            const now = new Date('2020-03-07');
            spyOn(Date, 'now').and.returnValue(now.getTime());

            const message = messageService.alterRecordEntry(oldEntry, newEntry);

            const expected = `Record entry ${oldEntry._id.toString()} updated at ${moment(now).format('YYYY-MM-DD hh:mm')} [number: ${oldEntry.number}, channel: ${oldEntry.channel}(${oldEntry.area}), point: ${oldEntry.point}(${oldEntry.pointType})][number: ${newEntry.number}, channel: ${newEntry.channel}(${newEntry.area}), point: ${newEntry.point}(${newEntry.pointType})]`;
            expect(message).toBe(expected);
        });
    });

    describe('processRecordEntryFailed()', () => {
        it('should throw error if entry is null', () => {
            expect(messageService.processRecordEntryFailed).toThrowError();
        });

        it('should valid message', () => {
            const entry = {
                _id: 'entry id',
                area: 'south',
                channel: 'new channel',
                number: '111',
                point: 41,
                pointType: 'test-goal-point',
                type: 'new type',
                date: 'new date'
            };
            const now = new Date('2020-03-07');
            spyOn(Date, 'now').and.returnValue(now.getTime());

            const message = messageService.processRecordEntryFailed(entry);

            const expected = `Record entry ${entry._id} is failed at process ${moment(now).format('YYYY-MM-DD hh:mm')} [number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})]`;
            expect(message).toBe(expected);
        });
    });

    describe('processRecordEntrySuccess()', () => {
        it('should throw error if entry is null', () => {
            expect(messageService.processRecordEntrySuccess).toThrowError();
        });

        it('should throw error if goal point is null', () => {
            const entry = {};

            expect(() => messageService.processRecordEntrySuccess(entry)).toThrowError();
        });

        it('should valid message', () => {
            const entry = {
                _id: 'entry id',
                area: 'south',
                channel: 'new channel',
                number: '111',
                point: 41,
                pointType: 'test-goal-point',
                type: 'new type',
                date: 'new date'
            };
            const goalPoint = 100;
            const now = new Date('2020-03-07');
            spyOn(Date, 'now').and.returnValue(now.getTime());

            const message = messageService.processRecordEntrySuccess(entry, goalPoint);

            const expected = `Record entry ${entry._id} is success (${goalPoint}) at process ${moment(now).format('YYYY-MM-DD hh:mm')} [number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})]`;
            expect(message).toBe(expected);
        });
    });
});