const autoIncrementTestPoint = require('../../../src/middlewares/auto-increment-test-point');
const goalPointService = require('../../../src/services/goal-point');
const { of } = require('rxjs');

describe('AutoIncrementTestPoint Middleware', () => {
    it('should skip if user is not found', () => {
        const req = {};
        const next = () => { };
        const context = { next };
        spyOn(context, 'next');

        autoIncrementTestPoint()(req, null, context.next);

        expect(context.next).toHaveBeenCalled();
    });

    it('should add daily test point for user', (done) => {
        const user = {};
        const req = { user };
        const next = () => {
            expect(context.next).toHaveBeenCalled();
            done();
        };
        const context = { next };
        spyOn(goalPointService, 'addDailyTestGoalPointEntryForUser')
            .and.returnValue(of({}));
        spyOn(context, 'next').and.callThrough();

        autoIncrementTestPoint()(req, null, context.next);

        expect(goalPointService.addDailyTestGoalPointEntryForUser).toHaveBeenCalled();
    });
});