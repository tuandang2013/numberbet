const modelValidation = require('../../../src/middlewares/model-validation');

const fakeValidatorRun = (error) => (req) => {
    if (error)
        req.fakeError = error;
    return Promise.resolve();
};

describe('ModelValidation Middleware', () => {
    it('should skip if rules is null', () => {
        const req = {};
        const res = {};
        const next = () => { };
        //trick
        const context = { next };
        spyOn(context, 'next');

        modelValidation()(req, res, context.next);

        expect(req.modelErrors).toBeFalsy();
        expect(context.next).toHaveBeenCalled();
    });

    it('should skip if rules is not array', () => {
        const invalidRules = 0;
        const req = {};
        const res = {};
        const next = () => { };
        //trick
        const context = { next };
        spyOn(context, 'next');

        modelValidation(invalidRules)(req, res, context.next);

        expect(req.modelErrors).toBeFalsy();
        expect(context.next).toHaveBeenCalled();
    });

    it('should skip if rules is empty', () => {
        const emptyRules = [];
        const req = {};
        const res = {};
        const next = () => { };
        //trick
        const context = { next };
        spyOn(context, 'next');

        modelValidation(emptyRules)(req, res, context.next);

        expect(req.modelErrors).toBeFalsy();
        expect(context.next).toHaveBeenCalled();
    });
});