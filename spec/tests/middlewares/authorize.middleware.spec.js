const authorizeMiddleware = require('../../../src/middlewares/authorize.middleware');

describe('AuthorizeMiddleware', () => {
    it('should 401 result if user not found', () => {
        const req = {};
        const res = { statusCode: 0, end() { } };
        spyOn(res, 'end');

        authorizeMiddleware()(req, res);

        expect(res.statusCode).toBe(401);
        expect(res.end).toHaveBeenCalledWith('Unauthorized');
    });

    it('should 401 result if user not have claims', () => {
        const requiredClaims = ['claim 1', 'claim 2'];
        const req = {};
        const res = { statusCode: 0, end() { } };
        spyOn(res, 'end');

        authorizeMiddleware(requiredClaims)(req, res);

        expect(res.statusCode).toBe(401);
        expect(res.end).toHaveBeenCalledWith('Unauthorized');
    });

    it('should 401 json result if json is configured', () => {
        const req = {};
        const res = { statusCode: 0, json() { } };
        spyOn(res, 'json');

        authorizeMiddleware(null, { type: 'json' })(req, res);

        expect(res.statusCode).toBe(401);
        expect(res.json).toHaveBeenCalledWith({ error: { message: 'Unauthorized' } });
    });

    it('should next if request user if found', () => {
        const req = {user: {}};
        const res = {};
        const context = {
            next() { }
        };
        spyOn(context, 'next');

        authorizeMiddleware()(req, res, context.next);

        expect(context.next).toHaveBeenCalledWith();
    });
});