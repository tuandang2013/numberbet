const authenticate = require('../../../src/middlewares/authenticate');
const tokenService = require('../../../src/services/token');
const userService = require('../../../src/services/user');
const { of, throwError } = require('rxjs');

describe('Authenticate Middleware', () => {
    it('should next with no req.user if Authorization header not found', () => {
        const req = { get() { } };
        const next = () => { };
        const authenticateMiddleware = authenticate();

        authenticateMiddleware(req, null, next);

        expect(req.user).toBeFalsy();
    });

    it('should remove Bearer if exists', () => {
        const headerName = 'Authorization';
        const headerValue = 'Bearer token';
        const req = { get() { } };
        const next = () => { };
        spyOn(req, 'get').withArgs(headerName).and.returnValue(headerValue);
        spyOn(tokenService, 'verifyToken').and.returnValue(of());
        const authenticateMiddleware = authenticate();

        authenticateMiddleware(req, null, next);

        expect(tokenService.verifyToken).toHaveBeenCalledWith('token');
    });

    it('should next with no req.user if token is invalid', (done) => {
        const headerName = 'Authorization';
        const headerValue = 'Bearer invalid-token';
        const error = 'error';
        const req = { get() { } };
        const next = () => { done(); };
        spyOn(req, 'get').withArgs(headerName).and.returnValue(headerValue);
        spyOn(tokenService, 'verifyToken').and.returnValue(throwError(error));
        const authenticateMiddleware = authenticate();

        authenticateMiddleware(req, null, next);

        expect(tokenService.verifyToken).toHaveBeenCalledWith('invalid-token');
        expect(req.user).toBeFalsy();
    });

    it('should have get user if token is valid', (done) => {
        const headerName = 'Authorization';
        const headerValue = 'Bearer valid-token';
        const payload = { id: 'id' };
        const user = { user: 'user' };
        const req = { get() { } };
        const next = () => { done(); };
        spyOn(req, 'get').withArgs(headerName).and.returnValue(headerValue);
        spyOn(tokenService, 'verifyToken').and.returnValue(of(payload));
        spyOn(userService, 'findUserById').withArgs(payload.id)
            .and.returnValue(of(user));
        const authenticateMiddleware = authenticate();

        authenticateMiddleware(req, null, next);

        expect(userService.findUserById).toHaveBeenCalled();
        expect(req.user).toBe(user);
    });
});