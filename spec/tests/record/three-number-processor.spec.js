const threeNumberProcessor = require('../../../src/services/records/types/three-number/processor');
const config = require('../../../src/services/records/types/three-number/config');

describe('ThreeNumberProcessor', () => {
    describe('canProcess()', () => {
        const originPattern = config.pattern;
        afterEach(() => {
            config.pattern = originPattern;
        });

        it('should false if number is null', () => {
            const falsy = threeNumberProcessor.canProcess();

            expect(falsy).toBeFalsy();
        });

        it('should true if number matches configuration pattern', () => {
            const pattern = /^\d{3}$/;
            const number = "123";
            config.pattern = pattern;

            expect(threeNumberProcessor.canProcess(number)).toBeTruthy();
        });
    });

    describe('process()', () => {
        const originPattern = config.pattern;
        afterEach(() => {
            config.pattern = originPattern;
        });

        it('should throw error if source is null', () => {
            expect(() => threeNumberProcessor.process(0)).toThrowError();
        });

        it('should throw error if source is not Array', () => {
            const invalidSource = {};

            expect(() => threeNumberProcessor.process(0, invalidSource)).toThrowError();
        });

        it('should throw error if number is not match pattern', () => {
            const pattern = /^\d{3}$/;
            const number = 0;
            config.pattern = pattern;

            expect(() => threeNumberProcessor.process(number, [])).toThrowError();
        });

        it('should process failed', () => {
            const pattern = /^\d{3}$/;
            const number = "023";
            config.pattern = pattern;
            const source = ['12102', '1033'];

            expect(threeNumberProcessor.process(number, source)).toBeFalsy();
        });
    });
});