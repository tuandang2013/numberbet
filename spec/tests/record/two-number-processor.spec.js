const twoNumberProcessor = require('../../../src/services/records/types/two-number/processor');
const config = require('../../../src/services/records/types/two-number/config');

describe('TwoNumberProcessor', () => {
    describe('canProcess()', () => {
        const originPattern = config.pattern;
        afterEach(() => {
            config.pattern = originPattern;
        });

        it('should false if number is null', () => {
            const falsy = twoNumberProcessor.canProcess();

            expect(falsy).toBeFalsy();
        });

        it('should true if number matches configuration pattern', () => {
            const pattern = /^\d{2}$/;
            const number = "12";
            config.pattern = pattern;

            expect(twoNumberProcessor.canProcess(number)).toBeTruthy();
        });
    });

    describe('process()', () => {
        const originPattern = config.pattern;
        afterEach(() => {
            config.pattern = originPattern;
        });

        it('should throw error if source is null', () => {
            expect(() => twoNumberProcessor.process(0)).toThrowError();
        });

        it('should throw error if source is not Array', () => {
            const invalidSource = {};

            expect(() => twoNumberProcessor.process(0, invalidSource)).toThrowError();
        });

        it('should throw error if number is not match pattern', () => {
            const pattern = /^\d{2}$/;
            const number = 0;
            config.pattern = pattern;

            expect(() => twoNumberProcessor.process(number, [])).toThrowError();
        });

        it('should process success', () => {
            const pattern = /^\d{2}$/;
            const number = "02";
            config.pattern = pattern;
            const source = ['102', '103'];

            expect(twoNumberProcessor.process(number, source)).toBeTruthy();
        });
    });
});