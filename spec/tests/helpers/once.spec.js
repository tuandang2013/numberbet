const { once } = require('../../../src/helpers/index');

describe('once()', () => {
    it('should run for first time', () => {
        const fn = () => 0;

        expect(once(fn)()).toEqual(0);
    });

    it('should skip after first time', () => {
        const fn = () => 0;
        const context = { fn };
        spyOn(context, 'fn').and.callThrough();

        const resultFn = once(context.fn);
        resultFn();
        resultFn();

        expect(context.fn).toHaveBeenCalledTimes(1);
    });
});