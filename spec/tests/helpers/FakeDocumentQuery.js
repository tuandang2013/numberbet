
function FakeDocumentQuery(value) {
    return {
        exec(fn) {
            if (fn)
                fn(null, value);
            else
                return Promise.resolve(value);
        },
        populate(){
            return this;
        }
    };
}

module.exports = FakeDocumentQuery;