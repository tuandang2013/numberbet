const { of } = require('rxjs');

class FakeParser { parse() { } };
const fakeProcess = () => of({});

const fakeSource = () => ({
    url: 'url',
    sections: [{
        parser: FakeParser,
        process: fakeProcess,
        dailyUrl(date) { return 'daily-url'; }
    }],
    process() { }
});

module.exports = fakeSource;