const pagerHelper = require('../../../src/helpers/pager.helper');

describe('PagerHelper', () => {
    describe('getPager()', () => {
        it('should return pager info', () => {
            const pageNumber = 1;
            const pageSize = 3;
            const count = 7;

            const pager = pagerHelper.getPager({pageNumber, pageSize, count});

            expect(pager).toEqual({
                pageNumber,
                pageSize,
                totalItems: count,
                totalPages: Math.ceil(count / pageSize)
            });
        });
    });
});