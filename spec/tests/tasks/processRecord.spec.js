const processRecord = require('../../../src/tasks/process-record');
const userService = require('../../../src/services/user');
const recordService = require('../../../src/services/record');
const { of } = require('rxjs');

describe('ProcessRecord Task', () => {
    it('process for each user', (done) => {
        const users = [{ name: 'user1', email: '' }, { name: 'user2', email: '' }];
        spyOn(userService, 'getAllUsers').and.returnValue(of(users));
        spyOn(recordService, 'processForUser').and.returnValue(of({}));

        processRecord.task().subscribe(null, null, () => {
            expect(userService.getAllUsers).toHaveBeenCalled();
            expect(recordService.processForUser).toHaveBeenCalledTimes(users.length);
            done();
        });
    });

    it('should update info time after complete', (done) => {
        const users = [{ name: 'user1', email: '' }, { name: 'user2', email: '' }];
        spyOn(userService, 'getAllUsers').and.returnValue(of(users));
        spyOn(recordService, 'processForUser').and.returnValue(of({}));

        processRecord.task().subscribe(null, null, () => {
            setImmediate(() => {
                //expect(processRecord.info.lastProcessTime).toBeTruthy();
                //expect(processRecord.info.totalProcessTime).toBeTruthy();
                expect(processRecord.info.nextRunTime).toBeTruthy();
                expect(processRecord.info.lastRunTime).toBeTruthy();
                done();
            });
        });
    });
});