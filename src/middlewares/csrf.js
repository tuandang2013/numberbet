const csurf = require('csurf');

const csrfProtection = csurf({ cookie: false });

function csrf() {
    return csrfProtection;
}

module.exports = csrf;