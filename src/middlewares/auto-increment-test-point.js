const goalPointService = require('../services/goal-point');

const autoIncrementTestPoint = () => {
    return function (req, res, next) {
        const { user } = req;
        if (user == null) {
            next();
            return;
        }
        goalPointService.addDailyTestGoalPointEntryForUser(user, new Date())
            .subscribe(() => {
                next();
            }, error => {
                next();
            });
    };
};

module.exports = autoIncrementTestPoint;