
const authorizeMiddleware = (claims, { type } = {}) => ({user}, res, next) => {
    if (!user) {
        return unauthorized(type, res);
    }
    if (claims && claims.length) {
        if (!user.claims || !user.claims.length) {
            return unauthorized(type, res);
        }
        for (const claimName of claims) {
            if (!user.claims.some(claim => claim.name === claimName)) {
                return unauthorized(type, res);
            }
        }
    }
    next();
};

const unauthorized = (type, res) => {
    res.statusCode = 401;
    switch (type) {
        case 'json':
            res.json({ error: { message: 'Unauthorized' } });
            break;
        default:
            res.end('Unauthorized');
            break;
    };
}

module.exports = authorizeMiddleware;