const tokenService = require('../services/token');
const userService = require('../services/user');
const { switchMap } = require('rxjs/operators');

function authenticate() {
    return function (req, res, next) {
        const authorization = req.get('Authorization');
        if (authorization != null) {
            const token = getActualToken(authorization);
            tokenService.verifyToken(token).pipe(
                switchMap(payload => userService.findUserById(payload.id))
            ).subscribe(user => {
                req.user = user;
                next();
            }, err => next());
        } else {
            next();
        }
    };
};

function getActualToken(value) {
    if (value == null)
        throw new Error('Value must be not null');
    const index = value.indexOf(' ');
    return value.substring(index + 1);
}

module.exports = authenticate;