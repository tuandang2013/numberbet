function csrfTokenErrorHandle() {
    return function (err, req, res, next) {
        if (err.code !== 'EBADCSRFTOKEN') return next(err)

        // handle CSRF token errors here
        res.status(403);
        res.send('Request Forbidden');
    };
}

module.exports = csrfTokenErrorHandle;