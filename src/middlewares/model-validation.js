const { validationResult } = require('express-validator');

const getValidationErrors = validationResult.withDefaults({
    formatter: ({ location, msg, param, value, nestedErrors }) => {
        return `${msg}`;
    }
});

function modelValidation(rules) {
    return function (req, res, next) {
        if (rules == null || !(rules instanceof Array) || rules.length === 0) {
            next();
            return;
        }
        Promise.all(rules.map(rule => rule.run(req)))
            .then(() => {
                const errors = getValidationErrors(req);
                if (!errors.isEmpty()) {
                    let modelErrors = Array.from(req.modelErrors || []);
                    modelErrors = modelErrors.concat(errors.mapped());
                    req.modelErrors = modelErrors;
                }
                next();
            });
    };
}

module.exports = modelValidation;