const session = require('express-session');
const config = require('../config');

function appSession() {
    return session({ secret: config.SESSION_SECRET, resave: false, saveUninitialized: true });
}

module.exports = appSession;