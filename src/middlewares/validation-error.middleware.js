function validationErrorMiddleware({ type }) {
    return function (req, res, next) {
        if (req.modelErrors) {
            res.statusCode = 400;
            switch (type) {
                case 'json':
                    return res.json(req.modelErrors);
                default:
                    return res.end(req.modelErrors);
            };
        }
        return next();
    };
}

module.exports = validationErrorMiddleware;