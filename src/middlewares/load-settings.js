const getAll = require('../models/Setting').getAll;

module.exports = function loadSettingMiddleware() {
    return function (req, res, next) {
        getAll().then((settings = []) => {
            const target = {};
            for (const { name, value } of settings) {
                target[name] = value;
            }
            res.locals.settings = target;
            next();
        });
    };
};