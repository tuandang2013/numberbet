const express = require('express'),
    router = express.Router(),
    csrf = require('../middlewares/csrf'),
    csrfErrorHandle = require('../middlewares/csrf-error-handle');

const southNumberRoute = require('./south-number/south-number.route'),
    numberRoute = require('./number/number.route'),
    userRoute = require('./user/user.route'),
    commonRoute = require('./common/common.route'),
    recordRoute = require('./record/record.route'),
    taskRoute = require('./tasks/task.route'),
    goalRoute = require('./goal/goal.route'),
    claimRoute = require('./claim/claim.route');

router
    .use(csrf())
    .use('/numbers', numberRoute)
    .use('/south-numbers', southNumberRoute)
    .use('/users', userRoute)
    .use('/claims', claimRoute)
    .use('/common', commonRoute)
    .use('/records', recordRoute)
    .use('/tasks', taskRoute)
    .use('/goals', goalRoute)
    .use(csrfErrorHandle());

module.exports = router;