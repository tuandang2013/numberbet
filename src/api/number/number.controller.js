const northNumberService = require('../../services/north-number');
const southNumberService = require('../../services/south-number');
const sourceService = require('../../services/source');
const dateTimeHelperService = require('../../services/date-helper');
const xsktSource = require('../../services/resources/index').xskt;
const { forkJoin, of, iif, defer } = require('rxjs');
const { map, switchMap, catchError } = require('rxjs/operators');
const responses = require('../responses');

class NumberController {
    _source = xsktSource;

    $southNumber = (id) => southNumberService.getById(id);
    $northNumber = (id) => northNumberService.getById(id);

    $getSource = (date) => sourceService.getByDate(date).pipe(
        switchMap(source => forkJoin([
            this.$northNumber(source.north.id),
            this.$southNumber(source.south.id)]
        ))
    );

    $requestData = (source) => sourceService.getData(null, source);

    lastest() {
        const date = new Date();
        const requestDate = dateTimeHelperService.getRequestableDate(date);
        const source = this._source;

        return sourceService.has(requestDate).pipe(
            switchMap(exists => iif(() => exists,
                defer(() => this.$getSource(requestDate)),
                defer(() => this.$requestData(this._source)))),
            map(([north, south]) => ({
                name: source.name,
                url: source.url,
                date: north.date,
                numbers: { north, south },
                createdDate: date
            })),
            map((data) => responses.json(data)),
            catchError(error => {
                return of(responses.jsonError500());
            })
        );
    }
}

module.exports = new NumberController();