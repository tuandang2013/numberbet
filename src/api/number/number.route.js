const express = require('express');
const router = express.Router();
const controller = require('./number.controller');
const renderer = require('../renderer');

router.route(['/', '/lastest'])
    .get((req, res) => controller.lastest().subscribe(renderer(req, res)));

module.exports = router;