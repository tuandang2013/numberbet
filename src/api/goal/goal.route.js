const express = require('express');
const router = express.Router();
const controller = require('./goal.controller');
const renderer = require('../renderer');
const authorize = require('../../middlewares/authorize.middleware');
const { ADMINISTRATOR } = require('../../models/Claim');
const modelValidation = require('../../middlewares/model-validation');
const validationError = require('../../middlewares/validation-error.middleware');
const checkoutValidator = require('../../validators/goal/checkout.validator');
const { requiredParamId } = require('../../validators/common');

router.get('/',
    authorize(null, { type: 'json' }),
    (req, res) => controller.getGoals(req)
        .subscribe(renderer(req, res)));

router.get('/goal-point',
    authorize(null, { type: 'json' }),
    (req, res) => controller.getGoalPoints(req)
        .subscribe(renderer(req, res)));

router.get('/test-goal-point',
    authorize(null, { type: 'json' }),
    (req, res) => controller.getTestGoalPoints(req)
        .subscribe(renderer(req, res)));

router.route('/user/:userId/goal-point')
    .get(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation([requiredParamId('userId')]),
        validationError({ type: 'json' }),
        (req, res) => controller.getUserGoalPoints(req.params.userId, req.query)
            .subscribe(renderer(req, res)));

router.post('/user/:userId/checkout',
    authorize([ADMINISTRATOR], { type: 'json' }),
    modelValidation(checkoutValidator),
    validationError({ type: 'json' }),
    (req, res) => controller.checkoutForUser(req.params.userId, req.body)
        .subscribe(renderer(req, res)));

router.get('/user/:userId/test-goal-point',
    authorize([ADMINISTRATOR], { type: 'json' }),
    modelValidation([requiredParamId('userId')]),
    validationError({ type: 'json' }),
    (req, res) => controller.getUserTestGoalPoints(req.params.userId, req.query)
        .subscribe(renderer(req, res)));

router.get('/user/:userId',
    authorize([ADMINISTRATOR], { type: 'json' }),
    modelValidation([requiredParamId('userId')]),
    validationError({ type: 'json' }),
    (req, res) => controller.getUserGoals(req.params.userId, req.query)
        .subscribe(renderer(req, res)));

module.exports = router;