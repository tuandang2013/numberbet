const { forkJoin, of, iif, defer } = require('rxjs');
const { map, switchMap, catchError } = require('rxjs/operators');
const goalPointService = require('../../services/goal-point');
const userService = require('../../services/user');
const responses = require('../responses');
const config = require('../../config');

function getGoals({ user }) {
    return userPoints$(user).pipe(
        map(goalPoints => responses.json(goalPoints)),
        catchError(() => of(responses.jsonError500()))
    );
}

function getGoalPoints({
    user,
    query: {
        pageSize = config.DEFAULT_PAGESIZE,
        pageNumber = 1
    } = {}
} = {}) {
    return userGoalPoint$(user, pageNumber, pageSize).pipe(
        map(goalPointEntryData => responses.json(goalPointEntryData)),
        catchError(() => of(responses.jsonError500()))
    );
}

function getTestGoalPoints({
    user,
    query: {
        pageSize = config.DEFAULT_PAGESIZE,
        pageNumber = 1
    } = {}
} = {}) {
    return userTestGoalPoint$(user, pageNumber, pageSize).pipe(
        map(testGoalPointEntryData => responses.json(testGoalPointEntryData)),
        catchError(() => of(responses.jsonError500()))
    );
}

function getUserGoals(id, {
    pageSize = config.DEFAULT_PAGESIZE
} = {}) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => userPoints$(user, pageSize).pipe(
                map(data => responses.json(data)),
                catchError(() => of(responses.jsonError500()))
            )),
            of(responses.jsonError404())
        ))
    );
}

function getUserGoalPoints(
    id, {
        pageSize = config.DEFAULT_PAGESIZE,
        pageNumber = 1
    } = {}) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => userGoalPoint$(user, pageNumber, pageSize).pipe(
                map(data => responses.json(data)),
                catchError(() => of(responses.jsonError500()))
            )),
            of(responses.jsonError404())
        ))
    );
}

function getUserTestGoalPoints(
    id, {
        pageSize = config.DEFAULT_PAGESIZE,
        pageNumber = 1
    } = {}) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => userTestGoalPoint$(user, pageNumber, pageSize).pipe(
                map(data => responses.json(data)),
                catchError(() => of(responses.jsonError500()))
            )),
            of(responses.jsonError404())
        ))
    );
}

function checkoutForUser(id, { amount } = {}) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => processCheckout$(user, amount)),
            of(responses.jsonError404('User is not found'))
        ))
    );
}

const user$ = id => userService.findUserById(id);

const goalPointAmount$ = user => goalPointService.getGoalPointAmountForUser(user);

const userGoalPoint$ = (user, pageNumber, pageSize) => goalPointService.getGoalPointEntriesForUser(user, { pageNumber, pageSize });

const userTestGoalPoint$ = (user, pageNumber, pageSize) => goalPointService.getTestGoalPointEntriesForUser(user, { pageNumber, pageSize });

const userPoints$ = (user, pageSize = config.DEFAULT_PAGESIZE) => forkJoin([
    userGoalPoint$(user, 1, pageSize),
    userTestGoalPoint$(user, 1, pageSize)
]).pipe(
    map(([goalPointEntries, testGoalPointEntries]) => ({
        goalPoint: goalPointEntries,
        testGoalPoint: testGoalPointEntries
    }))
);

const processCheckout$ = (user, amount) => goalPointAmount$(user).pipe(
    switchMap(goalPointAmount => iif(() => goalPointAmount > amount,
        defer(() => goalPointService.addGoalPointCheckoutForUser(user, amount).pipe(
            map(goalPoint => responses.json({ success: true, data: goalPoint }))
        )),
        of(responses.jsonError400('Goal point of user is less than request'))
    ))
);

module.exports = {
    getGoals,
    getGoalPoints,
    getTestGoalPoints,
    getUserGoals,
    getUserGoalPoints,
    getUserTestGoalPoints,
    checkoutForUser
}