const claimService = require('../../services/claim.service');
const userService = require('../../services/user');
const { from, of, iif, defer } = require('rxjs');
const { map, switchMap, concatMap, last } = require('rxjs/operators');
const responses = require('../responses');

function getClaims() {
    return claimService.getAllClaims().pipe(
        map(claims => claims.sort((item1, item2) => item1.name.localeCompare(item2.name))),
        map(claims => claims.map(claim => _claimDetail(claim))),
        map(claims => responses.json(claims)));
}

function getUserClaims({ user }) {
    return of(user.claims).pipe(
        map(claims => claims.sort((item1, item2) => item1.name.localeCompare(item2.name))),
        map(claims => claims.map(claim => _claimDetail(claim))),
        map(claims => responses.json(claims))
    );
}

function getClaimsByUserId(id) {
    if (id == null)
        return of(responses.jsonError400('Id must be not null'));

    return userService.findUserById(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => claimsForUser$(user).pipe(
                map(claims => claims.map(claim => _claimDetail(claim))),
                map(claims => responses.json(claims))
            )),
            of(responses.jsonError400('User is not found'))
        ))
    );
}

function addClaimsForUser(id, claims) {
    if (id == null)
        return of(responses.jsonError400('Id must be not null'));
    if (claims == null)
        return of(responses.jsonError400('Claims must be not null'));

    return userById$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => addClaims$(user, claims).pipe(
                last(),
                map(claims => responses.json(claims))
            )),
            defer(() => of(responses.jsonError400('User is not found')))
        ))
    );
}

function removeClaimsForUser(id, claims) {
    if (id == null)
        return of(responses.jsonError400('Id must be not null'));
    if (claims == null)
        return of(responses.jsonError400('Claims must be not null'));

    return userById$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => removeClaims$(user, claims).pipe(
                last(),
                map(claims => responses.jsonError200(claims))
            )),
            of(responses.jsonError400('User is not found'))
        ))
    );
}

const userById$ = (id) => userService.findUserById(id);

const claimsForUser$ = (user) => claimService.getClaimsForUser(user).pipe(
    map(claims => claims.sort((cl1, cl2) => cl1.name.localeCompare(cl2.name)))
);

const addClaims$ = (user, claims) => from(claims).pipe(
    concatMap(claim => addClaim$(user, claim))
);

const removeClaims$ = (user, claims) => from(claims).pipe(
    concatMap(claim => removeClaim$(user, claim))
);

const addClaim$ = (user, claim) => claimService.addClaimForUser(user, claim);

const removeClaim$ = (user, claim) => claimService.removeClaimForUser(user, claim);

const _claimDetail = claim => ({
    id: claim._id,
    name: claim.name
})

module.exports = {
    getClaims,
    getUserClaims,
    getClaimsByUserId,
    addClaimsForUser,
    removeClaimsForUser
};