const express = require('express');
const router = express.Router();
const controller = require('./claim.controller');
const renderer = require('../renderer');
const authorize = require('../../middlewares/authorize.middleware');
const { ADMINISTRATOR } = require('../../models/Claim');
const modelValidation = require('../../middlewares/model-validation');
const validationError = require('../../middlewares/validation-error.middleware');
const { requiredParamId } = require('../../validators/common');

router.get('/user',
    authorize(null, { type: 'json' }),
    (req, res) => controller.getUserClaims(req)
        .subscribe(renderer(req, res))
);

router.route('/user/:userId')
    .get(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation([requiredParamId('userId')]),
        validationError({ type: 'json' }),
        (req, res) => controller.getClaimsByUserId(req.params.userId)
            .subscribe(renderer(req, res)))
    .post(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation([requiredParamId('userId')]),
        validationError({ type: 'json' }),
        (req, res) => controller.addClaimsForUser(req.params.userId, req.body)
            .subscribe(renderer(req, res)))
    .delete(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation([requiredParamId('userId')]),
        validationError({ type: 'json' }),
        (req, res) => controller.removeClaimsForUser(req.params.userId, req.body)
            .subscribe(renderer(req, res))
    );

router.route('/')
    .get(
        authorize([ADMINISTRATOR], { type: 'json' }),
        (req, res) => controller.getClaims(req)
            .subscribe(renderer(req, res)));

module.exports = router;