const responses = {
    json: (data) => ({
        statusCode: 200,
        type: 'json',
        data
    }),
    jsonError200: (error) => ({
        statusCode: 200,
        type: 'json',
        data: { error }
    }),
    jsonError400: (error) => ({
        statusCode: 400,
        type: 'json',
        data: { error }
    }),
    jsonErrorMessage400: (message) => ({
        statusCode: 400,
        type: 'json',
        data: { error: { message } }
    }),
    jsonError401: () => ({
        statusCode: 401,
        type: 'json',
        data: { error: { message: 'Unauthorized' } }
    }),
    jsonError404: () => ({
        statusCode: 404,
        type: 'json',
        data: { error: { message: 'Not Found' } }
    }),
    jsonError500: () => ({
        statusCode: 500,
        type: 'json',
        data: { error: { message: 'Internal Server Error' } }
    }),
    error500: () => ({
        statusCode: 500,
        type: 'text',
        data: 'Internal Server Error'
    }),
    error400: (message) => ({
        statusCode: 400,
        type: 'text',
        data: message
    })
};

module.exports = responses;