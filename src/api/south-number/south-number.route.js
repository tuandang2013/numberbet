const express = require('express');
const router = express.Router();
const controller = require('./south-number.controller');

router.route('/')
    .get(controller.get);

module.exports = router;