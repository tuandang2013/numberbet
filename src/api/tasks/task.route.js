const express = require('express');
const router = express.Router();
const controller = require('./task.controller');
const renderer = require('../renderer');
const authorize = require('../../middlewares/authorize.middleware');
const { ADMINISTRATOR } = require('../..//models/Claim');

router.post('/process-record',
    authorize([ADMINISTRATOR], { type: 'json' }),
    (req, res) => controller.processRecord()
        .subscribe(renderer(req, res)));

module.exports = router;