const responses = require('../responses');
const processRecordTask = require('../../tasks/process-record');
const { Observable } = require('rxjs');

function processRecord() {
    return new Observable(observer => {
        processRecordTask.task().subscribe(null,
            error => {
                console.log(error);
                observer.next(responses.jsonError500())
            },
            () => {
                observer.next(responses.json({ done: true }));
                observer.complete();
            });
    });
}

module.exports = {
    processRecord
};