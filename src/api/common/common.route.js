const express = require('express');
const router = express.Router();
const controller = require('./common.controller');
const renderer = require('../renderer');

router.get('/csrf', (req, res) => controller.getCsrfToken(req)
    .subscribe(renderer(req, res)));

module.exports = router;