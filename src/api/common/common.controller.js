const { of } = require('rxjs');
const responses = require('../responses');

function getCsrfToken({ csrfToken } = {}) {
    return of(responses.json({ token: csrfToken() }));
}

module.exports = {
    getCsrfToken
}