const { forkJoin, of, iif, defer } = require('rxjs');
const { map, catchError, switchMap } = require('rxjs/operators');
const responses = require('../responses');
const recordInfo = require('../../services/records/bet-record-info');
const recordService = require('../../services/record');
const userService = require('../../services/user');
const goalPointService = require('../../services/goal-point');
const logger = require('../../services/logger');
const { NORTH_AREA } = require('../../models/constants/areas');
const recordTypes = require('../../services/records/types');
const moment = require('moment');
const dateTimeHelper = require('../../services/date-helper');
const config = require('../../config');
const converter = require('../../helpers/converter');
const { getPager } = require('../../helpers/pager.helper');

function getInfo() {
    return of(responses.json(recordInfo.betRecordInfo));
}

function getTodayInfo() {
    const dayOfWeek = moment(Date.now()).day();
    let channelIndex = dayOfWeek;
    if (!dateTimeHelper.isBeforeEndTime()) {
        channelIndex++;
        if (channelIndex > 6) {
            channelIndex = 0;
        }
    }

    return of(responses.json({
        ...recordInfo.betRecordInfo,
        areas: [
            { ...recordInfo.northBetRecordInfo, channels: recordInfo.northBetRecordInfo.channels[channelIndex] },
            { ...recordInfo.southBetRecordInfo, channels: recordInfo.southBetRecordInfo.channels[channelIndex] }
        ],
        date: recordService.getRecordableDate()
    }));
}

function getEntries({ user } = {}) {
    if (user.recordId == null) {
        return of(responses.json([]));
    }
    return userEntries$(user).pipe(
        map(entries => responses.json(entries)),
        catchError(() => of(responses.error500()))
    );
}

function getHistory({
    user,
    query: {
        pageSize = config.DEFAULT_PAGESIZE,
        pageNumber = 1
    } = {}
} = {}) {
    pageNumber = converter.toNumber(pageNumber, 1);
    if (pageNumber < 1) pageNumber = 1;

    pageSize = converter.toNumber(pageSize, config.DEFAULT_PAGESIZE);
    if (pageSize <= 0) pageSize = config.DEFAULT_PAGESIZE;

    return userHistories$(user, pageNumber, pageSize).pipe(
        map(histories => responses.json(histories)),
        catchError(() => of(responses.jsonError500()))
    );
}

function getUserRecords(id, {
    pageSize = config.DEFAULT_PAGESIZE,
    pageNumber = 1
} = {}) {
    pageNumber = converter.toNumber(pageNumber, 1);
    if (pageNumber < 1) pageNumber = 1;

    pageSize = converter.toNumber(pageSize, config.DEFAULT_PAGESIZE);
    if (pageSize <= 0) pageSize = config.DEFAULT_PAGESIZE;

    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => forkJoin([
                userHistories$(user, pageNumber, pageSize),
                userEntries$(user)
            ]).pipe(
                map(([histories, entries]) => ({
                    id,
                    histories,
                    entries
                })),
                map(data => responses.json(data)),
                catchError(() => of(responses.jsonError500()))
            )),
            of(responses.jsonError404())
        ))
    );
}

function createEntry({ user, body: data, modelErrors } = {}) {
    if (modelErrors != null) {
        return of(responses.jsonError400(modelErrors));
    }
    if (data == null) {
        return of(responses.jsonErrorMessage400('Bad Request'));
    }

    if (!data.channel && data.area != NORTH_AREA) {
        return of(responses.jsonError400('Channel is required'));
    }

    const recordType = recordTypes.find(recordType => recordType.info.code === data.type);
    if (recordType == null) {
        return of(responses.jsonError400('Type is invalid'));
    } else if (data.point % recordType.config.pointRate[data.area] !== 0) {
        return of(responses.jsonError400('Point is not enough or invalid'));
    }

    const entryDate = new Date(data.date);
    const entryDateM = moment(entryDate);
    const now = new Date(Date.now());
    if (entryDateM.isBefore(now, 'day')) {
        return of(responses.jsonError400('Date must be greater than or equal today'));
    } else {
        const endDate = dateTimeHelper.getEndDate(now);
        if (moment(now).isSameOrAfter(endDate) && !entryDateM.isAfter(now, 'day')) {
            return of(responses.jsonError400('Today is ended. Please play for tomorror'));
        }
    }

    return recordService.addRecordEntryForUser(user, data).pipe(
        switchMap(() => goalPointService.getPointInfoForUser(user), (entry, pointInfo) => responses.json({
            success: true,
            entry,
            pointInfo
        })),
        catchError(error => {
            logger.error('[RecordController][createEntry] ERROR', error);

            return of(responses.error500());
        })
    );
}

function updateEntry({ user, body: { data }, modelErrors } = {}) {
    if (modelErrors != null) {
        return of(responses.jsonError400(modelErrors));
    }
    if (data == null || data.id == null) {
        return of(responses.jsonErrorMessage400('Bad Request'));
    }

    return recordService.alterRecordEntryForUser(user, data.id, data).pipe(
        map(entry => responses.json(entry)),
        catchError(() => of(responses.error500()))
    );
}

function deleteEntry({ user, id } = {}) {
    if (id == null) {
        return of(responses.jsonErrorMessage400('Bad Request'));
    }
    return recordService.deleteRecordEntryForUser(user, id).pipe(
        map(() => responses.json({ success: true })),
        catchError(() => of(responses.error500()))
    );
}

function getRecordableDate() {
    const recordableDate = recordService.getRecordableDate();
    return of(responses.json(recordableDate.toJSON()));
}

const userHistories$ = (user, pageNumber, pageSize) => recordService.getRecordHistoryForUser(user).pipe(
    map(histories => histories.sort((item1, item2) => item2.createdDate.getTime() - item1.createdDate.getTime())),
    map(histories => ({
        data: histories.filter((_, index) => index >= (pageNumber - 1) * pageSize && index < pageNumber * pageSize),
        pager: getPager({ pageNumber, pageSize, count: histories.length })
    }))
);

const userEntries$ = (user) => recordService.getRecordEntriesForUser(user).pipe(
    map(entries => entries.sort((item1, item2) => item2.createdDate.getTime() - item1.createdDate.getTime()))
);

const user$ = (id) => userService.findUserById(id);

module.exports = {
    getInfo,
    getTodayInfo,
    getEntries,
    createEntry,
    updateEntry,
    deleteEntry,
    getRecordableDate,
    getHistory,
    getUserRecords
}