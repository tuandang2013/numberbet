const express = require('express');
const router = express.Router();
const controller = require('./record.controller');
const renderer = require('../renderer');
const recordEntryValidator = require('../../validators/record/entry');
const modelValidation = require('../../middlewares/model-validation');
const validationError = require('../../middlewares/validation-error.middleware');
const authorize = require('../../middlewares/authorize.middleware');
const { ADMINISTRATOR } = require('../../models/Claim');
const { requiredParamId } = require('../../validators/common');

router.get('/info', (req, res) => controller.getInfo()
    .subscribe(renderer(req, res)));

router.get('/info/today', (req, res) => controller.getTodayInfo()
    .subscribe(renderer(req, res)));

router.get('/date', (req, res) => controller.getRecordableDate()
    .subscribe(renderer(req, res)));

router.get('/history', authorize(null, { type: 'json' }), (req, res) => controller.getHistory(req)
    .subscribe(renderer(req, res)));

router.get('/user/:userId',
    authorize([ADMINISTRATOR], { type: 'json' }),
    modelValidation([requiredParamId('userId')]),
    validationError({ type: 'json' }),
    (req, res) => controller.getUserRecords(req.params.userId, req.query)
        .subscribe(renderer(req, res)));

router.route('/')
    .get(
        authorize(null, { type: 'json' }),
        (req, res) => controller.getEntries(req)
            .subscribe(renderer(req, res)))
    .post(
        authorize(null, { type: 'json' }),
        modelValidation(recordEntryValidator),
        (req, res) => controller.createEntry(req)
            .subscribe(renderer(req, res)))
    .put(
        authorize(null, { type: 'json' }),
        modelValidation(recordEntryValidator),
        (req, res) => controller.updateEntry(req)
            .subscribe(renderer(req, res)));

router.delete('/:id([a-zA-Z0-9]+)',
    authorize(null, { type: 'json' }),
    modelValidation([requiredParamId('id')]),
    validationError({ type: 'json' }),
    (req, res) => controller.deleteEntry({ user: req.user, id: req.params.id })
        .subscribe(renderer(req, res)));

module.exports = router;