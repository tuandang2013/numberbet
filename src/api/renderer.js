const render = (req, res) => (response) => {
    res.statusCode = response.statusCode;
    switch (response.type) {
        case 'json':
            res.json(response.data);
            break;
        case 'text':
            res.end(response.data);
            break;
        default:
            res.end('');
    }
};

module.exports = render;