const userService = require('../../services/user');
const tokenService = require('../../services/token');
const goalPointService = require('../../services/goal-point');
const { from, forkJoin, of, iif, defer } = require('rxjs');
const { toArray, switchMap, concatMap, map, catchError } = require('rxjs/operators');
const responses = require('../responses');
const config = require('../../config');
const claimService = require('../../services/claim.service');
const recordService = require('../../services/record');

function userInfo({ user } = {}) {
    if (user != null) {
        return goalPointService.getPointInfoForUser(user).pipe(
            map(({ goalPoint, testGoalPoint }) => responses.json({
                email: user.email,
                goalPoint,
                testGoalPoint,
                claims: user.claims.map(claim => claim.name)
            })),
            catchError(() => of(responses.jsonError500()))
        );
    }
    return of(responses.json(null));
}

function login({ email, password, remember } = {}, modelErrors) {
    if (modelErrors)
        return of(responses.jsonError400(modelErrors));
    if (email == null)
        return of(_failedResult());
    if (password == null)
        return of(_failedResult());

    return userService.findUserByEmailAndPassword(email, password).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => goalPointService.getPointInfoForUser(user).pipe(
                map(({ goalPoint, testGoalPoint }) => _successResult(user, goalPoint, testGoalPoint, remember, user.claims))
            )),
            defer(() => of(_failedResult()))
        ))
    );
}

function register({ email, password, phone } = {}, modelErrors) {
    if (modelErrors)
        return of(responses.jsonError400(modelErrors));
    if (email == null)
        return of(responses.jsonErrorMessage400('Email must be not null'));
    if (password == null)
        return of(responses.jsonErrorMessage400('Password must be not null'));

    return userService.emailExists(email).pipe(
        switchMap(exists =>
            iif(() => exists,
                of(responses.jsonErrorMessage400('Email is existing')),
                defer(() => processInsertUser$(email, password, phone)))
        ),
        catchError(() => of(responses.error500())));
}

function getUsers({ pageNumber = 1, pageSize = config.DEFAULT_PAGESIZE } = {}) {
    return userService.getUsers({ pageNumber, pageSize }).pipe(
        switchMap(userData => getUsersInfo$(userData.data), (userData, userInfos) => ({ data: userInfos, pager: userData.pager })),
        map(result => responses.json(result)),
        catchError(_ => of(responses.jsonError500()))
    );
}

function deleteUser(id) {
    if (id == null)
        return of(responses.jsonError400('Is must not be null'));

    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => userService.deleteUser(user).pipe(
                map(() => responses.json())
            )),
            of(responses.jsonError400('User is not found'))
        ))
    );
}

function updateUser({ id, email, phone, claims }) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => processUpdateUser$(user, email, phone, claims)),
            of(responses.jsonError400('User is not found'))
        ))
    );
}

function exists(email) {
    if (email == null)
        return of(responses.jsonError400('Email must be not null'));

    return from(userService.findUserByEmail(email)).pipe(
        map(user => !!user),
        map(exists => responses.json({ exists })),
        catchError(() => of(responses.jsonError500()))
    );
}

function changePassword(id, { password, oldPassword }) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => processChangePassword$(user, oldPassword, password)),
            of(responses.jsonError400('User is not found'))
        ))
    );
}

function adminChangePassword(id, { password }, req) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => changePassword$(user, password)),
            of(responses.jsonError400('User is not found'))
        ))
    );
}

function getUserDetails(id) {
    return user$(id).pipe(
        switchMap(user => iif(() => user != null,
            defer(() => of(responses.json(_userDetails(user)))),
            of(responses.jsonError404())
        ))
    );
}

const user$ = (id) => userService.findUserById(id);

const userByEmail$ = email => userService.findUserByEmail(email);

const getUsersInfo$ = (users) => from(users).pipe(
    concatMap(user => userInfo$(user)),
    toArray()
);

const userInfo$ = (user) => forkJoin([
    goalPointService.getPointInfoForUser(user),
    userRecordCount$(user)
]).pipe(
    map(([info, recordCount]) => ({
        _id: user._id,
        email: user.email,
        goalPoint: info.goalPoint,
        testGoalPoint: info.testGoalPoint,
        recordCount
    }))
);

const userRecordCount$ = (user) => recordService.getRecordCountForUser(user);

const hashPassword$ = (password) => userService.hashPassword(password);

const insertUser$ = (email, hashedPassword, salt, phone) => userService.insert({ email, hashedPassword, salt, phone });

const addTestGoalPoint$ = (user) => goalPointService.addDailyTestGoalPointEntryForUser(user, new Date(Date.now()));

const addRegisteredClaim$ = (user) => claimService.addRegisteredClaimForUser(user).pipe(
    switchMap(() => claimService.getClaimsForUser(user))
);

const checkPassword$ = (user, oldPassword) => userService.checkPassword(user, oldPassword);

const changePassword$ = (user, newPassword) => userService.hashPassword(newPassword).pipe(
    switchMap(hashResult => {
        user.hashedPassword = hashResult.hash;
        user.salt = hashResult.salt;
        return userService.update(user);
    }),
    map(() => responses.json({ success: true })),
    catchError(() => of(responses.jsonError500()))
);

const claimIdsByNames$ = (claimNames) => claimService.getClaimIdsByNames(claimNames);

const processInsertUser$ = (email, password, phone) => hashPassword$(password).pipe(
    switchMap(hashResult => insertUser$(email, hashResult.hash, hashResult.salt, phone)),
    switchMap(user => forkJoin([
        addTestGoalPoint$(user),
        addRegisteredClaim$(user)
    ]), (user, [testPointEntry, claims]) => _successResult(user, 0, testPointEntry.value, false, claims)),
    catchError(() => {
        return of(responses.jsonErrorMessage400('User info is invalid'));
    }));

const processUpdateUser$ = (user, email, phone, claims) => userByEmail$(email).pipe(
    switchMap(existUser => iif(() => existUser != null && existUser._id.toString() != user._id.toString(),
        of(responses.jsonError400('Email is existing')),
        defer(() => claimIdsByNames$(claims).pipe(
            switchMap(claimIds => userService.update(Object.assign(user, { email, phone, claims: claimIds }))),
            switchMap(() => userService.findUserById(user._id)),
            map(updatedUser => responses.json({ success: true, data: _userDetails(updatedUser) })),
            catchError(() => of(responses.jsonError500()))
        ))
    ))
);

const processChangePassword$ = (user, oldPassword, newPassword) => checkPassword$(user, oldPassword).pipe(
    switchMap(valid => iif(() => valid,
        defer(() => changePassword$(user, newPassword)),
        of(responses.jsonError400('Old password is incorrect'))
    ))
);

const _userDetails = (user) => ({
    id: user._id,
    email: user.email,
    phone: user.phone,
    claims: (user.claims || []).map(claim => claim.name)
});

const _failedResult = () => responses.jsonErrorMessage400('Email or password is incorrect');

const _successResult = (user, goalPoint, testGoalPoint, remember, claims) => {
    const userData = {
        email: user.email,
        goalPoint,
        testGoalPoint,
        token: tokenService.getToken({
            id: user._id
        }, { expire: remember ? config.USER_REMEMBER_EXPIRE : config.USER_EXPIRE }),
        claims: (claims || []).map(claim => claim.name)
    };

    return responses.json({
        success: true,
        user: userData
    });
};

module.exports = {
    login,
    register,
    userInfo,
    getUsers,
    getUserDetails,
    deleteUser,
    exists,
    updateUser,
    changePassword,
    adminChangePassword
}