const express = require('express');
const router = express.Router();
const controller = require('./user.controller');
const renderer = require('../renderer');
const modelValidation = require('../../middlewares/model-validation');
const validationError = require('../../middlewares/validation-error.middleware');
const { loginValidator, registerValidator, updateValidator, changePasswordValidator, adminChangePasswordValidator } = require('../../validators/user');
const { bodyFieldRequired, isEmail, requiredParamId } = require('../../validators/common');
const authorize = require('../../middlewares/authorize.middleware');
const { ADMINISTRATOR } = require('../../models/Claim');

router.post('/login',
    modelValidation(loginValidator),
    validationError({ type: 'json' }),
    (req, res) => controller.login(req.body, req.modelErrors)
        .subscribe(renderer(req, res)));

router.post('/register',
    modelValidation(registerValidator),
    validationError({ type: 'json' }),
    (req, res) => controller.register(req.body, req.modelErrors)
        .subscribe(renderer(req, res)));

router.get('/info',
    authorize(null, { type: 'json' }),
    (req, res) => controller.userInfo(req)
        .subscribe(renderer(req, res)));

router.get('/change-password',
    authorize(null, { type: 'json' }),
    modelValidation(changePasswordValidator),
    validationError({ type: 'json' }),
    (req, res) => controller.changePassword(req)
        .subscribe(renderer(req, res)));

router.post('/exists',
    modelValidation([
        isEmail(
            bodyFieldRequired('email')
        ).withMessage('Email is invalid format')
    ]),
    validationError({ type: 'json' }),
    (req, res) => controller.exists(req.body.email)
        .subscribe(renderer(req, res)));

router.route('/:userId')
    .get(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation([requiredParamId('userId')]),
        validationError({ type: 'json' }),
        (req, res) => controller.getUserDetails(req.params.userId)
            .subscribe(renderer(req, res))
    )
    .delete(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation([requiredParamId('userId')]),
        validationError({ type: 'json' }),
        (req, res) => controller.deleteUser(req.params.userId)
            .subscribe(renderer(req, res))
    );

router.post('/:userId/change-password',
    authorize([ADMINISTRATOR], { type: 'json' }),
    modelValidation([requiredParamId('userId')]),
    modelValidation(adminChangePasswordValidator),
    validationError({ type: 'json' }),
    (req, res) => controller.adminChangePassword(req.params.userId, req.body)
        .subscribe(renderer(req, res))
)

router.route('/')
    .get(
        authorize([ADMINISTRATOR], { type: 'json' }),
        (req, res) => controller.getUsers(req.query)
            .subscribe(renderer(req, res)))
    .put(
        authorize([ADMINISTRATOR], { type: 'json' }),
        modelValidation(updateValidator),
        validationError({ type: 'json' }),
        (req, res) => controller.updateUser(req.body)
            .subscribe(renderer(req, res)));

module.exports = router;