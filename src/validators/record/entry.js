const { check } = require('express-validator');
const { NORTH_AREA, SOUTH_AREA } = require('../../models/constants/areas');
const { NUMBER_PATTERN, DATE_PATTERN } = require('../../config');
const { GOAL_POINT, TEST_GOAL_POINT } = require('../../models/constants/goal-points');

const recordEntryValidator = [
    check('area')
        .trim()
        .notEmpty().withMessage('Area is required')
        .isIn([NORTH_AREA, SOUTH_AREA]).withMessage(`Area must be "${NORTH_AREA}" or "${SOUTH_AREA}"`),
    check('pointType')
        .trim()
        .notEmpty().withMessage('Point type is required')
        .isIn([GOAL_POINT, TEST_GOAL_POINT]).withMessage(`Point type must be "${GOAL_POINT}" or "${TEST_GOAL_POINT}"`),
    check('channel')
        .trim()
        .escape(),
    check('type')
        .trim()
        .notEmpty().withMessage('Type is required'),
    check('point')
        .trim()
        .notEmpty().withMessage('Point is required')
        .isInt().withMessage('Point must be a number')
        .custom(value => {
            if (value < 0)
                throw new Error('Point must greater than 0')
            return true;
        })
        .toInt(),
    check('number')
        .trim()
        .notEmpty().withMessage('Number is required')
        .matches(NUMBER_PATTERN).withMessage('Number is invalid format'),
    check('date')
        .trim()
        .notEmpty().withMessage('Date is required')
        .matches(DATE_PATTERN).withMessage('Date must be "YYYY-MM-DD" format')
];

module.exports = recordEntryValidator;