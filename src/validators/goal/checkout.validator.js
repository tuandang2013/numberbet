const { check } = require('express-validator');

const checkoutValidator = [
    check('userId')
        .notEmpty().withMessage('Id is required')
        .isMongoId().withMessage('Id is invalid'),
    check('amount')
        .notEmpty().withMessage('Amound is required')
        .isInt().withMessage('Amount must be number')
        .toInt()
        .custom(value => {
            if (value <= 0) {
                throw new Error('Amount must greater than 0');
            }
            return true;
        })
];

module.exports = checkoutValidator;