const config = require('../../config');

function isId(rule){
    return rule.matches(config.ID_PATTERN);
}

module.exports = isId;