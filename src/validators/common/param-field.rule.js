const { param } = require('express-validator');

function paramFieldRequired(name) {
    return param(name).notEmpty().withMessage(`${name} is required`);
}

module.exports = paramFieldRequired;