const paramFieldRequired = require('./param-field.rule');
const bodyFieldRequired = require('./body-field.rule');
const isEmail = require('./isEmail.decorator');
const isPhone = require('./isPhone.decorator');
const isId = require('./isId.decorator');
const requiredParamId = require('./required-param-id');

module.exports = {
    paramFieldRequired,
    bodyFieldRequired,
    isEmail,
    isPhone,
    isId,
    requiredParamId
};