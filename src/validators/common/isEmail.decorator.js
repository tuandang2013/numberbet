const config = require('../../config');

function isEmail(rule){
    return rule.matches(config.EMAIL_PATTERN);
}

module.exports = isEmail;
