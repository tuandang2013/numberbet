const {body} = require('express-validator');

function bodyFieldRequired(name){
    return body(name).notEmpty().withMessage(`${name} is required`);
}

module.exports = bodyFieldRequired;