const isId = require('./isId.decorator');
const paramFieldRequired = require('./param-field.rule');

function requiredParamId(name){
    return isId(
        paramFieldRequired(name)
    ).withMessage('Id is invalid format');
}

module.exports = requiredParamId;