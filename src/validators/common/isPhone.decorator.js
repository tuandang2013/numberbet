const config = require('../../config');

function isPhone(rule) {
    return rule.matches(config.PHONE_PATTERN);
}

module.exports = isPhone;