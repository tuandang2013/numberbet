const { check } = require('express-validator');
const { isEmail, isPhone, isId } = require('../common');

const updateValidator = [
    isId(
        check('id')
            .notEmpty().withMessage('Id is required')
    ).withMessage('Id is invalid format'),
    isEmail(
        check('email')
            .notEmpty().withMessage('Email is required')
            //.isEmail().withMessage('Email is invalid')
            .isLength({ max: 200 }).withMessage('Email max length is 200')
    ).withMessage('Email is invalid format')
        .normalizeEmail()
        .trim(),
    isPhone(check('phone')
        .notEmpty().withMessage('Phone is required')
    ).withMessage('Phone is invalid format').trim()
];

module.exports = updateValidator;