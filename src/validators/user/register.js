const { check } = require('express-validator');
const { isEmail, isPhone } = require('../common');

const registerValidator = [
    isEmail(
        check('email')
            .notEmpty().withMessage('Email is required')
            //.isEmail().withMessage('Email is invalid')
            .isLength({ max: 200 }).withMessage('Email max length is 200')
    ).withMessage('Email is invalid format')
        .normalizeEmail()
        .trim(),
    check('password')
        .notEmpty().withMessage('Password is required')
        .isLength({ min: 6 }).withMessage('Password min length is 6')
        .isLength({ max: 200 }).withMessage('Password max length is 200'),
    check('confirmPassword')
        .notEmpty().withMessage('Confirm password is required')
        .custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Password confirmation does not match password');
            }
            return true;
        }),
    isPhone(check('phone')
        .notEmpty().withMessage('Phone is required')
    ).withMessage('Phone is invalid format').trim()
];

module.exports = registerValidator;