const loginValidator = require('./login');
const registerValidator = require('./register');
const updateValidator = require('./update.validator');
const changePasswordValidator = require('./change-password.validator');
const adminChangePasswordValidator = require('./admin-change-password.validator');

module.exports = {
    loginValidator,
    registerValidator,
    updateValidator,
    changePasswordValidator,
    adminChangePasswordValidator
};