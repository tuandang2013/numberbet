const { check } = require('express-validator');
const { isEmail } = require('../common');

const loginValidator = [
    isEmail(
        check('email').notEmpty().withMessage('Email is required')
    ).withMessage('Email is invalid format'),
    check('password').notEmpty().withMessage('Password is required')
];

module.exports = loginValidator;