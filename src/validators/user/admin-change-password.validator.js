const { check } = require('express-validator');

const adminChangePasswordValidator = [
    check('password')
        .notEmpty().withMessage('Password is required')
        .isLength({ min: 6 }).withMessage('Password min length is 6')
        .isLength({ max: 200 }).withMessage('Password max length is 200'),
    check('confirmPassword')
        .notEmpty().withMessage('Confirm password is required')
        .custom((value, { req }) => {
            if (value !== req.body.password) {
                throw new Error('Password confirmation does not match password');
            }
            return true;
        })
];;

module.exports = adminChangePasswordValidator;