const mongoose = require('mongoose');
const southNumberSchema = require('./schemas').southNumber;

//model
const SouthNumber = mongoose.model('SouthNumbers', southNumberSchema);

function insert(item) {
    if (item == null) throw new Error('Item must be not null');
    return SouthNumber.create(item).then(docs => docs[0]);
}

function has(date) {
    if (date == null)
        throw new Error('Date must be not a null');
    if (typeof date !== 'string')
        throw new Error('Date must be a string');

    return SouthNumber.exists({ date });
}

function getById(id) {
    return new Promise((resolve, reject) => {
        SouthNumber
            .findById(id)
            .exec((err, doc) => {
                if (err) return reject(err);
                resolve(doc);
            });
    });
}

function getByDate(date) {
    if (date == null)
        throw new Error('Date must be specified');
    if (typeof date !== 'string')
        throw new Error('Date must be string');

    return new Promise((resolve, reject) => {
        SouthNumber
            .findOne({ date })
            .exec((err, doc) => {
                if (err) return reject(err);
                resolve(doc);
            });
    });
}

module.exports = {
    _Model: SouthNumber,
    insert,
    getByDate,
    getById,
    has
};
