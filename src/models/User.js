const mongoose = require('mongoose');
const userSchema = require('./schemas').user;

//model
const Model = mongoose.model('Users', userSchema);

function insert(user) {
    if (user == null) throw new Error('User must be not null');
    return Model.create(user);
}

function update(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof Model))
        throw new Error('User is invalid');

    return user.save();
}

function remove(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof Model))
        throw new Error('User is invalid');

    return user.remove();
}

function findById(id) {
    if (id == null)
        throw new Error('Id must be not null');

    return new Promise((resolve, reject) => {
        Model.findById(id).populate('claims').exec((err, user) => {
            if (err) return reject(err);
            resolve(user);
        })
    });
}

function getAll() {
    return Model.find({}).exec();
}

function getUsers({ pageNumber, pageSize }) {
    return Model.find({})
        .sort({ 'createdDate': -1, 'email': 1 })
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize)
        .populate('claims')
        .exec();
}

function findByEmail(email) {
    if (email == null)
        throw new Error('Email must be not null');

    return new Promise((resolve, reject) => {
        Model.findOne({ email: email }).populate('claims').exec((err, user) => {
            if (err) return reject(err);
            resolve(user);
        })
    });
}

function count() {
    return Model.estimatedDocumentCount().exec();
}

module.exports = {
    _Model: Model,
    getAll,
    getUsers,
    findByEmail,
    findById,
    insert,
    update,
    count,
    remove
};