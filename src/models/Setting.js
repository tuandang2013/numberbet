const fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose');
const settingSchema = require('./schemas').setting,
    logger = require('../services/logger'),
    config = require('../config');

//file
const _targetFile = path.join(config.BASE_PATH, 'settings.json');
//model
const Setting = mongoose.model('Settings', settingSchema);

module.exports = {
    clear,
    init,
    getAll
};

function getAll() {
    return new Promise((resolve, reject) => {
        Setting.find((err, settings) => {
            if (err) return reject(err);
            resolve(settings);
        });
    });
}

function clear() {
    return new Promise((resolve, reject) => {
        Setting.deleteMany(true, (err) => {
            if (err)
                return reject(err);
            resolve(true);
        })
    });
}

function isEmpty() {
    return new Promise((resolve, reject) => {
        Setting.find((err, settings) => {
            if (err)
                return reject(err);
            resolve(settings.length == 0);
        })
    });
}

function _readFromFile() {
    return new Promise((resolve, reject) => {
        fs.readFile(_targetFile, { encoding: 'utf-8' }, (err, data) => {
            if (err)
                return reject(err);
            const settingData = JSON.parse(data);
            resolve(settingData);
        });
    });
}

function init(force = false) {
    logger.debug('Setting init [process] [args]', { force });

    return new Promise((resolve, reject) => {
        const firstTask = force ? clear() : isEmpty();
        firstTask.then((start) => {
            logger.debug('Setting init [process] [firstTaskDone]', { start });

            if (start) {
                _readFromFile().then((settingData) => {
                    logger.debug('Setting init [process] [readFromFile]', settingData);

                    const settings = [];
                    for (const prop in settingData) {
                        settings.push(new Setting({ name: prop, value: settingData[prop] }));
                    }

                    logger.debug('Setting init [process] [settings]', settings);

                    Setting.insertMany(settings, (err, docs) => {
                        if (err) {
                            logger.error('Setting init [error]', err);

                            return reject(err);
                        }

                        logger.debug('Setting init [process] [insertedSettings]', docs);
                        resolve(docs);
                    })
                })
            } else {
                resolve();
            }
        });
    });
}