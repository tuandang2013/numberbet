const mongoose = require('mongoose');
const moment = require('moment');
const goalPointSchema = require('./schemas/goal-point');
const User = require('./User');

const Model = mongoose.model('GoalPoints', goalPointSchema);

function createForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.goalPointId != null)
        throw new Error('User already has goal point');

    return Model.create({})
        .then(goalPoint => {
            user.goalPointId = goalPoint._id;
            return goalPoint;
        });
}

function getForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.goalPointId == null)
        return Promise.resolve(null);
    return new Promise((resolve, reject) => {
        Model.findById(user.goalPointId).exec((err, goalPoint) => {
            if (err) return reject(err);
            resolve(goalPoint);
        });
    });
}

function addEntryForUser(user, entry) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (entry == null)
        throw new Error('Entry must be not null');

    return (user.goalPointId != null ? Model.findById(user.goalPointId).exec() : Model.create({}).then(goalPoint => {
        user.goalPointId = goalPoint._id;
        return user.save({ validateBeforeSave: false }).then(() => goalPoint);
    })).then(goalPoint => {
        goalPoint.entries.push(entry);
        return goalPoint.save().then(() => entry);
    });
}

function deleteEntryForUser(user, id) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (id == null)
        throw new Error('Id must be not null');
    if (user.goalPointId == null)
        throw new Error('User don\'t have goal point');

    return Model.findById(user.goalPointId).exec().then(goalPoint => {
        const index = goalPoint.entries.findIndex(entry => entry._id == id);
        if (~index) {
            goalPoint.entries.splice(index, 1);
            return goalPoint.save();
        }
        return Promise.reject('Entry is not found');
    });
}

function findEntryByDateForUser(user, date) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (date == null)
        throw new Error('Date must be not null');
    if (!(date instanceof Date))
        throw new Error('Date is invalid');
    if (user.goalPointId == null)
        return Promise.resolve([]);

    return Model.findById(user.goalPointId).exec().then(goalPoint =>
        goalPoint == null ? [] : goalPoint.entries.filter(entry =>
            moment(entry.createdDate).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD'))
    );
}

function getEntriesForUser(user) {
    return getForUser(user)
        .then(goalPoint => goalPoint == null ? [] : goalPoint.entries);
}

function removeForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return getForUser(user).then(goalPoint => {
        if (goalPoint != null) {
            return goalPoint.remove().then(() => {
                user.goalPointId = null;
                return user.save();
            });
        }
        return Promise.resolve(null);
    });
}

function remove(goalPoint) {
    if (goalPoint == null)
        throw new Error('Goal point must be not null');
    if (!(goalPoint instanceof Model))
        throw new Error('Goal point is invalid');

    return goalPoint.remove();
}

function findById(id){
    if (id == null)
        throw new Error('Id must be not null');

    return Model.findById(id).exec();
}

module.exports = {
    _Model: Model,
    createForUser,
    getForUser,
    addEntryForUser,
    deleteEntryForUser,
    findEntryByDateForUser,
    getEntriesForUser,
    removeForUser,
    remove,
    findById
};