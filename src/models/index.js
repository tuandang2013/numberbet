
module.exports = {
    Setting: require('../models/Setting'),
    NumberSource: require('../models/NumberSource'),
    NorthNumber: require('../models/NorthNumber'),
    SouthNumber: require('../models/SouthNumber'),
    User: require('../models/User')
};