const mongoose = require('mongoose');
const numberSourceSchema = require('./schemas').numberSource;

const NumberSource = mongoose.model('NumberSources', numberSourceSchema);

module.exports = NumberSource;