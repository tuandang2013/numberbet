const mongoose = require('mongoose');
const sourceSchema = require('./schemas').source;

//model
const Source = mongoose.model('Source', sourceSchema);

function insert(item) {
    if (item == null) throw new Error('Item must be not null');
    return Source.create(item).then(docs => docs[0]);
}

function has(query) {
    if (query == null)
        throw new Error('Query must be not a null');

    return Source.exists(query);
}

function getByDate(date) {
    if (date == null)
        throw new Error('Date must be specified');
    if (typeof date !== 'string')
        throw new Error('Date must be string');

    return new Promise((resolve, reject) => {
        Source
            .findOne({ date })
            .exec((err, doc) => {
                if (err) return reject(err);
                resolve(doc);
            });
    });
}

module.exports = {
    _Model: Source,
    insert, getByDate,
    has
};
