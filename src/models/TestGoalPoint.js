const mongoose = require('mongoose');
const moment = require('moment');
const goalPointSchema = require('./schemas/goal-point');
const User = require('./User');

const Model = mongoose.model('TestGoalPoints', goalPointSchema);

function createForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.testGoalPointId != null)
        throw new Error('User already has test goal point');

    return Model.create({})
        .then(goalPoint => {
            user.testGoalPointId = goalPoint._id;
            return goalPoint;
        });
}

function getForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.testGoalPointId == null)
        return Promise.resolve(null);
    return new Promise((resolve, reject) => {
        Model.findById(user.testGoalPointId).exec((err, goalPoint) => {
            if (err) return reject(err);
            resolve(goalPoint);
        });
    });
}

function addEntryForUser(user, entry) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (entry == null)
        throw new Error('Entry must be not null');

    return (user.testGoalPointId != null ? Model.findById(user.testGoalPointId).exec() : Model.create({}).then(testGoalPoint => {
        user.testGoalPointId = testGoalPoint._id;
        return user.save({ validateBeforeSave: false }).then(() => testGoalPoint);
    })).then(testGoalPoint => {
        testGoalPoint.entries.push(entry);
        return testGoalPoint.save().then(() => entry);
    });
}

function deleteEntryForUser(user, id) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (id == null)
        throw new Error('Id must be not null');
    if (user.testGoalPointId == null)
        throw new Error('User don\'t have test goal point');

    return Model.findById(user.testGoalPointId).exec().then(testGoalPoint => {
        const index = testGoalPoint.entries.findIndex(entry => entry._id == id);
        if (~index) {
            testGoalPoint.entries.splice(index, 1);
            return testGoalPoint.save();
        }
        return Promise.reject('Entry is not found');
    });
}

function findEntryByDateForUser(user, date) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (date == null)
        throw new Error('Date must be not null');
    if (!(date instanceof Date))
        throw new Error('Date is invalid');
    if (user.testGoalPointId == null)
        return Promise.resolve([]);

    return Model.findById(user.testGoalPointId).exec().then(testGoalPoint =>
        testGoalPoint == null ? [] :
            testGoalPoint.entries.filter(entry =>
                moment(entry.createdDate).format('YYYY-MM-DD') === moment(date).format('YYYY-MM-DD')));
}

function getEntriesForUser(user) {
    return getForUser(user).then(testGoalPoint => testGoalPoint == null ? [] : testGoalPoint.entries);
}


function removeForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return getForUser(user).then(testGoalPoint => {
        if (testGoalPoint != null) {
            return testGoalPoint.remove().then(() => {
                user.testGoalPointId= null;
                return user.save();
            });
        }
        return Promise.resolve(null);
    });
}

function remove(testGoalPoint) {
    if (testGoalPoint == null)
        throw new Error('Test goal point must be not null');
    if (!(testGoalPoint instanceof Model))
        throw new Error('Test goal point is invalid');

    return testGoalPoint.remove();
}


function findById(id){
    if (id == null)
        throw new Error('Id must be not null');

    return Model.findById(id).exec();
}

module.exports = {
    _Model: Model,
    createForUser,
    getForUser,
    addEntryForUser,
    deleteEntryForUser,
    findEntryByDateForUser,
    getEntriesForUser,
    removeForUser,
    remove,
    findById
};