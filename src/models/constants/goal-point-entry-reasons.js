const SUCCESS_GOAL_POINT_REASON = '@app/goal-point/success';
const SUCCESS_TEST_GOAL_POINT_REASON = '@app/goal-point/test/success';

const ADD_RECORD_ENTRY_GOAL_POINT_REASON = '@app/goal-point/record/add';
const DELETE_RECORD_ENTRY_GOAL_POINT_REASON = '@app/goal-point/record/delete';
const UPDATE_RECORD_ENTRY_GOAL_POINT_REASON = '@app/goal-point/record/update';

const ADD_RECORD_ENTRY_TEST_GOAL_POINT_REASON = '@app/goal-point/test/record/add';
const DELETE_RECORD_ENTRY_TEST_GOAL_POINT_REASON = '@app/goal-point/test/record/delete';
const UPDATE_RECORD_ENTRY_TEST_GOAL_POINT_REASON = '@app/goal-point/test/record/update';

const AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON = '@app/goal-point/test/auto-daily-increment';

const CHECKOUT_REASON = '@app/goal-point/checkout';

module.exports = {
    AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON,

    SUCCESS_GOAL_POINT_REASON,
    SUCCESS_TEST_GOAL_POINT_REASON,

    ADD_RECORD_ENTRY_GOAL_POINT_REASON,
    DELETE_RECORD_ENTRY_GOAL_POINT_REASON,
    UPDATE_RECORD_ENTRY_GOAL_POINT_REASON,

    ADD_RECORD_ENTRY_TEST_GOAL_POINT_REASON,
    DELETE_RECORD_ENTRY_TEST_GOAL_POINT_REASON,
    UPDATE_RECORD_ENTRY_TEST_GOAL_POINT_REASON,

    CHECKOUT_REASON
};