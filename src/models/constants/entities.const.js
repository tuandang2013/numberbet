const USER = 'user';
const RECORD = 'record';
const RECORD_ENTRY = 'record-entry';
const RECORD_HISTORY = 'record-history';
const SOURCE = 'source';
const NORTH_NUMBER = 'north-number';
const SOUTH_NUMBER = 'south-number';

module.exports = {
    USER,
    RECORD, RECORD_ENTRY, RECORD_HISTORY,
    SOURCE, NORTH_NUMBER, SOUTH_NUMBER
};