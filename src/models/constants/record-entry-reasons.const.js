const INVALID_DATA_REASON = '@app/record-entry/process/invalid-data';
const SOURCE_ERROR = '@app/record-entry/process/source-error';
const PROCESSOR_NOT_FOUND_REASON = '@app/record-entry/process/processor-not-found';
const NUMBER_CANNOT_PROCESS_REASON = '@app/record-entry/process/number-cannot-process';
const PROCESS_FAILED = '@app/record-entry/process/failed';

module.exports = {
    SOURCE_ERROR,
    INVALID_DATA_REASON,
    PROCESSOR_NOT_FOUND_REASON,
    NUMBER_CANNOT_PROCESS_REASON,
    PROCESS_FAILED
};