const NORTH_AREA = 'north';
const SOUTH_AREA = 'south';

module.exports = { NORTH_AREA, SOUTH_AREA };