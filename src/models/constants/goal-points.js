const GOAL_POINT = 'goal-point';
const TEST_GOAL_POINT = 'test-goal-point';

module.exports = {
    GOAL_POINT,
    TEST_GOAL_POINT
};