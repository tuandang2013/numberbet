const mongoose = require('mongoose');
const claimSchema = require('./schemas/claim');

const GUEST = 'Guest';
const REGISTERED = 'Registered';
const ADMINISTRATOR = 'Administrator';

const defaultClaims = [
    GUEST,
    REGISTERED,
    ADMINISTRATOR
];

const Model = mongoose.model('Claims', claimSchema);

function create(claim) {
    if (claim == null)
        throw new Error('Claim must be not null');

    return Model.create(claim);
}

function findByName(name) {
    if (name == null)
        throw new Error('Name must be not null');

    return Model.findOne({ name }).exec();
}

function findById(id) {
    if (id == null)
        throw new Error('Id must be not null');
    return Model.findById(id).exec();
}

function guestClaim() {
    return findByName(GUEST);
}

function registeredClaim() {
    return findByName(REGISTERED);
}

function administratorClaim() {
    return findByName(ADMINISTRATOR);
}

function getAll(){
    return Model.find().exec();
}

module.exports = {
    GUEST,
    REGISTERED,
    ADMINISTRATOR,
    get _Model() {
        return Model;
    },
    get defaultClaims() {
        return defaultClaims;
    },
    create,
    findByName,
    findById,
    guestClaim,
    registeredClaim,
    administratorClaim,
    getAll
};