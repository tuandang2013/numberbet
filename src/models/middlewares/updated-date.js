function middleware() {
    this.updatedDate = new Date();
}

function updatedDate(schema) {
    schema.pre('save', middleware);

    return schema;
}

module.exports = updatedDate;