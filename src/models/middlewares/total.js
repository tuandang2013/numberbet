function middleware() {
    this.total = this.entries.reduce((total, entry) => total + entry.value, 0);
}

function total(schema) {
    schema.pre('save', middleware);

    return schema;
}

module.exports = total;