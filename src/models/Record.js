const mongoose = require('mongoose');
const recordSchema = require('./schemas/record');
const User = require('./User');

const Model = mongoose.model('Records', recordSchema);

function createForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.recordId != null)
        throw new Error('User already has record');

    return Model.create({})
        .then(record => {
            user.recordId = record._id;
            return record;
        });
}

function getForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.recordId == null)
        return Promise.resolve(null);
    return new Promise((resolve, reject) => {
        Model.findById(user.recordId).exec((err, record) => {
            if (err) return reject(err);
            resolve(record);
        });
    });
}

function getEntriesForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (user.recordId == null)
        return Promise.resolve([]);
    return Model.findById(user.recordId).exec().then(record => record == null ? [] : record.entries);
}

function getRecordHistoryForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (user.recordId == null)
        return Promise.resolve([]);

    return Model.findById(user.recordId).exec().then(record => record == null ? [] : record.histories);
}

function addEntryForUser(user, entry) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (entry == null)
        throw new Error('Entry must be not null');

    return (user.recordId ? Model.findById(user.recordId).exec() : Promise.resolve(null))
        .then(record => {
            if (record != null)
                return record;
            return Model.create({}).then(createdRecord => {
                user.recordId = createdRecord._id;
                return user.save({ validateBeforeSave: false })
                    .then(() => createdRecord);
            });
        })
        .then(record => {
            record.entries.push(entry);
            return record.save().then(() => entry);
        });
}

function deleteEntryForUser(user, id) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (user.recordId == null)
        throw new Error('User don\'t have record');
    if (id == null)
        throw new Error('Id must be not null');

    return Model.findById(user.recordId).exec()
        .then(record => {
            if (record == null) {
                return Promise.reject('Record is not found');
            }
            const index = record.entries.findIndex(entry => entry._id == id);
            if (~index) {
                record.entries.splice(index, 1);
                return record.save();
            };
            return Promise.reject('Entry is not found');
        });
}

function alterEntryForUser(user, id, entry) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (user.recordId == null)
        throw new Error('User don\'t have record');
    if (id == null)
        throw new Error('Id must be not null');
    if (entry == null)
        throw new Error('Entry must be not null');

    return Model.findById(user.recordId).exec()
        .then(record => {
            if (record == null) {
                return Promise.reject('Record is not found');
            }
            const index = record.entries.findIndex(entry => entry._id.equals(id));
            if (~index) {
                const alteredItem = Object.assign(record.entries[index], entry);
                return record.save().then(() => alteredItem);
            }
            return Promise.reject('Entry is not found');
        })
}

function addHistoryForUser() {

}

function removeForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return getForUser(user).then(record => {
        if (record != null)
            return record.remove().then(() => {
                user.recordId = null;
                return user.save();
            });
        return Promise.resolve(null);
    });
}

function remove(record) {
    if (record == null)
        throw new Error('Record must be not null');
    if (!(record instanceof Model))
        throw new Error('Record is invalid');

    return record.remove();
}

function findById(id) {
    if (id == null)
        throw new Error('Id must be not null');

    return Model.findById(id).exec();
}

module.exports = {
    _Model: Model,
    createForUser,
    getForUser,
    addEntryForUser,
    deleteEntryForUser,
    alterEntryForUser,
    getEntriesForUser,
    addHistoryForUser,
    getRecordHistoryForUser,
    removeForUser,
    remove,
    findById
};