const mongoose = require('mongoose');
const northNumberSchema = require('./schemas').northNumber;

//model
const NorthNumber = mongoose.model('NorthNumbers', northNumberSchema);

function insert(item) {
    if (item == null) throw new Error('Item must be not null');
    return NorthNumber.create(item).then(docs => docs[0]);
}

function has(date) {
    if (date == null)
        throw new Error('Date must be not a null');
    if (typeof date !== 'string')
        throw new Error('Date must be a string');
    return NorthNumber.exists({ date });
}

function getByDate(date) {
    if (date == null)
        throw new Error('Date must be specified');
    if (typeof date !== 'string')
        throw new Error('Date must be string');

    return new Promise((resolve, reject) => {
        NorthNumber
            .findOne({ date })
            .exec((err, doc) => {
                if (err) return reject(err);
                resolve(doc);
            });
    });
}

function getById(id) {
    return new Promise((resolve, reject) => {
        NorthNumber
            .findById(id)
            .exec((err, doc) => {
                if (err) return reject(err);
                resolve(doc);
            });
    });
}

module.exports = {
    _Model: NorthNumber,
    insert,
    getByDate,
    getById,
    has
};
