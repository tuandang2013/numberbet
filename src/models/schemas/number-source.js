const mongoose = require('mongoose');

const numberSourceSchema = new mongoose.Schema({
    url: String,
    data: String,
    date: Date
});

module.exports = numberSourceSchema;