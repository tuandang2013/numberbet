const mongoose = require('mongoose');

const sourceSchema = new mongoose.Schema({
    name: String,
    url: String,
    date: String,
    north: {
        date: String,
        id: String
    },
    south: {
        date: String,
        id: String
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = sourceSchema;