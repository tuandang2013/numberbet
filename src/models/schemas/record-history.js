const mongoose = require('mongoose');
const recordEntrySchema = require('../schemas/record-entry');

const recordHistorySchema = new mongoose.Schema({
    entry: {
        type: recordEntrySchema,
        required: [true, 'Entry is required']
    },
    success: {
        type: Boolean,
        required: [true, 'Success is required']
    },
    failedReason: {
        type: String
    },
    source: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = recordHistorySchema;