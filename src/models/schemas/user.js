const mongoose = require('mongoose');
const config = require('../../config');
const claimSchema = require('./claim');

const emailPattern = config.EMAIL_PATTERN;
const phonePattern = config.PHONE_PATTERN;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, 'Email is required'],
        unique: true,
        validate: {
            validator: value => emailPattern.test(value),
            message: 'Email is invalid format'
        },
        maxlength: [200, 'Email max length is 200']
    },
    claims: [{
        type: String,
        ref: 'Claims'
    }],
    phone: {
        type: String,
        required: [true, 'Phone is required'],
        validate: {
            validator: value => phonePattern.test(value),
            message: 'Phone is invalid format'
        }
    },
    hashedPassword: {
        type: String,
        required: [true, 'Hashed password is required']
    },
    salt: {
        type: String,
        required: [true, 'Salt is required']
    },
    goalPointId: {
        type: String,
    },
    testGoalPointId: {
        type: String,
    },
    recordId: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = userSchema;