const mongoose = require('mongoose');

const claimSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
        maxlength: [200, 'Name max length is 200']
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = claimSchema;