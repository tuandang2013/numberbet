const mongoose = require('mongoose');

const entitySchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required']
    },
    target: {
        type: String,
        required: [true, 'Target is required']
    }
});

module.exports = entitySchema;