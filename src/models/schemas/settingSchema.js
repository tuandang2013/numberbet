const mongoose = require('mongoose');

//schema
const settingSchema = new mongoose.Schema({
    name: String,
    value: String
});

module.exports = settingSchema;