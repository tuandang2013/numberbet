const mongoose = require('mongoose');
const channelSchema = require('./channel');

const northSchema = new mongoose.Schema({
    code: String,
    url: String,
    dailyUrl: String,
    name: String,
    date: String, //YYYY-MM-DD
    day: String,
    channels: [channelSchema],
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = northSchema;