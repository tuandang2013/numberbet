const mongoose = require('mongoose');
const recordEntrySchema = require('./record-entry');
const recordHistorySchema = require('./record-history');
const { updatedDate } = require('../middlewares');

const recordSchema = new mongoose.Schema({
    entries: [recordEntrySchema],
    histories: [recordHistorySchema],
    updatedDate: {
        type: Date
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = updatedDate(recordSchema);