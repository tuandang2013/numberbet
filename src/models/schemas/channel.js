const mongoose = require('mongoose');

const channelSchema = new mongoose.Schema({
    name: String,
    url: String,
    goals: [{
        code: String,
        name: String,
        numbers: Array
    }],
    allNumbers: Array,
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = channelSchema;