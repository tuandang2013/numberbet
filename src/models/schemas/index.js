module.exports = {
    setting: require('./settingSchema'),
    entity: require('./entity.schema'),
    northNumber: require('./north-number'),
    southNumber: require('./south-number'),
    channel: require('./channel'),
    numberSource: require('./number-source'),
    source: require('./source'),
    user: require('./user'),
    goalPoint: require('./goal-point'),
    goalPointEntry: require('./goal-point-entry'),
    recordEntry: require('./record-entry'),
    recordHistory: require('./record-history'),
    record: require('./record'),
    claim: require('./claim')
};