const mongoose = require('mongoose');
const entitySchema = require('../schemas/entity.schema');

const goalPointEntrySchema = new mongoose.Schema({
    value: {
        type: Number,
        default: 0,
        required: [true, 'Value is required']
    },
    reason: {
        type: String,
        required: [true, 'Reason is required']
    },
    entity: {
        type: entitySchema
    },
    description: {
        type: String
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = goalPointEntrySchema;