const mongoose = require('mongoose');

const { NORTH_AREA, SOUTH_AREA } = require('../constants/areas');
const { GOAL_POINT, TEST_GOAL_POINT } = require('../constants/goal-points');
const datePattern = /^\d{4}-\d{1,2}-\d{1,2}$/;
const numberPattern = /^\d+$/;

const recordEntrySchema = new mongoose.Schema({
    area: {
        type: String,
        enum: {
            values: [NORTH_AREA, SOUTH_AREA],
            message: `Area must be "${NORTH_AREA}" or "${SOUTH_AREA}"`,
            trim: true
        },
        required: [true, 'Area is required']
    },
    channel: {
        type: String,
        required: [true, 'Channel is required']
    },
    type: {
        type: String,
        required: [true, 'Type is required']
    },
    point: {
        type: Number,
        required: [true, 'Point is required']
    },
    pointType: {
        type: String,
        enum: {
            values: [GOAL_POINT, TEST_GOAL_POINT],
            message: `Point type must be "${GOAL_POINT}" or "${TEST_GOAL_POINT}"`,
            trim: true
        },
        required: [true, 'Point type is required']
    },
    number: {
        type: String,
        required: [true, 'Number is required'],
        validate: {
            validator: (value) => numberPattern.test(value),
            message: 'Number is invalid format'
        }
    },
    date: {
        type: String,
        required: [true, 'Date is required'],
        validate: {
            validator: (value) => datePattern.test(value),
            message: 'Date must be "YYYY-MM-DD" format'
        }
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

module.exports = recordEntrySchema;