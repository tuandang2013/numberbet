const mongoose = require('mongoose');
const goalPointEntrySchema = require('./goal-point-entry');
const { updatedDate, total } = require('../middlewares');

const goalPointSchema = new mongoose.Schema({
    entries: [goalPointEntrySchema],
    total: {
        type: Number,
        default: 0
    },
    createdDate: {
        type: Date,
        default: Date.now
    },
    updatedDate: {
        type: Date
    }
});

module.exports = total(updatedDate(goalPointSchema));