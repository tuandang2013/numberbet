function getPager({ pageNumber, pageSize, count }) {
    return {
        pageNumber,
        pageSize,
        totalItems: count,
        totalPages: Math.ceil(count / pageSize)
    };
}

module.exports = {
    getPager
};