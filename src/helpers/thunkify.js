function thunkify(fn) {
    return function () {
        const args = [].slice.call(arguments);

        return function (cb) {
            args.push(cb);
            return fn.apply(null, args);
        };
    };
}

module.exports = thunkify;