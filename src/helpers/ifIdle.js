const { Observable } = require('rxjs');
const { finalize } = require('rxjs/operators');

function ifIdle(fn) {
    let running = false;
    let result;
    return function () {
        if (!running) {
            running = true;

            result = fn.apply(null, arguments);
            
            if (result instanceof Observable) {
                result.pipe(
                    finalize(() => running = false)
                );
            }
            else if (result instanceof Promise) {
                result.then(r => {
                    running = false;
                    return r;
                })
            } else {
                running = false;
            }
        }
        return result;
    }
}

module.exports = ifIdle;