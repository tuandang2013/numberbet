function toNumber(data, defaultValue) {
    let number = Number(data);
    if (Number.isNaN(number) && defaultValue) {
        return defaultValue;
    }
    return number;
}

module.exports = { toNumber };