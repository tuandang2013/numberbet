function promisify(fn) {
    return function () {
        const args = [].slice.call(arguments);
        return new Promise((resolve, reject) => {
            args.push(function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
            fn.apply(null, args);
        });
    };
}

module.exports = promisify;