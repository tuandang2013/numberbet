module.exports = {
    once: require('./once'),
    ifIdle: require('./ifIdle'),
    K: require('./K'),
    Y: require('./Y'),
    I: require('./I')
}