const once = (fn) => {
    let run = false;
    return () => {
        if (!run) {
            run = true;
            return fn.apply(null, arguments);
        }
    }
}

module.exports = once;