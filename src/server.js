//imports
const path = require('path'),
    mongoose = require('mongoose');
const config = require('./config'),
    logger = require('./services/logger'),
    settingService = require('./services/setting.service'),
    claimService = require('./services/claim.service');

logger.debug('----------------------START-----------------------', { time: new Date() });

//cache
require('./services/cache').init();

//db
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
};

logger.info('[Server] Initial database connection...');
logger.debug('[Server] Initial database connection [process] [options]', options);

mongoose.connect(config.DB, options);
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Database connection error:'));
db.once('open', async () => {
    logger.info('[Server] Initial database connection [done]');

    //settings
    await settingService.init();

    //claims
    claimService.init().subscribe();

    //tasks
    const runner = require('./services/task-runner');
    const tasks = require('./tasks');
    runner.setup(config.TASK_RUNNER_INTERVAL, tasks);
    runner.run(tasks).subscribe(null, null, () => logger.info('[TaskRunner][run] Task runner run [done]'));
});

//app
const express = require('express'),
    helmet = require('helmet'),
    morgan = require('morgan'),
    routes = require('./routes'),
    session = require('./middlewares/session'),
    authenticate = require('./middlewares/authenticate'),
    autoIncrementTestPoint = require('./middlewares/auto-increment-test-point');

const app = express()
    //config
    .set('views', path.join(config.BASE_PATH, 'views'))
    .set('view engine', 'ejs')
    //middlewares
    .use(morgan('dev'))
    .use(helmet())
    .disable('x-powered-by')
    .use(express.static(path.join(config.ROOT_PATH, 'public')))
    .use(session())
    .use(authenticate())
    .use(autoIncrementTestPoint())
    //router
    .use(routes);

//start server
logger.info('[Server] Start server...');

app.listen(config.PORT, () => logger.info('[Server] Start server [done]', { port: config.PORT }));
