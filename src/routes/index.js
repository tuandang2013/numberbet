const express = require('express'),
    cors = require('cors'),
    pulicRoute = require('./public'),
    apiRoute = require('../api/api'),
    loadSetting = require('../middlewares/load-settings'),
    config = require('../config');
const router = express.Router();

const corsOptions = {
    credentials: true,
    origin: (origin, callback) => {
        if (config.SUPPORTED_ORIGINS.indexOf(origin) !== -1 || !origin) {
            callback(null, true);
        } else {
            callback(new Error('Not allowed by CORS'));
        }
    }
}

router.use('/api',
    cors(corsOptions),
    express.json(), express.urlencoded({ extended: true }),
    apiRoute);
router.use(loadSetting(), pulicRoute);

module.exports = router;