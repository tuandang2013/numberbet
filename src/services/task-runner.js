const { from, iif, defer, empty } = require('rxjs');
const { concatMap, tap } = require('rxjs/operators');
const logger = require('./logger');

function setup(time, tasks) {
    logger.info('[TaskRunner][setup] Task runner setup...', { time });

    const intervalId = setInterval(() => run(tasks).subscribe(), time);

    logger.info('[TaskRunner][setup] Task runner setup [done]');

    return intervalId;
}

function run(tasks) {
    logger.info('[TaskRunner][run] Task runner run...');
    logger.debug('[TaskRunner][run] Task runner run [process] [tasks]', tasks.map(task => task.info));

    return from(tasks).pipe(
        tap(task => logger.info('[TaskRunner][run] Task runner run [process] [task]', { name: task.info.name })),
        //finalize(() => logger.info('Task runner run [done]')),
        concatMap(task => iif(() =>
            task.info.lastRunTime == null ||
            (task.info.nextRunTime == null || task.info.nextRunTime < Date.now()),
            defer(() => task.task()),
            empty()
        ))
    );
}


module.exports = {
    setup,
    run
};