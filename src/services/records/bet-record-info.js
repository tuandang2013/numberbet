const { NORTH_AREA, SOUTH_AREA } = require('../../models/constants/areas');
const { GOAL_POINT, TEST_GOAL_POINT } = require('../../models/constants/goal-points');
const recordTypes = require('./types');

const recordTypesInfo = recordTypes.map(type => ({
    info: type.info,
    pattern: type.pattern.source,
    pointRate: type.config.pointRate,
    numberCount: type.config.numberCount
}));

const southBetRecordInfo = {
    name: 'Miền Nam',
    code: SOUTH_AREA,
    channels: [
        ['Đà Lạt', 'Kiên Giang', 'Tiền Giang'],
        ['Cà Mau', 'Đồng Tháp', 'TP.HCM'],
        ['Bạc Liêu', 'Bến Tre', 'Vũng Tàu'],
        ['Cần Thơ', 'Đồng Nai', 'Sóc Trăng'],
        ['An Giang', 'Bình Thuận', 'Tây Ninh'],
        ['Bình Dương', 'Trà Vinh', 'Vĩnh Long'],
        ['Bình Phước', 'TP.HCM', 'Hậu Giang', 'Long An']
    ]
};

const northBetRecordInfo = {
    name: 'Miền Bắc',
    code: NORTH_AREA,
    channels: [
        ['Thái Bình'],
        ['Hà Nội'],
        ['Quảng Ninh'],
        ['Bắc Ninh'],
        ['Hà Nội'],
        ['Hải Phòng'],
        ['Nam Định']
    ]
};

const betRecordInfo = {
    areas: [
        northBetRecordInfo,
        southBetRecordInfo
    ],
    types: recordTypesInfo,
    pointTypes: [GOAL_POINT, TEST_GOAL_POINT]
};

module.exports = {
    northBetRecordInfo,
    southBetRecordInfo,
    betRecordInfo
};