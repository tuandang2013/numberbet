module.exports = [{
    info: require('../types/two-number/info.json'),
    config: require('../types/two-number/config'),
    pattern: require('../types/two-number/config').pattern,
    processor: require('../types/two-number/processor')
}, {
    info: require('../types/three-number/info.json'),
    config: require('../types/three-number/config'),
    pattern: require('../types/three-number/config').pattern,
    processor: require('../types/three-number/processor')
}];