const config = require('./config');

function canProcess(number) {
    return config.pattern.test(number);
}

function process(number, source) {
    if (source == null)
        throw new Error('Source must be not null');
    if (!(source instanceof Array))
        throw new Error('Source must be array');
    if (!canProcess(number))
        throw new Error('Number cannot process');

    return source.some(item => String(item).endsWith(number));
}

module.exports = { canProcess, process };