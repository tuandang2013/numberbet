const { NORTH_AREA, SOUTH_AREA } = require('../../../../models/constants/areas');

module.exports = {
    goalRate: {
        [NORTH_AREA]: 70,
        [SOUTH_AREA]: 70
    },
    pointRate: {
        [NORTH_AREA]: 14,
        [SOUTH_AREA]: 14
    },
    pattern: /^\d{2}$/,
    numberCount: 2
}