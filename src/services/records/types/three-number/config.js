const { NORTH_AREA, SOUTH_AREA } = require('../../../../models/constants/areas');

module.exports = {
    goalRate: {
        [NORTH_AREA]: 600,
        [SOUTH_AREA]: 600
    },
    pointRate: {
        [NORTH_AREA]: 17,
        [SOUTH_AREA]: 17
    },
    pattern: /^\d{3}$/,
    numberCount: 3
}