const Record = require('../models/Record');
const sourceService = require('./source');
const messageService = require('./message.service');
const { throwError, from, of, iif, defer, empty } = require('rxjs');
const { concatMap, switchMap, map, tap } = require('rxjs/operators');
const User = require('../../src/models/User');
const { NORTH_AREA, SOUTH_AREA } = require('../models/constants/areas');
const { GOAL_POINT, TEST_GOAL_POINT } = require('../models/constants/goal-points');
const {
    SUCCESS_GOAL_POINT_REASON, SUCCESS_TEST_GOAL_POINT_REASON,
    ADD_RECORD_ENTRY_GOAL_POINT_REASON, ADD_RECORD_ENTRY_TEST_GOAL_POINT_REASON,
    DELETE_RECORD_ENTRY_GOAL_POINT_REASON, DELETE_RECORD_ENTRY_TEST_GOAL_POINT_REASON,
    UPDATE_RECORD_ENTRY_GOAL_POINT_REASON, UPDATE_RECORD_ENTRY_TEST_GOAL_POINT_REASON
} = require('../models/constants/goal-point-entry-reasons');
const { INVALID_DATA_REASON, PROCESSOR_NOT_FOUND_REASON, PROCESS_FAILED, NUMBER_CANNOT_PROCESS_REASON } = require('../models/constants/record-entry-reasons.const');
const recordTypes = require('./records/types');
const southService = require('./south-number');
const northService = require('./north-number');
const goalPointService = require('./goal-point');
const logger = require('./logger');
const { RECORD_ENTRY } = require('../models/constants/entities.const');
const xsktSource = require('./resources').xskt;
const moment = require('moment');
const dateTimeHelper = require('./date-helper');

function createRecordForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.recordId != null)
        throw new Error('User already has record');

    return from(Record.createForUser(user));
};

function addRecordEntryForUser(user, entry) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (entry == null)
        throw new Error('Entry must be not null');

    let goalPoint$;
    switch (entry.pointType) {
        case GOAL_POINT:
            goalPoint$ = goalPointService.getGoalPointForUser(user);
            break;
        case TEST_GOAL_POINT:
            goalPoint$ = goalPointService.getTestGoalPointForUser(user);
            break;
        default:
            throw new Error(`Point type must be "${GOAL_POINT}" or "${TEST_GOAL_POINT}"`);
    }

    return goalPoint$.pipe(
        switchMap((goalPoint) => iif(() => goalPoint != null && goalPoint.total >= entry.point,
            defer(() => {
                return from(Record.addEntryForUser(user, entry)).pipe(
                    switchMap(() => {
                        const description = messageService.addRecordEntry(entry);
                        const entity = { name: RECORD_ENTRY, target: JSON.stringify(entry) };
                        switch (entry.pointType) {
                            case GOAL_POINT:
                                return goalPointService.addGoalPointEntryForUser(user, ADD_RECORD_ENTRY_GOAL_POINT_REASON, -entry.point, description, entity);
                            case TEST_GOAL_POINT:
                                return goalPointService.addTestGoalPointEntryForUser(user, ADD_RECORD_ENTRY_TEST_GOAL_POINT_REASON, -entry.point, description, entity);
                        }
                        return of(null);
                    }),
                    map(() => entry)
                );
            }),
            defer(() => throwError('User don\'t have enough point'))
        ))
    );
}

function getRecordEntriesForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (user.recordId == null)
        return of([]);

    return from(Record.getEntriesForUser(user));
}

function getRecordHistoryForUser(user) {
    const recordHistory = Record.getRecordHistoryForUser(user);
    return from(recordHistory);
}

function deleteRecordEntryForUser(user, id) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (user.recordId == null)
        throw new Error('User don\'t have record');
    if (id == null)
        throw new Error('Id must be not null');

    return from(Record.deleteEntryForUser(user, id)).pipe(
        switchMap(entry => {
            const description = messageService.deleteRecordEntry(entry);
            const entity = { name: RECORD_ENTRY, target: JSON.stringify(entry) };
            switch (entry.pointType) {
                case GOAL_POINT:
                    return goalPointService.addGoalPointEntryForUser(user, DELETE_RECORD_ENTRY_GOAL_POINT_REASON, entry.point, description, entity);
                case TEST_GOAL_POINT:
                    return goalPointService.addTestGoalPointEntryForUser(user, DELETE_RECORD_ENTRY_TEST_GOAL_POINT_REASON, entry.point, description, entity);
            }
            return of(null);
        }),
        map(() => of(null))
    );
}

function alterRecordEntryForUser(user, id, entry) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (user.recordId == null)
        throw new Error('User don\'t have record');
    if (id == null)
        throw new Error('Id must be not null');
    if (entry == null)
        throw new Error('Entry must be not null');
    if ('pointType' in entry)
        throw new Error('Cannot alter "pointType"');
    if ('_id' in entry)
        throw new Error('Cannot alter "_id"');

    return from(Record.getForUser(user)).pipe(
        switchMap(record => {
            if (record != null) {
                const updateEntry = record.entries.find(item => item._id == id);
                if (updateEntry == null)
                    return throwError('Entry is not found');
                const oldEntry = Object.assign({}, updateEntry);
                const oldPoint = updateEntry.point;

                let goalPoint$;
                switch (updateEntry.pointType) {
                    case GOAL_POINT:
                        goalPoint$ = goalPointService.getGoalPointForUser(user);
                        break;
                    case TEST_GOAL_POINT:
                        goalPoint$ = goalPointService.getTestGoalPointForUser(user);
                        break;
                    default:
                        return throwError(`Point type must be "${GOAL_POINT}" or "${TEST_GOAL_POINT}"`);
                }
                const updatePoint = entry.point || 0;
                return goalPoint$.pipe(
                    switchMap(goalPoint => iif(() => goalPoint != null && goalPoint.total >= (updatePoint - oldPoint),
                        defer(() => Record.alterEntryForUser(user, id, entry)),
                        defer(() => throwError('User don\'t have enough point'))
                    )),
                    switchMap(alteredEntry => {
                        const description = messageService.alterRecordEntry(oldEntry, alteredEntry);
                        const entity = { name: RECORD_ENTRY, target: JSON.stringify(alteredEntry) };
                        switch (alteredEntry.pointType) {
                            case GOAL_POINT:
                                return goalPointService.addGoalPointEntryForUser(user, UPDATE_RECORD_ENTRY_GOAL_POINT_REASON, oldPoint - alteredEntry.point, description, entity)
                                    .pipe(
                                        map(() => alteredEntry)
                                    );
                            case TEST_GOAL_POINT:
                                return goalPointService.addTestGoalPointEntryForUser(user, UPDATE_RECORD_ENTRY_TEST_GOAL_POINT_REASON, oldPoint - alteredEntry.point, description, entity)
                                    .pipe(
                                        map(() => alteredEntry)
                                    );
                        }
                        return of(alteredEntry);
                    })
                );
            }
            return throwError('User don\'t have record');
        })
    );
}

function getRecordTypeForEntry(entry) {
    if (entry == null)
        throw new Error('Entry must be not null');

    return recordTypes.find(rt => rt.info.code === entry.type);
}

function getDataForEntry(entry) {
    logger.debug('[RecordService][getDataForEntry] Get data for process entry...');
    logger.debug('[RecordService][getDataForEntry] Get data for process entry [process] [entry]', { entry: { number: entry.number, date: entry.date } });

    if (entry == null)
        throw new Error('Entry must be not null');

    const date = new Date(entry.date);
    const source = xsktSource;

    return sourceService.getByDate(date, source).pipe(
        // sourceService.has(date).pipe(
        // tap(exist => logger.debug('[RecordService][getDataForEntry] Get data for process entry [process] [dataExist]', { exist, entry: { number: entry.number, date: entry.date } })),
        // switchMap(exists => iif(() => exists,
        //     //defer(() => cacheService.tryGetItem(cacheNumberKey.sourceByDate(entry.date), () => sourceService.getByDate(date))),
        //     defer(() => sourceService.getByDate(date, source)),
        //     of(null)
        // )),
        switchMap(source => iif(() => source != null,
            defer(() => {
                logger.debug('[RecordService][getDataForEntry] Get data for process entry [process] [dataExist]');
                let areaDataSource$;
                switch (entry.area) {
                    case NORTH_AREA:
                        areaDataSource$ = //cacheService.tryGetItem(
                            //cacheNumberKey.northNumberByDate(entry.date),
                            //() => northService.getById(source.north.id));
                            northService.getById(source.north.id);
                        break;
                    case SOUTH_AREA:
                        areaDataSource$ = //cacheService.tryGetItem(
                            //cacheNumberKey.southNumberByDate(entry.date),
                            //() => southService.getById(source.south.id));
                            southService.getById(source.south.id);
                        break;
                    default:
                        return of({
                            success: false,
                            reason: INVALID_DATA_REASON,
                            result: {
                                sourceId: source._id.toString()
                            }
                        });
                }
                return areaDataSource$.pipe(
                    switchMap(areaData => {
                        if (areaData == null)
                            return throwError(`Get "${entry.area}" data is failed`);
                        let channel = null;
                        if (entry.area === SOUTH_AREA) {
                            channel = areaData.channels.find(ch => ch.name === entry.channel);
                        }
                        else {
                            channel = areaData.channels[0];
                        }
                        if (channel == null)
                            return of({
                                success: false,
                                reason: INVALID_DATA_REASON,
                                result: {
                                    sourceId: source._id.toString()
                                }
                            });
                        return of({
                            success: true,
                            result: {
                                sourceId: source._id,
                                areaId: areaData._id,
                                numbers: channel.allNumbers,
                            }
                        });
                    })
                );
            }),
            of(null)
        ))
    );
}

function processForUser(user) {
    logger.debug('[RecordService][processForUser] Record process for user...');

    if (user == null) {
        throw new Error('User must be not null');
    }
    if (!(user instanceof User._Model)) {
        throw new Error('User is invalid');
    }

    logger.debug('[RecordService][processForUser] Record process for user [process] [user]', { user: user.email });
    //logger.debug('[RecordService][processForUser] Record process for user [process] [user]', { user: user.email.substring(0, user.email.length < 5 ? user.email.length : 5) + '***' });

    return from(Record.getForUser(user)).pipe(
        switchMap(record => {
            return iif(() => record != null && record.entries.length > 0,
                defer(() => {
                    logger.debug('[RecordService][processForUser] Record process for user [process] [record]', { id: record._id.toString(), entries: record.entries.length });

                    return from(record.entries).pipe(
                        concatMap(entry => processEntryForUser(entry, record, user)),
                    );
                }),
                defer(() => {
                    logger.debug('[RecordService][processForUser] Record process for user [process] [skip]');

                    return empty();
                })
            );
        })
    );
}

function processEntryForUser(entry, record, user) {
    logger.debug('[RecordService][processEntryForUser] Record entry process for user...');

    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (entry == null)
        throw new Error('Entry must be not null');

    logger.debug('[RecordService][processEntryForUser] Record entry process for user [process] [entry]', { entry: { number: entry.number, date: entry.date } });

    const recordType = getRecordTypeForEntry(entry);
    if (recordType == null) {
        logger.debug('[RecordService][processEntryForUser] Record entry process for user [process] [processorNotFound]');
        return processEntryResultForUser(user, record, entry, false, null, null, PROCESSOR_NOT_FOUND_REASON)
    }
    if (!recordType.processor.canProcess(entry.number)) {
        logger.debug('[RecordService][processEntryForUser] Record entry process for user [process] [entryCannotProcess]');

        return processEntryResultForUser(user, record, entry, false, recordType, null, NUMBER_CANNOT_PROCESS_REASON)
    }

    logger.debug('[RecordService][processEntryForUser] Record entry process for user [process] [entryCanProcess]');

    return getDataForEntry(entry, record, user).pipe(
        switchMap(entryData => iif(() => entryData,
            defer(() => {
                if (entryData.success) {
                    logger.debug('[RecordService][processEntryForUser] Record entry process for user [process] [entryData]', { data: { source: entryData.result.sourceId, numbers: entryData.result.numbers.length } });

                    const success = recordType.processor.process(entry.number, entryData.result.numbers);

                    logger.debug('[RecordService][processEntryForUser] Record entry process for user [process] [entryProcessResult]', { result: success });

                    return processEntryResultForUser(user, record, entry, success, recordType, entryData.result.sourceId, success ? null : PROCESS_FAILED);
                }
                return processEntryResultForUser(user, record, entry, false, recordType, entryData.result.sourceId, entryData.reason);
            }),
            of(null)
        ))
    );
}

function processEntryResultForUser(user, record, entry, success, recordType, sourceId, failedReason) {
    logger.debug('[RecordService][processEntryResultForUser] Record entry result process for user...');
    logger.debug('[RecordService][processEntryResultForUser] Record entry result process for user [process] [args]', {
        user: user.email,
        record: record._id.toString(),
        entry: { number: entry.number, date: entry.date },
        success,
        recordType,
        sourceId
    });

    const history = {
        entry,
        success,
        source: sourceId,
        failedReason
    };

    record.histories.push(history);

    logger.debug('[RecordService][processEntryResultForUser] Record entry process for user [process] [addHistory]', history);

    const index = record.entries.findIndex(item => item._id == entry._id);
    if (~index)
        record.entries.splice(index, 1);

    logger.debug('[RecordService][processEntryResultForUser] Record entry process for user [process] [removeEntry]', entry);

    return from(record.save({ validateBeforeSave: false })).pipe(
        tap(() => logger.debug('[RecordService][processEntryResultForUser] Record entry process for user [process] [recordUpdated]')),
        switchMap(() => {
            if (success) {
                let rate = 0;
                switch (entry.area) {
                    case NORTH_AREA:
                        rate = recordType.config.goalRate.north;
                        break;
                    case SOUTH_AREA:
                        rate = recordType.config.goalRate.south;
                    default:
                        logger.debug('[RecordService][processEntryResultForUser] Record entry process for user [ERROR] [invalidArea]');
                        throw new Error('Record goal rate is not found');
                }
                const goalPointValue = entry.point * rate;
                const description = messageService.processRecordEntrySuccess(entry, goalPointValue);
                const entity = { name: RECORD_ENTRY, target: JSON.stringify(entry) };
                let goalPointAdd$;
                switch (entry.pointType) {
                    case GOAL_POINT:
                        logger.debug('[RecordService][processEntryResultForUser] Record entry process for user [process] [addGoalPoint]', {
                            value: goalPointValue, description, entity, reason: SUCCESS_GOAL_POINT_REASON
                        });
                        goalPointAdd$ = goalPointService.addGoalPointEntryForUser(user, SUCCESS_GOAL_POINT_REASON, goalPointValue, description, entity);
                        break;
                    case TEST_GOAL_POINT:
                        logger.debug('[RecordService][processEntryResultForUser] Record entry process for user [process] [addTestGoalPoint]', {
                            value: goalPointValue, description, entity, reason: SUCCESS_TEST_GOAL_POINT_REASON
                        });
                        goalPointAdd$ = goalPointService.addTestGoalPointEntryForUser(user, SUCCESS_TEST_GOAL_POINT_REASON, goalPointValue, description, entity);
                        break;
                }
                return goalPointAdd$.pipe(
                    map(() => ({
                        entry,
                        success,
                        goalPoint: goalPointValue
                    }))
                );
            }
            return of({
                entry,
                success,
                reason: failedReason
            });
        })
    );
}

function isRecordableDate(date) {
    if (date == null)
        throw new Error('Date must be not null');
    if (!(date instanceof Date))
        throw new Error('Date is invalid');

    const mDate = moment(date);
    const now = new Date(Date.now());
    if (mDate.isBefore(now, 'day')) {
        return false;
    } else {
        const endDate = dateTimeHelper.getEndDate(now);
        if (moment(now).isSameOrAfter(endDate) && !mDate.isAfter(now, 'day')) {
            return false;
        }
    }
    return true;
}

function getRecordableDate() {
    const now = new Date(Date.now());
    const mNow = moment(now);
    const end = dateTimeHelper.getEndDate(now);

    let result;
    if (mNow.isBefore(end)) {
        result = end;
    }
    else {
        result = end.add(1, 'day');
    }
    const recordableDate = result
        .hour(0).minute(0).second(0).millisecond(0);

    return recordableDate.toDate();
}

function getRecordCountForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    if (user.recordId == null)
        return of(0);

    return from(Record.getForUser(user)).pipe(
        switchMap(record => iif(() => record == null,
            of(0),
            defer(() => of(record.entries.length + record.histories.length))
        ))
    );
}

function deleteRecordForUser(user) {
    const deleteRecord$ = Record.removeForUser(user);

    return from(deleteRecord$);
}

function deleteRecordById(id) {
    const record$ = Record.findById(id);

    return from(record$).pipe(
        switchMap(record => Record.remove(record))
    );
}

module.exports = {
    createRecordForUser,
    deleteRecordForUser,
    addRecordEntryForUser,
    getRecordEntriesForUser,
    deleteRecordEntryForUser,
    alterRecordEntryForUser,
    getRecordTypeForEntry,
    getDataForEntry,
    processForUser,
    processEntryForUser,
    isRecordableDate,
    getRecordableDate,
    getRecordHistoryForUser,
    getRecordCountForUser,
    deleteRecordById
};