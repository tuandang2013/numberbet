const moment = require('moment');
const config = require('../config');

function formatDate(date, format = config.DATE_FORMAT) {
    if (date == null)
        throw new Error('Date must be not null');
    if (!(date instanceof Date || 'format' in date))
        throw new Error('Date is invalid value');

    if (format == null)
        throw new Error('Format must be not null');

    const target = 'format' in date ? date : moment(date);
    return target.format(format);
}

function getDateString(date) {
    if (date == null)
        throw new Error('Date must be not null');

    const target = moment(date)
        .hour(0).minute(0).second(0).millisecond(0);
    return formatDate(target);
}

function getCurrentDateString() {
    return getDateString(new Date())
}

function getRequestableDateFormat(date = new Date(), endHour = config.NUMBER_ENDHOUR, endMinute = config.NUMBER_ENDMINUTE) {
    const requestableDate = getRequestableDate(date, endHour, endMinute);
    return formatDate(requestableDate);
}

function getRequestableDate(date = new Date(), endHour = config.NUMBER_ENDHOUR, endMinute = config.NUMBER_ENDMINUTE) {
    if (typeof date === 'undefined')
        throw new Error('Date must be not null');
    if (!(date instanceof Date))
        throw new Error('Date is invalid value');

    //check current time
    const current = moment(date);
    const end = getEndDate(date, endHour, endMinute);
    const subtract = current.isSameOrBefore(end) ? 1 : 0;
    current.subtract(subtract, 'days');
    return current.toDate();
}

function getEndDate(date = new Date(), endHour = config.NUMBER_ENDHOUR, endMinute = config.NUMBER_ENDMINUTE) {
    return moment(date).hours(endHour).minute(endMinute).second(0);
}

function isBeforeEndTime() {
    const now = moment(Date.now());
    const end = getEndDate(now);
    return now.isSameOrBefore(end);
}

module.exports = {
    getRequestableDateFormat,
    getRequestableDate,
    getDateString,
    getCurrentDateString,
    formatDate,
    getEndDate,
    isBeforeEndTime
};