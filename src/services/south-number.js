const southNumberModel = require('../models/SouthNumber');
const sourceService = require('./source');
const { from, iif } = require('rxjs');
const { switchMap } = require('rxjs/operators');
const datetimeHelper = require('./date-helper');

const southNumberService = {
    lastest() {
        const requestDate = datetimeHelper.getRequestableDateFormat();
        return from(southNumberModel.has(requestDate)).pipe(
            switchMap(exists =>
                iif(() => exists,
                    southNumberModel.getByDate(requestDate),
                    sourceService.getSouthNumber()
                ))
        );
    },

    getById(id) {
        if (id == null)
            throw new Error('Id must be not null');
        return from(southNumberModel.getById(id));
    }
};

module.exports = southNumberService;