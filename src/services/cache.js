const redis = require('redis'),
    { Observable } = require('rxjs'),
    logger = require('./logger'),
    config = require('../config');

let cache;

function getCache() {
    return cache;
}

function init() {
    logger.info('[CacheService][init] Initial cache...');

    const options = {
        port: config.REDIS_PORT,
        host: config.REDIS_HOST
    };
    cache = redis.createClient(options);

    logger.debug('[CacheService][init] Initial cache [process] [options]', options);
    //logger.debug('Initial cache [result]', cache);
    logger.info('[CacheService][init] Initial cache [done]');
}

function setItem(key, item) {
    if (key == null)
        throw new Error('Key must be not null');
    if (item == null)
        throw new Error('Item must be not null');
    const client = getCache();
    if (client == null)
        throw new Error('Cache is not initialized');

    return new Observable(subscriber => {
        client.set(key, item, (err) => {
            if (err) subscriber.error();
            else subscriber.next();
            subscriber.complete();
        });
        return subscriber.unsubscribe;
    });
}

function getItem(key) {
    if (key == null)
        throw new Error('Key must be not null');
    const client = getCache();
    if (client == null)
        throw new Error('Cache is not initialized');

    return new Observable(subscriber => {
        client.get(key, (err, item) => {
            if (err) subscriber.error(err);
            else subscriber.next(item);
            subscriber.complete();
        });
        return subscriber.unsubscribe;
    });
}

function tryGetItem(key, fn) {
    if (key == null)
        throw new Error('Key must be not null');
    const client = getCache();
    if (client == null)
        throw new Error('Cache is not initialized');

    return new Observable(subscriber => {
        client.get(key, (err, item) => {
            if (err) {
                subscriber.error(err);
                subscriber.complete();
            }
            else {
                if (item != null) {
                    subscriber.next(item);
                    subscriber.complete();
                } else {
                    const result = fn();
                    if (result instanceof Observable) {
                        result.subscribe(r => {
                            client.set(key, r, (err) => {
                                if (err) subscriber.error(err);
                                else subscriber.next(r);
                                subscriber.complete();
                            });
                        });
                    } else if (result instanceof Promise) {
                        result.then(r => {
                            client.set(key, r, err => {
                                if (err) subscriber.error(err)
                                else subscriber.next(r);
                            });
                        })
                            .then(subscriber.complete);
                    } else {
                        client.set(key, result, err => {
                            if (err) subscriber.error(err)
                            else subscriber.next(result);
                            subscriber.complete();
                        });
                    }
                }
            }
        });
        return subscriber.unsubscribe;
    });
}

module.exports = {
    getCache,
    init,
    setItem,
    getItem,
    tryGetItem
};