const moment = require('moment');
const Source = require('../../models/Source');
const { xskt_mienbac, xskt_miennam } = require('../SourceParser');
const southNumber = require('../../models/SouthNumber').insert;
const northNumber = require('../../models/NorthNumber').insert;

const xsktSource = {
    name: 'Xổ sổ kiến thiết',
    url: 'https://xskt.com.vn/',
    sections: [{
        code: 'MB0',
        name: 'Miền Bắc',
        parser: xskt_mienbac,
        process: northNumber,
        url: 'https://xskt.com.vn/xsmb/',
        dailyUrl
    }, {
        code: 'MN0',
        name: 'Miền Nam',
        parser: xskt_miennam,
        process: southNumber,
        url: 'https://xskt.com.vn/xsmn/',
        dailyUrl
    }],
    process: (name, url, [north, south]) => Source.insert({
        name, url,
        date: north.date || south.date,
        north: {
            date: north.date,
            id: north._id
        },
        south: {
            date: south.date,
            id: south._id
        }
    })
};

function dailyUrl(date) {
    if (date == null) throw new Error('Date must be not null');

    const dMoment = moment(date);
    const rDate = dMoment.date();
    const rMonth = dMoment.month() + 1;

    const dailyUrl = `${this.url}ngay-${rDate}-${rMonth}`;
    return dailyUrl;
}

module.exports = xsktSource;