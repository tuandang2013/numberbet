const { Setting } = require('../models');
const logger = require('./logger');

const settingService = {
    init() {
        logger.info('[Setting][init] Loading settings...');
        return Setting.init().then(settings => {
            logger.debug('[Setting][init] Loading settings [result]', settings);
            logger.info('[Setting][init] Loading settings [done]')
        });
    }
};

module.exports = settingService;