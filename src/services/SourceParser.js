const cheerio = require('cheerio');

class Parser {
    parse() { }
}

class XsktParser extends Parser {
    dataContainerElementName = 'div.box-ketqua';
    dataElementName = 'div.box-table table';
    numberPattern = /(?:<[\D+^>]>)?(\d+)(?=<\/?[\D+^>]\/?>)?/g;
    dataInfoElementName = 'h2 a';
    datePattern = /(\d{1,2})[-\/](\d{1,2})(?:[-\/])?(\d{4})?/;
    goalCodePattern = /^G[\.]?(\d)$/;

    getInfo($target) {
        const info = $target.find(this.dataInfoElementName).toArray();
        if (info.length < 2) return {};
        const [eName, eDate, eDay] = info;
        const date = this.getDate(eDate.firstChild.data);
        return {
            url: eName.attribs['href'],
            name: eName.firstChild.data,
            date: date,
            day: eDay ? eDay.firstChild.data : null
        };
    }

    getCode($target) {
        const eTable = $target.find(this.dataElementName).first();
        let code = eTable.attr('id');
        if (!code) {
            const firstAnchor = eTable.find('h3 > a')[0];
            if (firstAnchor == null) return;
            code = firstAnchor.attribs['href'].substring(1);
        }
        return code;
    }

    getNumberData(dataRows, targetIndex = 1) {
        const goals = [];
        const allNumbers = [];
        for (const row of dataRows) {
            const $ = cheerio.load(row);
            const columns = $('td');
            let goalCode = $(columns[0]).text(), goalNumber = 0;
            const goalName = $(columns[0]).attr('title'),
                numberData = $(columns[targetIndex]).html();

            if (goalName == null || numberData == null) continue;

            const match = this.goalCodePattern.exec(goalCode);
            if (match) {
                goalNumber = Number(match[1]);
                goalCode = `G.${goalNumber}`;
            } else if (goalCode === 'ĐB') {
                goalCode = 'G.ĐB';
            }

            const numbers = [];
            for (const group of numberData.matchAll(this.numberPattern)) {
                numbers.push(group[1]);
                allNumbers.push(group[1]);
            }

            goals[goalNumber] = {
                code: goalCode,
                name: goalName,
                numbers
            };
        }
        goals.reverse();
        return {
            allNumbers,
            goals
        };
    }

    getDate(data) {
        const match = this.datePattern.exec(data);
        if (match) {
            let [, date, month, year] = match;
            if (date.length == 1) date = '0' + date;
            if (month.length == 1) month = '0' + month;

            const yearNow = new Date().getFullYear(),
                dateString = `${year || yearNow}-${month}-${date}`;

            return dateString;
        }
        return null;
    }

    parse(dataSource) {
        const $ = cheerio.load(dataSource),
            dataBoxes = [];

        $(this.dataContainerElementName).each((i, boxKetqua) => {
            const $target = $(boxKetqua),
                info = this.getInfo($target),
                code = this.getCode($target);

            dataBoxes.push({
                info, code,
                target: $target.find(this.dataElementName).first()
            });
        });

        return dataBoxes;
    }
}

class XsktMienNamParser extends XsktParser {
    constructor(code, name) {
        super();
        this.code = code;
        this.name = name;
    }

    parse(dataSource) {
        const dataBoxes = super.parse(dataSource);
        const data = dataBoxes
            .filter(box => box.code === this.code)
            .map(box => {
                const $ = cheerio.load(box.target[0]);
                const [headerRow, ...numberRows] = $('tr').toArray();
                //url
                const dailyUrl = $(headerRow).find('th').children('a').attr('href');
                //numbers
                const channels = this.getChannels($(headerRow), numberRows);
                return {
                    code: box.code,
                    ...box.info,
                    dailyUrl,
                    channels
                };
            });
        return data;
    }

    getChannels($headerRow, numberRows) {
        const channels = [];
        $headerRow.find('th').each((i, header) => {
            if (i === 0) return;
            const item = cheerio.load(header)('a');
            const numberData = this.getNumberData(numberRows, i);

            channels.push({
                name: item.text(),
                url: item.attr('href'),
                goals: numberData.goals,
                allNumbers: numberData.allNumbers
            });
        });
        return channels;
    }
}

class XsktMienBacParser extends XsktParser {
    channelNamePatter = /\((.+)\)/;

    constructor(code, name) {
        super();
        this.code = code;
        this.name = name;
    }

    parse(dataSource) {
        const dataBoxes = super.parse(dataSource);
        const data = dataBoxes
            .filter(box => box.code === this.code)
            .map(box => {
                const $ = cheerio.load(box.target[0]);
                const [headerRow, ...numberRows] = $('tr').toArray();
                //numbers
                const channel = this.getChannel($(headerRow), numberRows);
                return {
                    code: box.code,
                    ...box.info,
                    dailyUrl: channel.dailyUrl,
                    channels: [channel]
                };
            });
        return data;
    }

    getChannel($headerRow, numberRows) {
        const contentElement = $headerRow.find('th').first().find('h3')[0];
        const [eUrl, , eDailyUrl, eName] = contentElement.children;
        const match = this.channelNamePatter.exec(eName.data);
        const name = match && match[1];

        //number
        const numberData = this.getNumberData(numberRows);

        const channel = {
            name: name,
            url: eUrl.attribs['href'],
            dailyUrl: eDailyUrl.attribs['href'],
            goals: numberData.goals,
            allNumbers: numberData.allNumbers
        };
        return channel;
    }
}

module.exports = {
    xskt_miennam: XsktMienNamParser,
    xskt_mienbac: XsktMienBacParser
};