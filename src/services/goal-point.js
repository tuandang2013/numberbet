const GoalPoint = require('../models/GoalPoint');
const TestGoalPoint = require('../models/TestGoalPoint');
const { from, defer, iif, of, forkJoin } = require('rxjs');
const { map, switchMap } = require('rxjs/operators');
const User = require('../models/User');
const { AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON, CHECKOUT_REASON } = require('../models/constants/goal-point-entry-reasons');
const config = require('../config');
const messageService = require('./message.service');
const converter = require('../helpers/converter');
const { getPager } = require('../helpers/pager.helper');

function createGoalPointForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.goalPointId != null)
        throw new Error('User already has goal point');

    return from(GoalPoint.createForUser(user));
};

function getGoalPointForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');

    return from(GoalPoint.getForUser(user));
}

function createTestGoalPointForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');
    if (user.testGoalPointId != null)
        throw new Error('User already has test goal point');

    return from(TestGoalPoint.createForUser(user));
};

function getTestGoalPointForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');

    return from(TestGoalPoint.getForUser(user));
}

function createUserGoalPoints(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model && '_id' in user))
        throw new Error('User is invalid');

    const goalPoint = user.goalPointId ? GoalPoint.getForUser(user) : GoalPoint.createForUser(user);
    const testGoalPoint = user.testGoalPointId ? TestGoalPoint.getForUser(user) : TestGoalPoint.createForUser(user);

    return from(Promise.all([goalPoint, testGoalPoint]));
}

function addTestGoalPointEntryForUser(user, reason, value, description, entity) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (reason == null)
        throw new Error('Reason must be not null');
    if (value == null)
        throw new Error('Value must be not null');

    return from(TestGoalPoint.addEntryForUser(user, { reason, value, description, entity }));
}

function addDailyTestGoalPointEntryForUser(user, date) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (date == null)
        throw new Error('Date must be not null');
    if (!(date instanceof Date))
        throw new Error('Date is invalid');

    return from(TestGoalPoint.findEntryByDateForUser(user, date)).pipe(
        map(entries => entries.filter(entry => entry.reason === AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON)),
        switchMap(entries => iif(() => entries.length,
            defer(() => of(entries[0])),
            defer(() => TestGoalPoint.addEntryForUser(user, {
                value: config.INCREMENT_TESTPOINT,
                reason: AUTO_DAILY_INCREMENT_TEST_POINT_ENTRY_REASON,
                description: messageService.dailyTestGoalPoint(config.INCREMENT_TESTPOINT, date),
                createdDate: date
            }))))
    );
}

function deleteTestGoalPointEntryForUser(user, id) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (id == null)
        throw new Error('Id must be not null');
    if (user.testGoalPointId == null)
        throw new Error('User don\'t have test goal point');

    return from(TestGoalPoint.deleteEntryForUser(user, id));
}

function addGoalPointEntryForUser(user, reason, value, description, entity) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (reason == null)
        throw new Error('Reason must be not null');
    if (value == null)
        throw new Error('Value must be not null');

    return from(GoalPoint.addEntryForUser(user, { reason, value, description, entity }));
}

function deleteGoalPointEntryForUser(user, id) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (id == null)
        throw new Error('Id must be not null');
    if (user.goalPointId == null)
        throw new Error('User don\'t have goal point');

    return from(GoalPoint.deleteEntryForUser(user, id));
}

function getPointInfoForUser(user) {
    const goalPoint$ = getGoalPointForUser(user);
    const testGoalPoint$ = getTestGoalPointForUser(user);

    return forkJoin([
        goalPoint$,
        testGoalPoint$
    ]).pipe(
        map(([goalPoint, testGoalPoint]) => ({
            goalPoint: goalPoint ? goalPoint.total : 0,
            testGoalPoint: testGoalPoint ? testGoalPoint.total : 0
        }))
    );
}

function getGoalPointEntriesForUser(user, { pageNumber = 1, pageSize = Number.MAX_SAFE_INTEGER } = {}) {
    pageNumber = converter.toNumber(pageNumber, 1);
    if (pageNumber < 1) pageNumber = 1;

    pageSize = converter.toNumber(pageSize, config.DEFAULT_PAGESIZE);
    if (pageSize <= 0) pageSize = config.DEFAULT_PAGESIZE;

    const goalPointEntries = GoalPoint.getEntriesForUser(user);

    return from(goalPointEntries).pipe(
        map(entries => entries.sort((item1, item2) => item2.createdDate.getTime() - item1.createdDate.getTime())),
        map(entries => ({
            data: entries.filter((_, index) => index >= (pageNumber - 1) * pageSize && index < pageNumber * pageSize),
            pager: getPager({ pageNumber, pageSize, count: entries.length })
        }))
    );
}

function getTestGoalPointEntriesForUser(user, { pageNumber = 1, pageSize = Number.MAX_SAFE_INTEGER } = {}) {
    pageNumber = converter.toNumber(pageNumber, 1);
    if (pageNumber < 1) pageNumber = 1;

    pageSize = converter.toNumber(pageSize, config.DEFAULT_PAGESIZE);
    if (pageSize <= 0) pageSize = config.DEFAULT_PAGESIZE;

    const testGoalPointEntries = TestGoalPoint.getEntriesForUser(user);

    return from(testGoalPointEntries).pipe(
        map(entries => entries.sort((item1, item2) => item2.createdDate.getTime() - item1.createdDate.getTime())),
        map(entries => ({
            data: entries.filter((_, index) => index >= (pageNumber - 1) * pageSize && index < pageNumber * pageSize),
            pager: getPager({ pageNumber, pageSize, count: entries.length })
        }))
    );
}

function deleteGoalPointForUser(user) {
    const deleteGoalPoint$ = GoalPoint.removeForUser(user);

    return from(deleteGoalPoint$);
}

function deleteTestGoalPointForUser(user) {
    const deleteTestGoalPoint$ = TestGoalPoint.removeForUser(user);

    return from(deleteTestGoalPoint$);
}

function deleteGoalPointById(id) {
    const goalPoint$ = GoalPoint.findById(id);

    return from(goalPoint$).pipe(
        switchMap(goalPoint => GoalPoint.remove(goalPoint))
    );
}

function deleteTestGoalPointById(id) {
    const testGoalPoint$ = TestGoalPoint.findById(id);

    return from(testGoalPoint$).pipe(
        switchMap(testGoalPoint => TestGoalPoint.remove(testGoalPoint))
    );
}

function getGoalPointAmountForUser(user) {
    const goalPoint$ = GoalPoint.getForUser(user);

    return from(goalPoint$).pipe(
        map(goalPoint => {
            if (goalPoint == null)
                return 0;
            return goalPoint.total;
        })
    );
}

function addGoalPointCheckoutForUser(user, amount) {
    const date = new Date(Date.now());
    const addGoalPointEntry$ = addGoalPointEntryForUser(
        user,
        CHECKOUT_REASON,
        -amount,
        messageService.checkout(amount, date));

    return from(addGoalPointEntry$);
}

module.exports = {
    getPointInfoForUser,

    getGoalPointForUser,
    getGoalPointEntriesForUser,
    getGoalPointAmountForUser,
    createGoalPointForUser,
    createUserGoalPoints,
    addDailyTestGoalPointEntryForUser,
    addGoalPointEntryForUser,
    addGoalPointCheckoutForUser,
    deleteGoalPointForUser,
    deleteGoalPointEntryForUser,
    deleteGoalPointById,

    getTestGoalPointForUser,
    getTestGoalPointEntriesForUser,
    createTestGoalPointForUser,
    addTestGoalPointEntryForUser,
    deleteTestGoalPointForUser,
    deleteTestGoalPointEntryForUser,
    deleteTestGoalPointById
}