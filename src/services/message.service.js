const moment = require('moment');

function dailyTestGoalPoint(value, date) {
    if (value == null)
        throw new Error('Value must be not null');
    if (date == null)
        throw new Error('Date must be not null');

    const message = `You received ${value} points for date(${moment(date).format('DD/MM/YYYY')})`;
    return message;
}

function checkout(amount, date) {
    const message = `You are CHECKED OUT ${amount} point(s) by system (${moment(date).format('DD/MM/YYYY')})`;
    return message;
}

function addRecordEntry(entry) {
    if (entry == null)
        throw new Error('Entry must be not null');

    const dateFormat = moment(Date.now()).format('YYYY-MM-DD hh:mm');
    const message = `Record entry added at ${dateFormat} [${getEntryInfo(entry)}]`;
    return message;
}

function deleteRecordEntry(entry) {
    if (entry == null)
        throw new Error('Entry must be not null');

    const dateFormat = moment(Date.now()).format('YYYY-MM-DD hh:mm');
    const message = `Record entry ${entry._id.toString()} deleted at ${dateFormat} [${getEntryInfo(entry)}]`;
    return message;
}

function getEntryInfo(entry) {
    return `number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})`;
}

function alterRecordEntry(oldEntry, newEntry) {
    if (oldEntry == null)
        throw new Error('Old entry must be not null');
    if (newEntry == null)
        throw new Error('New entry must be not null');

    const dateFormat = moment(Date.now()).format('YYYY-MM-DD hh:mm');
    const message = `Record entry ${oldEntry._id.toString()} updated at ${dateFormat} [${getEntryInfo(oldEntry)}][${getEntryInfo(newEntry)}]`;
    return message;
}

function processRecordEntryFailed(entry) {
    if (entry == null)
        throw new Error('Entry must be not null');

    const message = `Record entry ${entry._id.toString()} is failed at process ${moment(Date.now()).format('YYYY-MM-DD hh:mm')} [number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})]`
    return message;
}

function processRecordEntrySuccess(entry, goalPoint) {
    if (entry == null)
        throw new Error('Entry must be not null');
    if (goalPoint == null)
        throw new Error('Goal point must be not null');

    const message = `Record entry ${entry._id.toString()} is success (${goalPoint}) at process ${moment(Date.now()).format('YYYY-MM-DD hh:mm')} [number: ${entry.number}, channel: ${entry.channel}(${entry.area}), point: ${entry.point}(${entry.pointType})]`
    return message;
}

module.exports = {
    dailyTestGoalPoint,
    addRecordEntry,
    deleteRecordEntry,
    alterRecordEntry,
    processRecordEntryFailed,
    processRecordEntrySuccess,
    checkout
};