const logger = require('./logger');
const Claim = require('../models/Claim');
const User = require('../models/User');
const { forkJoin, from, of, iif, defer, empty } = require('rxjs');
const { concatMap, toArray, filter, mergeMap, switchMap, finalize, map } = require('rxjs/operators');

const createIfNotFound$ = (name) => from(Claim.findByName(name)).pipe(
    switchMap(found => iif(() => found,
        empty(),
        defer(() => Claim.create({ name }))
    ))
);

function init() {
    logger.info('[Claim][init] Init claims...');

    const defaultClaims = Claim.defaultClaims;
    return from(defaultClaims).pipe(
        concatMap(claimName => createIfNotFound$(claimName)),
        finalize(() => logger.info('[Claim][init] Init claims [done]'))
    );
}

function create(claim) {
    const create$ = Claim.create(claim);
    return from(create$);
}

function addClaimForUser(user, claimName) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (claimName == null)
        throw new Error('Claim name must be not null');

    return from(Claim.findByName(claimName)).pipe(
        switchMap(claim => iif(() => claim != null,
            of(claim),
            defer(() => from(Claim.create({ name: claimName })))
        )),
        switchMap(claim => {
            user.claims = user.claims || [];
            user.claims.push(claim._id);
            return user.save();
        }),
        map(user => user.claims)
    );
}

function removeClaimForUser(user, claimName) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (claimName == null)
        throw new Error('Claim name must be not null');

    return from(Claim.findByName(claimName)).pipe(
        switchMap(claim => iif(() => claim != null,
            defer(() => removeClaim$(user, claim._id)),
            of(user.claims)
        ))
    );
}

const removeClaim$ = (user, claimId) => {
    const index = user.claims.indexOf(claimId);
    if (~index) {
        user.claims.splice(index, 1);
        return user.save().then(() => user.claims);
    }
    return of(user.claims);
}

function removeClaimByIdForUser(user, claimId) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');
    if (claimId == null)
        throw new Error('Claim id must be not null');

    const index = user.claims.indexOf(claimId);
    if (~index) {
        user.claims.splice(index);
        return from(user.save()).pipe(
            map(user => user.claims)
        );
    }
    return of(user.claims);
}

function getClaimsForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return of(user.claims || []).pipe(
        map(claimIds => claimIds.map(id => Claim.findById(id))),
        switchMap(claims$ => forkJoin(claims$))
    );
}

function addRegisteredClaimForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return from(Claim.registeredClaim()).pipe(
        switchMap(registeredClaim => {
            user.claims = user.claims || [];
            if (!user.claims.some(claimId => registeredClaim._id == claimId)) {
                user.claims.push(registeredClaim._id);
                return user.save();
            }
            return of(user);
        }),
        map(user => user.claims)
    )
}

function addGuestClaimForUser(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return from(Claim.guestClaim()).pipe(
        switchMap(guestClaim => {
            user.claims = user.claims || [];
            if (!user.claims.some(claimId => guestClaim._id == claimId)) {
                user.claims.push(guestClaim._id);
                return user.save();
            }
            return of(user);
        }),
        map(user => user.claims)
    )
}

function getAllClaims() {
    return from(Claim.getAll());
}

function getClaimIdsByNames(names) {
    if (names == null || names.length == 0)
        return of([]);

    return from(names).pipe(
        mergeMap(name => Claim.findByName(name)),
        map(claim => claim ? claim._id.toString() : null),
        toArray(),
        map(ids => ids.filter(id => id != null))
    );
}

module.exports = {
    init,
    create,
    addClaimForUser,
    removeClaimForUser,
    removeClaimByIdForUser,
    getClaimsForUser,
    addRegisteredClaimForUser,
    addGuestClaimForUser,
    getAllClaims,
    getClaimIdsByNames
};