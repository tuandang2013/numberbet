const jwt = require('jsonwebtoken'),
    config = require('../config'),
    { Observable } = require('rxjs');

const defaultConfig = {
    key: config.USER_PRIVATEKEY,
    expire: config.USER_EXPIRE
};

function verifyToken(token, { key } = defaultConfig) {
    if (token == null)
        throw new Error('Token must be not null');

    return Observable.create(observer => {
        jwt.verify(token, key, (err, decoded) => {
            if (err) observer.error(err);
            else {
                observer.next(decoded);
                observer.complete();
            }
        });
    });
}

function getToken(payload, { key = defaultConfig.key, expire = defaultConfig.expire } = defaultConfig) {
    if (payload == null)
        throw new Error('Payload must be not null');

    return jwt.sign(payload, key, { expiresIn: expire });
}

module.exports = {
    getToken,
    verifyToken
};