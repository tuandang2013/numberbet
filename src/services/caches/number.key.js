const sourceByDate = (date) => `@app/cached/source/date/${date}`;
const southNumberByDate = (date) => `@app/cached/south-number/date/${date}`;
const northNumberByDate = (date) => `@app/cached/north-number/date/${date}`;

module.exports = {
    sourceByDate,
    southNumberByDate,
    northNumberByDate
};