const User = require('../models/User');
const { from, of, iif, forkJoin, concat } = require('rxjs');
const { switchMap, map, last } = require('rxjs/operators');
const bcrypt = require('bcrypt');
const goalPointService = require('./goal-point');
const recordService = require('./record');
const config = require('../config');
const converter = require('../helpers/converter');
const { getPager } = require('../helpers/pager.helper');

function insert(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (user.email == null)
        throw new Error('Email must be not null');
    if (user.hashedPassword == null)
        throw new Error('Hashed password must be not null');
    if (user.salt == null)
        throw new Error('Password salt must be not null');

    return from(User.insert(user)).pipe(
        switchMap(insertedUser =>
            forkJoin([goalPointService.createUserGoalPoints(insertedUser), recordService.createRecordForUser(insertedUser)])
                .pipe(switchMap(() => User.update(insertedUser))))
    );
}

function update(user) {
    if (user == null)
        throw new Error('User must be not null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return from(User.update(user));
}

function emailExists(email) {
    if (email == null)
        throw new Error('Email must be not null');

    return from(User.findByEmail(email)).pipe(
        map(user => !!user)
    );
}

function findUserById(id) {
    if (id == null)
        throw new Error('Id must be not null');
    return from(User.findById(id));
}

function findUserByEmailAndPassword(email, password) {
    if (email == null)
        throw new Error('Email must be not null');
    if (password == null)
        throw new Error('Password must be not null');

    return from(User.findByEmail(email)).pipe(
        switchMap(user =>
            iif(() => user == null,
                of(null),
                user != null && checkPassword(user, password).pipe(
                    map(pwdValid => pwdValid ? user : null)
                )))
    );
}

function hashPassword(password) {
    if (password == null)
        throw new Error('Password must be not null');

    return from(new Promise((resolve, reject) => {
        bcrypt.genSalt(12, (err, salt) => {
            if (err) return reject(err);
            bcrypt.hash(password, salt, (err2, hash) => {
                if (err2) return reject(err2);
                const result = { hash, salt };
                resolve(result);
            });
        });
    }));
}

function checkPassword(user, password) {
    if (user == null)
        throw new Error('User must be not null');
    if (!('hashedPassword' in user && 'salt' in user))
        throw new Error('User is invalid');

    if (password == null)
        return of(false);

    return from(new Promise((resolve, reject) => {
        bcrypt.hash(password, user.salt, (err, hashedPassword) => {
            if (err) return reject(err);
            resolve(user.hashedPassword === hashedPassword);
        });
    }));
}

function getAllUsers() {
    return from(User.getAll());
}

function getUsers({ pageNumber = 1, pageSize = config.DEFAULT_PAGESIZE } = {}) {
    pageNumber = converter.toNumber(pageNumber, 1);
    pageSize = converter.toNumber(pageSize, config.DEFAULT_PAGESIZE);

    if (pageNumber < 1) pageNumber = 1;
    if (pageSize <= 0) pageSize = config.DEFAULT_PAGESIZE;

    return forkJoin([
        User.getUsers({ pageNumber, pageSize }),
        User.count()
    ]).pipe(
        map(([users, count]) => ({
            data: users,
            pager: getPager({ pageNumber, pageSize, count })
        }))
    );
}

function deleteUser(user) {
    if (user == null)
        throw new Error('User must not be null');
    if (!(user instanceof User._Model))
        throw new Error('User is invalid');

    return concat([
        user.recordId ? recordService.deleteRecordById(user.recordId) : of(null),
        user.goalPointId ? goalPointService.deleteTestGoalPointById(user.testGoalPointId) : of(null),
        user.testGoalPointId ? goalPointService.deleteGoalPointById(user.goalPointId) : of(null)
    ]).pipe(
        last(),
        switchMap(() => User.remove(user))
    );
}

function findUserByEmail(email){
    const userByEmail$ = User.findByEmail(email);

    return from(userByEmail$);
}

module.exports = {
    findUserByEmailAndPassword,
    findUserById,
    hashPassword,
    checkPassword,
    insert,
    update,
    deleteUser,
    emailExists,
    getAllUsers,
    getUsers,
    findUserByEmail
};