const northNumberModel = require('../models/NorthNumber');
const sourceService = require('./source');
const { switchMap } = require('rxjs/operators');
const { from, iif } = require('rxjs');
const datetimeHelper = require('./date-helper');

const northNumberService = {
    lastest() {
        const requestDate = datetimeHelper.getRequestableDateFormat();
        return from(northNumberModel.has(requestDate)).pipe(
            switchMap(exists =>
                iif(() => exists,
                    northNumberModel.getByDate(requestDate),
                    sourceService.getNorthNumber()
                ))
        );
    },

    getById(id) {
        if (id == null)
            throw new Error('Id must be not null');
        return from(northNumberModel.getById(id));
    }
};

module.exports = northNumberService;