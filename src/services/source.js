const axios = require('axios').default;
const Source = require('../models/Source');
const { defer, from, iif, forkJoin, of, throwError } = require('rxjs');
const { map, switchMap } = require('rxjs/operators');
const datetimeHelper = require('../services/date-helper');
const xsktSource = require('./resources/index').xskt;

class SourceService {

    source$ = ({ name, url, sections, process }, date) =>
        (date == null ?
            this.lastest$(url, sections) :
            this.daily$(date, sections)
        ).pipe(
            switchMap(data => iif(() => data,
                defer(() =>
                    from(Source.has({ date: data[0].date || data[1].date })).pipe(
                        switchMap(exists => iif(() => exists || !process,
                            of(data),
                            defer(() => process(name, url, data)).pipe(
                                map(() => data)
                            )
                        ))
                    )
                ),
                of(data))
            )
        );

    section$ = ({ code, name, parser: Parser, process }, html) => {
        const data = new Parser(code, name).parse(html);
        return process ? process(data) : of(data);
    };

    html$ = url => from(axios.get(url)).pipe(
        map(response => response.data)
    );

    lastest$ = (url, sections) => this.html$(url).pipe(
        switchMap(html => forkJoin(
            sections.map(section => this.section$(section, html))))
    );

    daily$ = (date, sections) => forkJoin(
        sections.map(section => this.html$(section.dailyUrl(date)).pipe(
            switchMap(html => this.section$(section, html))
        ))
    ).pipe(
        map(data => {
            const [north, south] = data;
            const formatDate = datetimeHelper.formatDate(date)
            if (north.date !== formatDate ||
                south.date !== formatDate)
                return null;
            return data;
        })
    );

    _getSection(index) {
        const southNumberSection = xsktSource.sections[index];
        return this.html$(xsktSource.url).pipe(
            switchMap(html => this.section$(southNumberSection, html))
        )
    }

    has(date) {
        if (date == null)
            throw new Error('Date must be not null');
        if (!(date instanceof Date))
            throw new Error('Date must be a Date');

        const requestDate = datetimeHelper.formatDate(date);

        return from(Source.has({ date: requestDate }));
    }

    getByDate(date, target) {
        if (date == null)
            throw new Error('Date must be not null');
        if (!(date instanceof Date))
            throw new Error('Date is invalid value');

        const requestDateFormat = datetimeHelper.formatDate(date);
        return from(Source.getByDate(requestDateFormat)).pipe(
            switchMap(source => iif(() => source,
                of(source),
                iif(() => target == null,
                    throwError('Target must be not null'),
                    defer(() => this.source$(target, date))
                )
            ))
        );
    }

    getSouthNumber() {
        return this._getSection(1);
    }

    getNorthNumber() {
        return this._getSection(0);
    }

    getData(date, target) {
        if (target == null)
            throw new Error('Target must be not null');

        return (date == null ? of(false) : this.has(date)).pipe(
            switchMap(exists => iif(() => exists,
                defer(() => this.getByDate(date)),
                defer(() => this.source$(target, date))))
        );
    }
}

module.exports = new SourceService();