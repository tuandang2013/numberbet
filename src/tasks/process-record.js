const userService = require('../services/user'),
    recordService = require('../services/record'),
    logger = require('../services/logger'),
    { Observable } = require('rxjs'),
    { concatMap, finalize, tap } = require('rxjs/operators');

const interval = 5000;
let lastRunTime;
let nextRunTime;
let lastProcessTime = 0;
let totalProcessTime = 0;

const info = {
    get name() {
        return 'Process Record Task';
    },
    get interval() {
        return interval;
    },
    get lastProcessTime() {
        return lastProcessTime;
    },
    get lastRunTime() {
        return lastRunTime;
    },
    get nextRunTime() {
        return nextRunTime;
    },
    get totalProcessTime() {
        return totalProcessTime;
    },
    update(start, end) {
        lastRunTime = end;
        nextRunTime = lastRunTime + interval;
        lastProcessTime = end - start;
        totalProcessTime += lastProcessTime;
    }
}

function processRecord() {
    logger.info('[ProcessRecordTask][processRecord] Task process record run...', { name: info.name });

    const startTime = Date.now();
    logger.debug('[ProcessRecordTask][processRecord] Task process record run [process] [startTime]', { startTime });

    return Observable.create(observer => {
        userService.getAllUsers().pipe(
            tap(users => logger.debug('[ProcessRecordTask][processRecord] Task process record run [process] [users]', users.map(user => user.email.substring(0, user.email.length < 5 ? user.email.length : 5) + '***')))
        ).subscribe(users => {
            for (const user of users) {
                logger.debug('[ProcessRecordTask] Task process record run [process] [user]', { user: user.email.substring(0, user.email.length < 5 ? user.email.length : 5) + '***' });
                // logger.debug('[ProcessRecordTask] Task process record run [process] [user]', { user: user.email });
                observer.next(user);
            }
            observer.complete();
        });
    }).pipe(
        concatMap(user => recordService.processForUser(user)),
        tap(upr => logger.debug('[ProcessRecordTask][processRecord] Task process record run [process] [userProcessResult]', upr)),
        finalize(() => {
            const endTime = Date.now();

            logger.debug('[ProcessRecordTask][processRecord] Task process record run [process]', { endTime });

            info.update(startTime, endTime);

            logger.info('[ProcessRecordTask][processRecord] Task process record run [done]', { name: info.name, processTime: info.lastProcessTime });
        })
    );
}

module.exports = {
    task: processRecord,
    info
};