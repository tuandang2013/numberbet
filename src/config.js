const fs = require('fs');
const join = require('path').join;
const userPrivateKey = fs.readFileSync(join(process.cwd(), 'keys/privateKey.jwt.txt'), 'utf-8');
const sessionSecret = fs.readFileSync(join(process.cwd(), 'keys/secret.session.txt'), 'utf-8');
require('dotenv').config();

const supportedOrigins = ['http://localhost:3000'];

module.exports = {
    PORT: process.env.PORT || 3000,
    REDIS_PORT: process.env.REDIS_PORT || 6379,
    REDIS_HOST: process.env.REDIS_HOST || '127.0.0.1',
    DB: process.env.DB,
    BASE_PATH: __dirname,
    ROOT_PATH: process.cwd(),
    NUMBER_ENDHOUR: Number(process.env.NUMBER_ENDHOUR) || 0,
    NUMBER_ENDMINUTE: Number(process.env.NUMBER_ENDMINUTE) || 0,
    DATE_FORMAT: process.env.DATE_FORMAT || 'YYYY-MM-DD',
    USER_PRIVATEKEY: userPrivateKey,
    SESSION_SECRET: sessionSecret,
    USER_EXPIRE: '1d',
    USER_REMEMBER_EXPIRE: '30d',
    SUPPORTED_ORIGINS: supportedOrigins,
    DEFAULT_TESTPOINT: Number(process.env.DEFAULT_TESTPOINT),
    INCREMENT_TESTPOINT: Number(process.env.INCREMENT_TESTPOINT),
    EMAIL_PATTERN: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    PHONE_PATTERN: /^0\d{9,10}$/,
    NUMBER_PATTERN: /^\d+$/,
    DATE_PATTERN: /^\d{4}-\d{1,2}-\d{1,2}$/,
    ID_PATTERN: /^[0-9a-fA-F]{24}$/,
    TASK_RUNNER_INTERVAL: process.env.TASK_RUNNER_INTERVAL || 3600000,
    DEBUG: process.env.DEBUG === 'true',
    DEFAULT_PAGESIZE: 12
};